import numpy as np
from bbo.rates import Perturbative, SemiClassicTwoState, print_crossover
from bbo.logger import logger

from bbo.PES.tully import APES, APESBH, APES0, APES1, DPES, DPES0, DPES1
from bbo.dvr.tully import dvrBH, dvr0, dvr1, dvrE, Ddvr0, Ddvr1, gridl


print_crossover(APES0)
beta = 500
beta = 1000
logger.info("beta: " + str(beta))

tau_test = .15

RatePert = Perturbative(APES0, APES1, APES, dvr0, dvr1, beta)
RateSC = SemiClassicTwoState(APES0, APES1, APES, beta)

# Testing semiclassics
z1 = np.array([beta/2])
z2 = np.array([tau_test])

logger.info("#########")
logger.info("Testing some basic observables in SC and QM:")
logger.info("One state")
cff0000_QM = RatePert.cff0000(z1)[0].real #works only for beta/2
logger.info("cff0000_QM: " + str(cff0000_QM))
cff0000_SC = RateSC.cff0000(z1[0])
logger.info("cff0000_SC: " + str(cff0000_SC))
delta0000 = (cff0000_SC-cff0000_QM) / cff0000_QM
logger.info("error: " + "{:.2f}%".format(delta0000*100))

prop = .50
rhoF0F0_QM = RatePert.trRhoF0F0(z1*2, prop=prop).real
logger.info("rhoF0F0_QM: " + str(rhoF0F0_QM))
rhoF0F0_SC = RateSC.trRhoF0F0(z1[0]*2, prop=prop)
logger.info("rhoF0F0_SC: " + str(rhoF0F0_SC))
deltaF0F0 = (rhoF0F0_SC-rhoF0F0_QM) / rhoF0F0_QM
logger.info("error: " + "{:.2f}%".format(deltaF0F0*100))

rhodelta_QM = RatePert.trRhodelta(z1).real
logger.info("rhodelta_QM: " + str(rhodelta_QM))
rhodelta_SC = RateSC.trRhodelta(z1[0])
logger.info("rhodelta_SC: " + str(rhodelta_SC))
deltarhodelta = (rhodelta_SC - rhodelta_QM) / rhodelta_QM
logger.info("error: " + "{:.2f}%".format(deltarhodelta*100))


logger.info("Two state")
rhoF1_QM = RatePert.trRhoF1(z1)
logger.info("rhoF1_QM: " + str(rhoF1_QM))
rhoF1_SC = RateSC.trRhoF1(z1[0])
logger.info("rhoF1_SC: " + str(rhoF1_SC))
deltaF1 = ((rhoF1_SC - rhoF1_QM) / rhoF1_QM).real
logger.info("error: " + "{:.2f}%".format(deltaF1*100))
