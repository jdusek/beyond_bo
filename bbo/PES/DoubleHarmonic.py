import numpy as np
from polylib.PES.Eckart import Eckart
from polylib.units import atomic
from polylib.numderiv import hess4, grad4
from polylib.PES._base import BasePES
from bbo.PES._utils import LowerPES, UpperPES

class DoubleHarmonic(BasePES):

	def __init__(self, gap=.05, mass=1, Vddagger=0.25, omega_0=1.j, omega_1=1.) -> None:
		"""
		Two harmonic potentials.

		Lower potential:
		V_0(x) = 1/2 * m * omega_0**2 * x**2 + Vddagger

		Upper potential:
		V_1(x) = 1/2 * m * omega_1**2 * x**2 + epsilon

		where epsilon = Vddagger + gap

		omega_0 has to be imaginary

		"""
		self.UNITS = atomic()
		self.mass = mass
		self.gap = gap
		self.Vddagger = Vddagger
		self.epsilon = self.gap + self.Vddagger
		self.omega_1 = omega_1
		self.omega_0 = omega_0
		# Periods of the harmonic oscillators
		self.T0 = (2*np.pi / self.omega_0 ).imag
		self.T1 = 2*np.pi / self.omega_1

		if omega_0.real != 0:
			raise Exception("omega_0 must be purely imaginary")
		# -2B/a**2 = m * omega_0**2
		# a**2 = -2B / m * omega_0**2
		self.a = ( -2*self.Vddagger / (self.mass * self.omega_0**2) ).real
		#print(self.a)
		self.Eck = Eckart(m = self.mass, a = self.a, B = self.Vddagger)

	def _wrapper(self, x):
		res = np.array(x)
		if res.ndim == 0:
			res = res[None]
		return res

	def _lower(self, x, Eckart=False):
		res = .5*self.omega_0**2 * x**2 + self.Vddagger
		if Eckart:
			res = self.Eck.vpotential(x)
		return res

	def _upper(self, x):
		return .5*self.omega_1**2 * x**2

	def potential(self, x, Eckart=False):
		x = self._wrapper(x)
		res = np.zeros((2, x.shape[0],) )
		res[0] = self._lower(x, Eckart=Eckart).real
		res[1] = self._upper(x) + self.epsilon
		return res

	def gradient(self, x, Eckart=False):
		x = self._wrapper(x)
		sh = (2, x.shape[0]) + (1,)
		res = np.zeros(sh)
		h = 1e-3
		res[0] = grad4(x, lambda x: self.potential(x, Eckart=Eckart)[0], h=h)
		res[1] = grad4(x, lambda x: self.potential(x)[1], h=h)
		return 0

	def hessian(self, x, Eckart=False):
		x = self._wrapper(x)
		sh = (2, x.shape[0]) + (1, 1)
		res = np.zeros(sh)
		h = 1e-3
		res[0] = hess4(x, lambda x: self.potential(x, Eckart=Eckart)[0], h=h)
		res[1] = hess4(x, lambda x: self.potential(x)[1], h=h)
		return res

	def first_order_coupling(self, x):
		"""
		Invented 1st-order coupling.
		"""
		a = 1./2.  * self.omega
		res = np.exp(-a * x**2)
		return res


class Forbidden(DoubleHarmonic):

	def __init__(self, gap=0.05, mass=1, Vddagger=0.25, omega_0=1j, omega_1=1) -> None:
		super().__init__(gap, mass, Vddagger, omega_0, omega_1)

	def xtraj(self, t, xa, xb, ttot, surface="Lower", dynamics="Stable"):
		"""
		Classical trajectory on the harmonic oscillator.
		xa: starting position
		xb: ending position
		t: time (duration of the path)
		surface: on which surface
		dynamics: Stable means it's oscillatory, Unstable means it's "leaky"

		Note: the trajectory is only uniquely determined, if t < T (the HO period).
		"""
		if surface == "Lower":
			omega = self.omega_0
			omega *= -1.j
		elif surface == "Upper":
			omega = self.omega_1
		else:
			raise Exception("Surface has to be Upper or Lower")

		if dynamics == "Stable":
			res = (xa*np.sin(omega*t) + xb*np.sin(omega*(ttot-t)))
			res /= np.sin(omega*ttot)
		elif dynamics == "Unstable":
			res = (xa*np.sinh(omega*t) + xb*np.sinh(omega*(ttot-t)))
			res /= np.sinh(omega*ttot)
		else:
			raise Exception("Dynamics have to be either Real or Imaginary")
		return res

	def S_harm(self, xi, xf, tau, surface="Lower", dynamics="Stable"):
		if surface == "Lower":
			omega = self.omega_0
			omega *= -1.j
		elif surface == "Upper":
			omega = self.omega_1
		else:
			raise Exception("Surface has to be Upper or Lower")

		mass = self.mass

		if dynamics == "Stable":
			res = mass * omega * ( (xi**2 + xf**2)*np.cos(omega*tau) - 2*xi*xf)
			res /= (2 * np.sin(omega*tau))
		elif dynamics == "Unstable":
			res = mass * omega * ( (xi**2 + xf**2)*np.cosh(omega*tau) - 2*xi*xf)
			res /= (2 * np.sinh(omega*tau))
		else:
			raise Exception("Dynamics have to be either Stable or Unstable")
		return res

	def S0(self, xi, xf, tau):
		res = self.S_harm(xi, xf, tau, surface="Lower", dynamics="Stable")
		return res

	def S1(self, xi, xf, tau):
		res = self.S_harm(xi, xf, tau, surface="Upper", dynamics="Unstable")
		return res

	def d2Sdx2_harm(self, xi, xf, tau, surface="Lower", dynamics="Stable"):
		if surface == "Lower":
			omega = self.omega_0
			omega *= -1.j
		elif surface == "Upper":
			omega = self.omega_1

		mass = self.mass

		if dynamics == "Stable":
			res = mass * omega
			res /= np.sin(omega*tau)
		elif dynamics == "Unstable":
			res = mass * omega
			res /= np.sinh(omega*tau)
		else:
			raise Exception("Dynamics have to be either Stable or Unstable")
		return res

	def d2S0dx2(self, xi, xf, tau):
		res = self.d2Sdx2_harm(xi, xf, tau, surface="Lower", dynamics="Stable")
		return res

	def d2S1dx2(self, xi, xf, tau):
		res = self.d2Sdx2_harm(xi, xf, tau, surface="Upper", dynamics="Unstable")
		return res

	def K0(self, xi, xf, tau):
		"""
		https://en.wikipedia.org/wiki/Propagator#Basic_examples:_propagator_of_free_particle_and_harmonic_oscillator
		"""
		hbar = self.UNITS.hbar
		S = self.S0(xi, xf, tau)
		d2Sdx2 = self.d2S0dx2(xi, xf, tau)
		res = np.sqrt(d2Sdx2 / (2*np.pi * hbar)) * np.exp(-S / hbar)
		return res

	def K1(self, xi, xf, tau):
		hbar = self.UNITS.hbar
		S = self.S1(xi, xf, tau)
		d2Sdx2 = self.d2S1dx2(xi, xf, tau)
		res = np.sqrt(d2Sdx2 / (2*np.pi * hbar)) * np.exp(-S / hbar)
		return res


### Parameters
mass = 1.
Vddagger = .025
a_Joe = .5
Eck_1d = Eckart(m = mass, a = a_Joe, B = Vddagger)
gap = .005 #Energy gap between lower and upper PES
omega_0 = .5j
omega_1 = .25

APES = DoubleHarmonic(gap=gap, mass=mass, Vddagger=Vddagger, omega_0=omega_0, omega_1=omega_1)
APES0 = LowerPES(APES, mode="adiabatic")
APES1 = UpperPES(APES, mode="adiabatic")

F = Forbidden(gap=gap, mass=mass, Vddagger=Vddagger, omega_0=omega_0, omega_1=omega_1)

if __name__ == "__main__":
	import matplotlib.pyplot as plt
	if False:
		print(APES.potential(0))
		print(APES.hessian(0))

	# Setting the dividing surfaces:
	xa = -1
	xb = 1.5

	# 1. real-time dynamics on the upper surface
	tmax = F.T1 / 2
	tarr = np.linspace(0, tmax, num=1000)
	xarr = F.xtraj(tarr, xa, xb, tmax, surface="Upper", dynamics="Stable")
	if True:
		fig, ax = plt.subplots()
		ax.plot(tarr, xarr, label="x(t)")
		ax.set_ylabel("x")
		ax.set_xlabel("t")
		ax.legend()
		plt.title("real-time dynamics on the upper surface")
		plt.show()

	# 2. imaginary-time dynamics on the lower surface
	taumax = F.T0 / 2
	tau_arr = np.linspace(0, taumax, num=1000)
	xarr = F.xtraj(tau_arr, xa, xb, taumax, surface="Lower", dynamics="Stable")
	if True:
		fig, ax = plt.subplots()
		ax.plot(tarr, xarr, label="x(tau)")
		ax.set_ylabel("x")
		ax.set_xlabel("tau")
		ax.legend()
		plt.title("imaginary-time dynamics on the lower surface")
		plt.show()

	# 3. imaginary-time dynamics on the upper surface
	taumax = F.T1
	tau_arr = np.linspace(0, taumax, num=1000)
	xarr = F.xtraj(tau_arr, xa, xb, taumax, surface="Upper", dynamics="Unstable")
	if True:
		fig, ax = plt.subplots()
		ax.plot(tarr, xarr, label="x(tau)")
		ax.set_ylabel("x")
		ax.set_xlabel("tau")
		ax.legend()
		plt.title("imaginary-time dynamics on the lower surface")
		plt.show()


	exit()


	gridl = 1

	x = np.linspace(-gridl, gridl, num=1000)
	y1 = APES.potential(x)[0,]
	y12 = APES.potential(x, Eckart=True)[0,]
	y2 = APES.potential(x)[1,]

	fig, ax = plt.subplots()
	ax.plot(x, y1, label="V_0")
	ax.plot(x, y12, label="V_0 Eck")
	ax.plot(x, y2, label="V_1")
	ax.set_ylabel("V(x)")
	ax.set_xlabel("x")
	ax.legend()
	plt.show()
