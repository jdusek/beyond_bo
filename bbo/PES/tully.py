import numpy as np
from polylib.PES import diabatic #Need latest dev-future
from polylib.PES import nonadiabatic #Need latest dev-future
from polylib.PES._base import BasePES
from polylib.PES.Eckart import Eckart
from polylib.units import Energy, atomic
from bbo.PES._utils import LowerPES, UpperPES

import matplotlib.pyplot as plt
import os.path



#Using a library potential

## Joes parameters
Vddagger = .025
a_Joe = .5
B_Joe = 2 * Vddagger
Delta_Joe = Vddagger
gamma_Joe = a_Joe * 3./4.
## Conversion to polylib parameters
A = B_Joe
B = 1./a_Joe
C = Delta_Joe
D = 1./(2.* gamma_Joe)
DPES = diabatic.Tully1Analytic(A, B, C, D)
##Changing B to 2 messes up the DVR
DPES.UNITS = atomic()

##Shifting the potential up to be always positive
#TODO maybe it would be more elegant to solve this via inheritance?
def shifted_potential(self, x):
	res = self.potential_old(x)
	res[0, 0] += self.A
	res[1, 1] += self.A
	return res

DPES.potential_old = DPES.potential
DPES.potential = shifted_potential.__get__(DPES, diabatic.Tully1Analytic)

##DPES.test = test.__get__(DPES, diabatic.Tully1Analytic)
#
#x = 5
##print(DPES.test(x))
#print(DPES.potential_old(x))
#print(DPES.potential(x))
#
#exit()


##Adiabatic representation
APES = nonadiabatic.TwoLevelAdiabaticRepresentation(DPES)
APES.UNITS = atomic()

def d(x):
	r"""The derivative coupling vector (here scalar, because
	we are in 1D)
	"""
	res = APES.first_order_coupling(x)
	return res

def Lambda2(x):
	hbar = 1
	res = d(x)[0, 1] * d(x)[1, 0]
	res *= - hbar**2 / (2*APES.mass)
	return res


class LowerPES_BH():
	"""Born-Huang approximation - Lambda2 added"""
	def __init__(self, PES):
		self.PES = PES
		self.UNITS = atomic()
		self.mass = PES.mass

	def potential(self, x):
			V = self.PES.potential(x)[0]
			L = Lambda2(x)
			return V + L

APESBH = LowerPES_BH(APES)
APES0 = LowerPES(APES, mode="adiabatic")
APES1 = UpperPES(APES, mode="adiabatic")
DPES0 = LowerPES(DPES, mode="diabatic")
DPES1 = UpperPES(DPES, mode="diabatic")

##Conversion to Joe's parameters

Eck_1d = Eckart(m = APES0.mass, a = a_Joe, B = Vddagger)
Eck_1d.UNITS = atomic()
class VectorEckart(Eckart):
	def __init__(self, m, a, B, A=0):
		super().__init__(m, a, B, A)
		self.UNITS = atomic()

	def potential(self, x):
		V = super().potential(x)
		return np.array(V)

	def gradient(self, x):
		G = super().gradient(x)
		G = np.array([G])
		return G

	def hessian(self, x):
		H = super().hessian(x)
		H = np.array([[H]])
		return H
Eck = VectorEckart(m = APES0.mass, a = a_Joe, B = Vddagger)
plotEckart = True

#Testing hessians
if 0:
	print("APES0")
	TESTPES = APES0
	# x = np.array(0)
	x = np.array([0])
	P = TESTPES.potential(x)
	G = TESTPES.gradient(x)
	H = TESTPES.hessian(x)
	print("x: " + str(x.shape))
	print("V: " + str(P.shape))
	print("G: " + str(G.shape))
	print("H: " + str(H.shape))
	print(P)
	print(G)
	print(H)
	# exit()

if 0:
	print("Eck")
	TESTPES = Eck
	# x = np.array(0)
	x = np.array([0])
	P = TESTPES.potential(x)
	G = TESTPES.gradient(x)
	H = TESTPES.hessian(x)
	print("x: " + str(x.shape))
	print("V: " + str(P.shape))
	print("G: " + str(G.shape))
	print("H: " + str(H.shape))
	print(P)
	print(G)
	print(H)
	exit()



if plotEckart:
	class custom_pes(BasePES):
		def __init__(self):
			self.UNITS = atomic()
			self.mass = Eck.mass

		def potential(self, x):
			return Eck.potential(x)
else:
	class custom_pes(BasePES):
		def __init__(self):
			self.UNITS = atomic()
			self.mass = APES0.mass

		def potential(self, x):
			return APES0.potential(x)


if __name__ == "__main__":
	#Energy units converter:
	#https://www.weizmann.ac.il/oc/martin/tools/hartree.html
	barrier_height = Energy(Eck.B, 'hartree' )
	print("Barrier Height/(kcal/mol): " + str(barrier_height.get('kcal/mol')))

	#Plotting
	l = 15.
	nplot = 1000
	x = np.linspace(-l,l,nplot)
	y0D = np.zeros_like(x)
	y1D = np.zeros_like(x)
	y0A = np.zeros_like(x)
	y1A = np.zeros_like(x)
	y0ALambda = np.zeros_like(x)
	yE = np.zeros_like(x)
	d_vec = np.zeros_like(x)
	for i in range(len(x)):
		y0D[i] = DPES0.potential(x[i])
		y1D[i] = DPES1.potential(x[i])
		y0A[i] = APES0.potential(x[i])
		y1A[i] = APES1.potential(x[i])
		y0ALambda[i] = APESBH.potential(x[i])
		yE[i] = Eck.potential(x[i])
		d_vec[i] = APES.first_order_coupling(x[i])[0,1]

	d_vec = d_vec/d_vec[nplot//2] * 2.*Eck.B

	fig, ax = plt.subplots()
	if plotEckart:
		ax.plot(x, yE, linewidth=4, label="Eckart")
	ax.plot(x, y0D, label="DPES0")
	ax.plot(x, y1D, label="DPES1")
	ax.plot(x, y0A, linewidth=2, label="APES0")
	ax.plot(x, y1A, linewidth=2, label="APES1")
	ax.plot(x, y0ALambda, linewidth=2, label="APES0 + Lambda2")

	ax.plot(x, d_vec, linestyle="dashed", linewidth=2, label="d_01 (rescaled)")

	instfile = "optimized_inst.pkl"
	if os.path.isfile(instfile) and 0:
		data = np.load(instfile, allow_pickle=True, encoding="latin1")
		instx = data['x']
		#print(np.shape(instx))
		insty = np.zeros_like(instx)
		for i in range(len(instx)):
			if plotEckart:
				insty[i] = Eck.potential(instx[i])
			else:
				insty[i] = APES0.potential(instx[i])
		ax.plot(instx, insty, 'o-', label="instanton")

	ax.set_ylabel("V/Ha")
	ax.set_xlabel("x/A")
	ax.legend()
	plt.show()