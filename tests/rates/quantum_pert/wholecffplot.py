import numpy as np
from scipy import integrate
import matplotlib.pyplot as plt


name = "./cffdata/cffs.pkl"
print("Loading cff data from " + str(name))
cffdata = np.load(name, allow_pickle=True, encoding="latin1")

cfftime = cffdata['cfftime']
cfft0 = cffdata['cfft0']
cfft1 = cfft0 + cfftime
nquad = cffdata['nquad']
timeArray = np.arange(cfft0, cfft1, cfftime/nquad)

cffBO = cffdata['cffBO']
cffBO_t = cffdata['cffBO_t']

cffAExact_i = cffdata['cffAExact_i']
cffAExact_r = cffdata['cffAExact_r']

cffDExact_i = cffdata['cffDExact_i']
cffDExact_r = cffdata['cffDExact_r']

cff0000_i = cffdata['cff0000_i']
cff0000_r = cffdata['cff0000_r']

cff0101_i = cffdata['cff0101_i']
cff0101_r = cffdata['cff0101_r']

cff1001_ii = cffdata['cff1001_ii']
cff1001_ri = cffdata['cff1001_ri']
cff1001_rr = cffdata['cff1001_rr']

cff1100_ii = cffdata['cff1100_ii']
cff1100_ri = cffdata['cff1100_ri']
cff1100_rr = cffdata['cff1100_rr']

cff2000_ii = cffdata['cff2000_ii']
cff2000_ri = cffdata['cff2000_ri']
cff2000_rr = cffdata['cff2000_rr']

# cff1000_rii = cffdata['cff1000_rii']
# cff1000_rir = cffdata['cff1000_rir']
# cff1000_rri = cffdata['cff1000_rri']
# cff1000_rrr = cffdata['cff1000_rrr']

# cff1010_rii = cffdata['cff1010_rii']
# cff1010_rir = cffdata['cff1010_rir']
# cff1010_rri = cffdata['cff1010_rri']
# cff1010_rrr = cffdata['cff1010_rrr']

Zr = cffdata['Zr']
beta = cffdata['beta']

print("beta: " + str(beta))
print("nquad: " + str(nquad))

reduce2d = True
reduce3d = False
threed   = False
heatmaps = True
plotcff  = True

def integratecff(cff, beta=beta, nquad=33):

    if cff.ndim == 2:
        t1length = cff.shape[0]
        t2length = cff.shape[1]
        t1 = np.arange(cfft0, cfft1, cfftime/t1length)
        res = np.zeros(t1length, dtype=complex)
        for i in range(t1length):
            currt1 = np.array( [ t1[i] ] )
            res[i] += integrate.simps(cff[i, : i + 1], dx=currt1[0]/(i+1))

    if cff.ndim == 3:
        #Is probably wrong because of beta - t1
        t1length = cff.shape[0]
        t2length = cff.shape[1]
        t1 = np.arange(cfft0, cfft1, cfftime/t1length)
        
        res1 = np.zeros((t1length, t2length), dtype=complex)
        for i in range(t1length):
            for j in range(t2length):
                currt1 = np.array( [ t1[i] ] )
                res1[i, j] += integrate.simps(cff[i, j], dx=currt1[0]/(i+1))
        
        res = integratecff(res1, beta=beta, nquad=nquad)
    return res

if reduce2d:
    cff1001reduced  = integrate.simps(cff1001_ri, axis=-1, dx=(beta/2)/nquad )
    cff1001reduced += integratecff(cff1001_rr)
    
    cff1100reduced  = integrate.simps(cff1100_ri, axis=-1, dx=(beta/2)/nquad)
    cff1100reduced += integratecff(cff1100_rr)

    cff2000reduced  = integrate.simps(cff2000_ri, axis=-1, dx=(beta/2)/nquad)
    cff2000reduced += integratecff(cff2000_rr)

if reduce3d:
    #Integrating cff 1000
    rii  = integrate.simps(cff1010_rii, axis=-1, dx=(beta/2)/nquad)
    cff1010reduced = integrate.simps(rii, axis=-1, dx=(beta/2)/nquad)

    rir = integrate.simps(cff1010_rir, axis=-2, dx=(beta/2)/nquad)
    cff1010reduced += integratecff(rir)
    
    rri = integrate.simps(cff1010_rri, axis=-1, dx=(beta/2)/nquad)
    cff1010reduced += integratecff(rri)

    rrr = integrate.simps(cff1010_rrr, axis=-2, dx=(beta/2)/nquad)

    #Integrating cff 1000


kAExact = integrate.simps(cffAExact_r.real, timeArray) / Zr
print("kAExact: " + str(kAExact))
k0000 = integrate.simps(cff0000_r.real, timeArray) / Zr
print("k0000:   " + str(k0000))
k0101 = integrate.simps(cff0101_r.real, timeArray) / Zr
print("k0101:   " + str(k0101))
if reduce2d: 
    k2000 = integrate.simps(cff2000reduced.real, timeArray) / Zr
    print("k2000:   " + str(k2000))

    k1001 = integrate.simps(cff1001reduced.real, timeArray) / Zr
    print("k1001:   " + str(k1001))

    k1100 = integrate.simps(cff1100reduced.real, timeArray) / Zr
    print("k1100:   " + str(k1100))
    
    print("k0000 + 2*k2000: " + str(k0000 + k2000 * 2))
if reduce3d: 
    k1010 = integrate.simps(cff1010reduced.real, timeArray) / Zr
    print("k1010:   " + str(k1010))



if threed:
    i = nquad // 2
    plt.imshow( cff1010_rii[...,i].real, origin='lower')
    plt.title("cff 1010(.,.," + str(i) + ")")
    plt.xlabel("z1")
    plt.ylabel("z2")
    plt.colorbar()
    plt.show()

if heatmaps:
    def plot3(cff1, cff2, cff3, name):
        fig, ax = plt.subplots(1, 3, figsize=(12, 5))
        im = ax[0].imshow( cff1.real)
        ax[0].set_title(name + " ii")
        ax[0].set_xlabel("z2")
        ax[0].set_ylabel("z1")
        plt.colorbar(im, ax=ax[0])

        im = ax[1].imshow( cff2.real)
        ax[1].set_title(name + "ri")
        ax[1].set_xlabel("z2")
        ax[1].set_ylabel("z1")
        plt.colorbar(im, ax=ax[1])

        im = ax[2].imshow( cff3.real)
        ax[2].set_title(name + "rr")
        ax[2].set_xlabel("z2")
        ax[2].set_ylabel("z1")
        plt.colorbar(im, ax=ax[2])

        plt.show()

    plot3(cff1001_ii.real, cff1001_ri.real, cff1001_rr.real, "cff 1001")
    plot3(cff1100_ii.real, cff1100_ri.real, cff1100_rr.real, "cff 1100")
    plot3(cff2000_ii.real, cff2000_ri.real, cff2000_rr.real, "cff 2000")

#Check the imaginary terms
#Real: symmetric
#Imag: antisymmetric
#Explain what happens at time 0
if plotcff:
    fig, ax = plt.subplots(1, 2, figsize=(12,5))
    ax[0].axhline(linewidth=2, color="black")
    ax[0].axvline(linewidth=2, color="black")
    ax[0].plot(timeArray, cffAExact_r.real, linewidth=3, label = "Adiabatic exact")
    ax[0].plot(timeArray, cffDExact_r.real, label = "Diabatic exact")
    ax[0].plot(cffBO_t, cffBO.real, label = "B-O")
    ax[0].plot(timeArray, cff0000_r.real, linewidth=3, label="0000")
    ax[0].plot(timeArray, cff0101_r.real, label="0101")
    if reduce2d: ax[0].plot(timeArray, cff1001reduced.real, label="1001")
    if reduce2d: ax[0].plot(timeArray, cff1100reduced.real, label="1100")
    if reduce2d: ax[0].plot(timeArray, cff2000reduced.real, label="2000")
    if reduce3d: ax[0].plot(timeArray, cff1010reduced.real, label="1010")
    everyContrib2d = cff0000_r.real + cff0101_r.real + 2*cff2000reduced.real + 2*cff1001reduced.real + 2*cff1100reduced.real
    if reduce2d: ax[0].plot(timeArray, everyContrib2d, label="first-order all (2d)")
    ax[0].set_xlabel("t")
    ax[0].set_ylabel("Re c_ff ")
    ax[0].set_title("Re cffs")
    ax[0].legend()

    ax[1].axhline(linewidth=2, color="black")
    ax[1].axvline(linewidth=2, color="black")
    ax[1].plot(cffBO_t, cffBO.imag, label = "B-O")
    ax[1].plot(timeArray, cffAExact_r.imag, label = "Adiabatic exact")
    ax[1].plot(timeArray, cffDExact_r.imag, label = "Diabatic exact")
    ax[1].plot(timeArray, cff0000_r.imag, label="0000")
    ax[1].plot(timeArray, cff0101_r.imag, label="0101")
    if reduce2d: ax[1].plot(timeArray, cff1001reduced.imag, label="1001")
    if reduce2d: ax[1].plot(timeArray, cff1100reduced.imag, label="1100")
    if reduce2d: ax[1].plot(timeArray, cff2000reduced.imag, label="2000")
    if reduce3d: ax[1].plot(timeArray, cff1010reduced.imag, label="1010")
    #if reduce2d: ax[1].plot(timeArray, cff0000.imag + 2*cff2000reduced.imag, label="cff0000 + 2*cff2000")
    #all = cff0000.imag + cff0101.imag + 2*cff2000reduced.imag + 2*cff1001reduced.imag + 2*cff1100reduced.imag
    #if reduce2d and reduce3d: ax[1].plot(timeArray, all, label="first-order all")
    ax[1].set_xlabel("t")
    ax[1].set_ylabel("Im c_ff ")
    ax[1].set_title("Im cffs")
    ax[1].legend()
    plt.show()
