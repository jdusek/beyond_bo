import numpy as np
from bbo.PES.DoubleHarmonic import APES, APES0, APES1
from bbo.rates import print_crossover, SemiClassicTwoState, SemiClassicOneState
from polylib.WKB import System as WKBSystem, trajectory_id, account_for_bounces

print_crossover(APES0)
beta = 20
print("beta: " + str(beta))

#TODO: get rid of this
gridl = 100

xd = np.array([-.1, .12])

#RateSC = SemiClassicOneState(APES0, beta, xdagger=xd)
#RateSC = SemiClassicTwoState(APES0, APES1, APES, beta, gridl)

traj = trajectory_id(xd[0], xd[-1], bounceno=1)

#K0 = RateSC.K_vV_0(traj, beta)
#K1 = RateSC.K_vV_1_fp(traj, beta)

E = 0
