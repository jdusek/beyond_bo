#!/usr/bin/env python
"""
Created on Sat Jun  2 15:33:19 2018

@author: gt317

Discrete variable representation approach to solving the Schroedinger
equation in low-dimensional systems.

"""

from __future__ import division, print_function
import numpy as np
from scipy import linalg

class OpenInterval(object):

    def __init__(self, PES, lb, ub, npts, E_max, E_cut=None):
        """Solve the one-dimensional Schroedinger equation using the
     discrete variable representation by Miller et al. for a potential
     on the open (–∞, +∞) symbol. See DOI 10.1063/1.462100

        Args:
            PES (object): the potential energy surface
            lb (float): lower bound of the coordinate grid
            ub (float): upper bound of the coordinate grid
            npts(int): number of points on the grid
            E_max (float): maximum energy eigenvalue to keep after diagonalisation
            E_cut (float, optional): energy cut-off for the grid, default is None.

        Notes:
            Minimal requirements for PES:
                -a method PES.potential(x) that takes
                   x scalar and returns a scalar energy
                -has a UNITS atrribute
                -has a mass attribute
        """

        self.PES = PES
        self.UNITS = PES.UNITS
        self.hbar = self.UNITS.hbar
        self.m = self.PES.mass
        self.E_max = E_max
        self.E_cut = np.inf if E_cut is None else E_cut
        self.lb = lb
        self.ub = ub
        self.grid, self.hh = np.linspace(lb, ub, npts, retstep=True)
        self.potgrid = np.asarray([PES.potential(x) for x in self.grid])
        self.mask = self.potgrid < self.E_cut
        self.masked_grid = self.grid[self.mask]
        self.idx = np.arange(len(self.masked_grid))
        self.masked_pot = self.potgrid[self.mask]
        self.PE = np.diag(self.masked_pot)
        self.KE = self.ke_matrix()
        self.HH = self.KE + self.PE
        self.diagonalise()

    def ke_matrix(self):
        diff = self.idx[:,None] - self.idx[None,:]
        sgn = 1 - 2*np.mod(diff,2)
        np.fill_diagonal(diff, 1.) #avoid division by zero
        ans = 2* sgn /diff**2
        np.fill_diagonal(ans, np.pi**2 / 3)
        ans *= self.hbar**2  / (self.hh**2)
        ans *= 1./(2*self.m)
        return ans

    def diagonalise(self):
        """Diagonalise the Hamiltonian matrix
        """
        self.evals, evecs = linalg.eigh(
            self.HH, overwrite_a=False,
            subset_by_value=[-np.inf, self.E_max])
        self.evecs = evecs.T
        # Normalisation factors:
        N = self.integrate(self.evecs*self.evecs, axis=-1)
        self.evecs /= np.sqrt(N[:, None])

    def integrate(self, integrand, *args, **kwargs):
        """Calculate a numerical integral in accordance with the
        underlying quadrature rule.
        """
        return np.sum(self.hh * integrand, axis=kwargs.get('axis', -1))

    def flux(self, xdagger, method='commutator'):
        test = np.abs(self.masked_grid-xdagger)
        idx = np.argmin(np.abs(self.masked_grid-xdagger))
        if test[idx] > self.hh:
            raise RuntimeError("Dividing surface is outside the DVR grid range!")
        xdagger = self.masked_grid[idx]
        method = method.lower()
        if method == 'commutator':
            side = np.where(self.masked_grid > xdagger, 1.0, 0.0)
            side[idx] = 0.5
            F = 1j/self.hbar * self.KE * (side[:,None] - side[None,:])
        elif method == 'direct':
            v = np.zeros_like(self.masked_grid)
            v[idx] = 1/self.hh
            j = self.idx - idx
            j[idx] = 1 # avoid division by zero
            d = (1-2*(np.mod(j,2)))/(self.hh**2 * j)
            d[idx] = 0
            F = self.hh * self.hbar / (2j * self.m) * (v[:,None]*d[None,:] - d[:,None]*v[None,:])
        else:
            raise RuntimeError(f'Unknown commutator calculation method {method}.')
        return F, xdagger

if __name__ == '__main__':

    import matplotlib.pyplot as plt

    class Units(object):
        hbar = 1.0

    class Harmonic(object):
        UNITS = Units()
        mass = 1.0
        def potential(self, x):
            return x**2/2

    PES = Harmonic()
    #DVR = OpenInterval(PES, -7, 7, npts=65, E_max=20)
    #print(DVR.evals)
    #DVR = OpenInterval(PES, -7, 7, npts=65, E_max=100)
    #print(DVR.evals)
    #plt.plot(DVR.masked_grid, DVR.evecs[-1])
    #plt.show()
    for n in [65, 129, 257, 513]:
        DVR = OpenInterval(PES, -15, 15, npts=n, E_max=20, E_cut=60)
        i,f = 0,1
        s = 0  # shift dividing surface from optimal position
        xdagger = s*30/64
        print(np.linalg.multi_dot([DVR.evecs[i],DVR.flux(xdagger)[0],DVR.evecs[f]])*DVR.hh, end="\t")
        print(np.linalg.multi_dot([DVR.evecs[i],DVR.flux(xdagger, method='direct')[0],DVR.evecs[f]])*DVR.hh)
    print(np.sum(DVR.hh * DVR.evecs[0]**2))
    print(DVR.evals)
    plt.plot(DVR.grid, DVR.potgrid, 'k-')
    for i in range(min(10, len(DVR.evals))):
        plt.plot(DVR.masked_grid, DVR.evecs[i]+DVR.evals[i])
    plt.ylim([-2.5, 11.0])
    plt.show()