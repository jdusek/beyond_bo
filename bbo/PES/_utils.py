import numpy as np

#Classes with just one of the potentials
class LowerPES():
	def __init__(self, PES, mode):
		self.PES = PES
		self.UNITS = PES.UNITS
		self.mass = PES.mass
		self.mode = mode

	def potential(self, x):
		if self.mode == "adiabatic":
			return self.PES.potential(x)[0]
		elif self.mode == "diabatic":
			return self.PES.potential(x)[0,0]

	def gradient(self, x):
		if self.mode == "adiabatic":
			G = np.array(
				#[self.PES.gradient(x)[0]]
				self.PES.gradient(x)[...,0]
			)
			# if G.shape == ():
			#     G = np.array([G])
		else:
			#G = self.PES.gradient(x)[0, 0]
			raise NotImplementedError
		return G

	def hessian(self, x):
		if self.mode == "adiabatic":
			H = self.PES.hessian(x)[..., 0]
		else:
			#H = self.PES.hessian(x)[0, 0]
			raise NotImplementedError
		return H


class UpperPES():
	def __init__(self, PES, mode):
		self.PES = PES
		self.UNITS = PES.UNITS
		self.mass = PES.mass
		self.mode = mode

	def potential(self, x):
		if self.mode == "adiabatic":
			return self.PES.potential(x)[1]
		elif self.mode == "diabatic":
			return self.PES.potential(x)[1,1]
