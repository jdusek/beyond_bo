import numpy as np
from scipy.interpolate import CubicSpline as CS
from scipy import integrate
import matplotlib.pyplot as plt

from bbo.PES.tully import APES, APES0, APES1
from bbo.dvr.tully import dvr0, dvr1, gridl


def dvrvalidate(dvr=dvr1, k=0, plot=True, verbose=False, caption_add=""):
	"""
	Checks whether the time-independent quantum evolution equation (TIQEE)
	works for the solution of dvr.
	dvr: returned from the previous calculation
	k: the eigenvector we check
	plot: shuold the LHS and RHS of the equation be plotted?
	verbose: print some random statistics
	"""
	hbar = 1.

	##Testing normalisation
	psi = CS(dvr.masked_grid, dvr.evecs[k])

	def square(x):
		res = psi(x)
		with np.errstate(all='ignore'):
			res = np.real(res)
		res = res**2
		return res


	norm = integrate.quad(square, -gridl, gridl)
	print("Norm, should be 1: " + str(norm[0]))

	##Checking the time-independent quantum evolution equation:
	# H * psi = E * psi
	xtest = np.arange(-gridl,gridl,.01)

	RHS = dvr1.evals[k] * psi(xtest)

	# H = p**2 / 2m
	dder = psi.derivative(2)
	LHS = np.zeros_like(xtest)
	for i in range(len(xtest)):
		LHS[i] = -hbar**2/(2.*APES1.mass) * ( dder(xtest[i]) ) + APES1.potential(xtest[i]) * psi(xtest[i])


	if verbose:
		print("All close? " + str(np.allclose(LHS, RHS)))
		j = len(xtest)//2
		print(RHS[j])
		print(LHS[j])
		print(APES1.potential(xtest[j]) *psi(xtest[j]) )
		print(-1./(2*APES1.mass) * ( dder(xtest[j]) ))

	if plot:
		fig, ax = plt.subplots()
		ax.plot(xtest, LHS, label="LHS")
		ax.plot(xtest, RHS, label="RHS")
		ax.legend()
		fig.suptitle(caption_add + "LHS and RHS of TIQEE for eigenvector " + str(k))
		plt.show()

	return LHS, RHS


dvrvalidate(dvr1, k=0, caption_add="Adiabatic 1: ")
#TODO: print better states for Adiabatic 0: sample the full range of energies
dvrvalidate(dvr0, k=0, caption_add="Adiabatic 0: ")