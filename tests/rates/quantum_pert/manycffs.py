import numpy as np
from scipy import integrate
import matplotlib.pyplot as plt

beta = 1000
name = "./cffdata/cffsMany_50.pkl"
SC1100 = True
if SC1100:
    test_data = np.load('./cffdata/test_'+str(beta)+'.pkl', allow_pickle=True, encoding="latin1")
projection = True
generated3d = False
tmpNew = False
print("Loading cff data from " + str(name))
cffdata = np.load(name, allow_pickle=True, encoding="latin1")
dim = cffdata['dim']
lambda_pert = cffdata['lambda_pert']

cfftime = cffdata['cfftime']
cfft0 = cffdata['cfft0']
cfft1 = cfft0 + cfftime
imag2 = cffdata['imag2']
#imag2cutoff = cffdata['imag2cutoff']
real2 = cffdata['real2']
maxtime = cffdata['maxtime']
nquad1 = cffdata['nquad1']
nquad3 = cffdata['nquad3']
t1array = np.linspace(0, maxtime, num=nquad3)



#Nans are intentional and we want to ignore them in a sum
cffAExact_i = np.nan_to_num(cffdata['cffAExact_i'])
cffAExact_r = np.nan_to_num(cffdata['cffAExact_r'])

cffDExact_i = np.nan_to_num(cffdata['cffDExact_i'])
cffDExact_r = np.nan_to_num(cffdata['cffDExact_r'])

cffBO_i = cffdata['cffBO_i']
cffBO_r = cffdata['cffBO_r']

cffBH_i = cffdata['cffBH_i']
cffBH_r = cffdata['cffBH_r']


cff0000_i = np.nan_to_num(cffdata['cff0000_i'])
cff0000_r = np.nan_to_num(cffdata['cff0000_r'])

cff0101_i = np.nan_to_num(cffdata['cff0101_i'])
cff0101_r = np.nan_to_num(cffdata['cff0101_r'])

if projection:
    cff0101_g_i = np.nan_to_num(cffdata['cff0101_g_i'])
    cff0101_g_r = np.nan_to_num(cffdata['cff0101_g_r'])
    cff0101_e_i = np.nan_to_num(cffdata['cff0101_e_i'])
    cff0101_e_r = np.nan_to_num(cffdata['cff0101_e_r'])

cff1001_ii = np.nan_to_num(cffdata['cff1001_ii'])
cff1001_ri = np.nan_to_num(cffdata['cff1001_ri'])
cff1001_rr = np.nan_to_num(cffdata['cff1001_rr'])

cff1100_ii = np.nan_to_num(cffdata['cff1100_ii'])
cff1100_ri = np.nan_to_num(cffdata['cff1100_ri'])
cff1100_rr = np.nan_to_num(cffdata['cff1100_rr'])

cff2000_ii = np.nan_to_num(cffdata['cff2000_ii'])
cff2000_ri = np.nan_to_num(cffdata['cff2000_ri'])
cff2000_rr = np.nan_to_num(cffdata['cff2000_rr'])

if tmpNew: 
    cff1010_alt_iii = np.nan_to_num(cffdata['cff1010_alt_iii'])
    cff1010_alt_rii = np.nan_to_num(cffdata['cff1010_alt_rii'])
    cff1010_alt_rir = np.nan_to_num(cffdata['cff1010_alt_rir'])
    cff1010_alt_rri = np.nan_to_num(cffdata['cff1010_alt_rri'])
    cff1010_alt_rrr = np.nan_to_num(cffdata['cff1010_alt_rrr'])
    # print(cffdata['cff1010_alt_rri'])


if generated3d:
    cff1000_iii = np.nan_to_num(cffdata['cff1000_iii'])
    cff1000_rii = np.nan_to_num(cffdata['cff1000_rii'])
    #cff1000_rir = np.nan_to_num(cffdata['cff1000_rir'])
    cff1000_rri = np.nan_to_num(cffdata['cff1000_rri'])
    cff1000_rrr = np.nan_to_num(cffdata['cff1000_rrr'])

    cff1010_iii = np.nan_to_num(cffdata['cff1010_iii'])
    cff1010_rii = np.nan_to_num(cffdata['cff1010_rii'])
    cff1010_rir = np.nan_to_num(cffdata['cff1010_rir'])
    cff1010_rri = np.nan_to_num(cffdata['cff1010_rri'])
    cff1010_rrr = np.nan_to_num(cffdata['cff1010_rrr'])

Zr = cffdata['Zr']
beta = cffdata['beta']

print("beta: " + str(beta))
print("nquad1: " + str(nquad1))

reduce2d = True
plot2d = False
plot3d = False
reduce3d = generated3d
plotzero  = True

if plot2d:
    i = 20
    fig1, ax1 = plt.subplots(1, 2, figsize=(12, 5))
    fig2, ax2 = plt.subplots(1, 2, figsize=(12, 5))
    fig3, ax3 = plt.subplots(1, 2, figsize=(12, 5))

    fig1.suptitle('beta='+str(int(beta)))
    fig2.suptitle('beta='+str(int(beta)))
    fig3.suptitle('beta='+str(int(beta)))
    ax1[0].plot(imag2, cff1001_ri[i], label="cff 1001")
    ax1[1].plot(real2.imag, cff1001_rr[i], label="cff 1001")

    ax2[0].plot(imag2, cff1100_ri[i], label="cff 1100")
    ax2[1].plot(real2.imag, cff1100_rr[i], label="cff 1100")

    ax3[0].plot(imag2, cff2000_ri[i], label="cff 2000")
    ax3[1].plot(real2.imag, cff2000_rr[i], label="cff 2000")
    
    ax1[0].set_xlabel("tau")
    ax1[1].set_xlabel("t")
    ax2[0].set_xlabel("tau")
    ax2[1].set_xlabel("t")
    ax3[0].set_xlabel("tau")
    ax3[1].set_xlabel("t")

    ax1[0].set_ylabel("cff(0, tau)")
    ax1[1].set_ylabel("cff(0, tau)")
    ax2[0].set_ylabel("cff(0, tau)")
    ax2[1].set_ylabel("cff(0, tau)")
    ax3[0].set_ylabel("cff(0, tau)")
    ax3[1].set_ylabel("cff(0, tau)")

    ax1[0].legend
    ax1[1].legend()
    ax2[0].legend()
    ax2[1].legend()
    ax3[0].legend()
    ax3[1].legend()
    plt.show()

if plot3d:
    i = 30
    #print(len(real2.imag))
    #print(len(imag2.real))
    fig, ax = plt.subplots(1, 2, figsize=(12, 5))
    im0 = ax[0].imshow( cff1000_rii[i].real, extent = (imag2.real[0], imag2.real[-1], imag2.real[-1], imag2.real[0]))
    im1 = ax[1].imshow( cff1010_rii[i].real, extent = (imag2.real[0], imag2.real[-1], imag2.real[-1], imag2.real[0]))
    
    ax[0].set_xlabel("tau2")
    ax[0].set_ylabel("tau1")
    ax[0].set_title("cff1000(" + str(int(imag2[i].real)) + ", tau1, tau2)")

    ax[1].set_xlabel("tau1")
    ax[1].set_ylabel("tau2")
    ax[1].set_title("cff1010(" + str(int(imag2[i].real)) + ", tau1, tau2)")
    
    #ax[0].set_xticks([imag2.real[0], imag2.real[-1]])
    #ax[0].set_yticks([imag2.real[0], imag2.real[-1]])
    plt.colorbar(im0, ax=ax[0])
    plt.colorbar(im1, ax=ax[1])
    plt.show()


if reduce2d:
    cff1001_reduced = np.zeros(nquad3)
    cff1100_reduced = np.zeros(nquad3)
    cff2000_reduced = np.zeros(nquad3)
    for i in range(nquad3):
        cff1001_reduced[i]  = integrate.simps(cff1001_ri[i].real, imag2.real)
        cff1001_reduced[i] += integrate.simps(cff1001_rr[i].real, real2.imag)

        cff1100_reduced[i]  = integrate.simps(cff1100_ri[i].real, imag2.real)
        cff1100_reduced[i] += integrate.simps(cff1100_rr[i].real, real2.imag)

        cff2000_reduced[i]  = integrate.simps(cff2000_ri[i].real, imag2.real)
        cff2000_reduced[i] += integrate.simps(cff2000_rr[i].real, real2.imag)
    #print("2000: " + str(cff2000_reduced))
    #print("1001: " + str(cff1001_reduced))

if reduce3d:
    cff1000_reduced = np.zeros(nquad3)
    cff1010_reduced = np.zeros(nquad3)
    for i in range(nquad3):
        ##1000
        #rii
        generated3d = integrate.simps(cff1000_rii[i], imag2.real)
        cff1000_reduced[i]  = integrate.simps(generated3d.real, imag2.real)
        #rri
        #First integrate along the imaginary time, then along real time
        generated3d = integrate.simps(cff1000_rri[i], imag2.real)
        cff1000_reduced[i] += integrate.simps(generated3d.real, real2.imag)
        #rrr
        generated3d = integrate.simps(cff1000_rrr[i], real2.imag)
        cff1000_reduced[i] += integrate.simps(generated3d.real, real2.imag)

        ##1010
        #rii
        generated3d = integrate.simps(cff1010_rii[i], imag2.real)
        cff1010_reduced[i]  = integrate.simps(generated3d.real, imag2.real)
        #rri
        generated3d = integrate.simps(cff1010_rri[i], imag2.real)
        cff1010_reduced[i] += integrate.simps(generated3d.real, real2.imag)
        #rir
        generated3d = integrate.simps(cff1010_rir[i], real2.imag)
        if i == 0:
            # print(cff1010_rir[0])
            print(integrate.simps(generated3d.real, imag2.real))
        cff1010_reduced[i] += integrate.simps(generated3d.real, imag2.real)
        #rrr
        generated3d = integrate.simps(cff1010_rrr[i], real2.imag)
        cff1010_reduced[i] += integrate.simps(generated3d.real, real2.imag)

    #cff1010_reduced = 0
    #print("1000: " + str(cff1000_reduced))
    #print("1010: " + str(cff1010_reduced))

if tmpNew:
    #print(cff1010_alt_rir)
    cff1010_alt_reduced = cff1010_alt_rii + cff1010_alt_rri + cff1010_alt_rir + cff1010_alt_rrr
    #print(cff1010_alt_rii)
    #print(cff1010_alt_rri)
    #print(cff1010_alt_rir)
    #print(cff1010_alt_rrr)

    #print("1010: " + str(cff1010_reduced[0].real) )
    #print("1010 (alt): " + str(cff1010_alt_reduced[0].real) )
    #print(cffAExact_r[0])
    # exit()

cff_fo = cff0000_r.real.copy()
cff_fo += cff0101_r.real
if reduce2d:
    cff_fo += 2*(cff2000_reduced + cff1100_reduced + cff1001_reduced)

if reduce3d:
    cff_fo += 2*(cff1000_reduced) + cff1010_reduced


if projection:
    both = cff0101_e_r.real + cff0101_g_r.real
    # print(both[0])
    # print(cff0101_r.real[0])
    # print(cff0101_e_r.real)
    # print(cff0101_g_r.real)
    # exit()

if 0:
    k0000 = integrate.simps(cff0000_r, real2.imag)
    print("k0000: " + str(k0000.real))
    print("cff0000(0): " + str(cff0000_r[0].real))
    print("cff2000(0): " + str(cff2000_reduced[0].real))
    print("2000/0000: " + str(cff2000_reduced[0].real/cff0000_r[0].real))
    print("cff1100(0): " + str(cff1100_reduced[0].real))
    k1100 = integrate.simps(cff1100_reduced.real, real2.imag)
    print("k1100: " + str(k1100))
    print("k1100/k0000: " + str((k1100/k0000).real))
    plot1100 = True
    # print(cff1100_ii.shape)

    def cff1100_approx(x):
        h = imag2[1]-imag2[0]
        der = (cff1100_ii[0,1] - cff1100_ii[0,0]) / h
        A = cff1100_ii[0,0]
        B = -der / A
        res = A * np.exp(-B*x)
        return res
    
    if plot1100:
        fig, ax = plt.subplots()
        x = imag2.real
        y = cff1100_ii[0].real
        y_approx = cff1100_approx(x.real).real

        cff1100_int = integrate.simps(y.real, x)
        cff1100_approx_int = integrate.simps(y_approx.real, x)
        print("cff1100 int: " + str(cff1100_int))
        print("cff1100 int (approx): " + str(cff1100_approx_int))
        
        ax.plot(x, y, label="QM cff1100")
        ax.plot(x, y_approx, label="QM cff1100 approx")
        ax.set_xlabel("tau")
        ax.set_ylabel("cff1100(beta, tau)")
        ax.set_title("cffs, QM and SC")

        if SC1100:
            y_test = test_data['y']
            y_a_test = test_data['y_approx']
            x_test = test_data['x']
            ax.plot(x_test, y_test, label="SC cff1100")
            ax.plot(x_test, y_a_test, label="SC cff1100 approx")
            print("SC cff1100(0): " + str(y_test[0]))
            print("SC / QM: " + str(y_test[0].real/cff1100_ii[0,0].real))
        
        ax.legend()
        plt.show()
        #exit()



if plotzero:
    fig, ax = plt.subplots(1, 2, figsize=(18,5))
    ax[0].axhline(linewidth=2, color="black")
    ax[0].axvline(linewidth=2, color="black")
    ax[0].plot(t1array, cffAExact_r.real, linewidth=3, label = "Adiabatic exact")
    ax[0].plot(t1array, cff0000_r.real, linewidth=3, label="0000")
    if 0: ax[0].plot(t1array, cffBH_r.real, linewidth=2, label="Born-Huang", color="pink")
    ax[0].plot(t1array, cff0101_r.real, label="0101", linewidth=4)
    if projection: ax[0].plot(t1array, cff0101_e_r.real + cff0101_g_r.real, label="0101 projection", color="purple", linewidth=2)
    if projection:
        ax[0].plot(t1array, cff0101_g_r.real, '--', label="0101 g", color="orange")
        ax[0].plot(t1array, cff0101_e_r.real, '-.', label="0101 e", color="crimson")
    if reduce2d: ax[0].plot(t1array, cff1001_reduced.real, label="1001")
    if reduce2d: ax[0].plot(t1array, cff1100_reduced.real, label="1100")
    if reduce2d: ax[0].plot(t1array, cff2000_reduced.real, label="2000")
    if reduce3d: ax[0].plot(t1array, cff1000_reduced.real, label="1000")
    if reduce3d: ax[0].plot(t1array, cff1010_reduced.real, label="1010")
    if tmpNew: ax[0].plot(t1array, cff1010_alt_reduced.real, label="1010 alt", color="crimson")
    ax[0].plot(t1array, cff_fo.real, label="First-order")
    ax[0].plot(t1array, cff_fo.real - cffAExact_r.real, label="Difference")

    ax[1].axhline(linewidth=2, color="black")
    ax[1].axvline(linewidth=2, color="black")
    ax[1].plot(t1array, cffAExact_r.imag, linewidth=3, label = "Adiabatic exact")
    ax[1].plot(t1array, cff0000_r.imag, linewidth=3, label="0000")
    ax[1].plot(t1array, cff0101_r.imag, label="0101")
    if reduce2d: ax[1].plot(t1array, cff1001_reduced.imag, label="1001")
    if reduce2d: ax[1].plot(t1array, cff1100_reduced.imag, label="1100")
    if reduce2d: ax[1].plot(t1array, cff2000_reduced.imag, label="2000")
    if reduce3d: ax[1].plot(t1array, cff1000_reduced.imag, label="1000")
    if reduce3d: ax[1].plot(t1array, cff1010_reduced.imag, label="1010")
    ax[1].plot(t1array, cff_fo.imag, label="First-order")

    ax[0].set_xlabel("t")
    ax[0].set_ylabel("Re c_ff(" + str(int(imag2[-1].real)) + " + it)")
    ax[0].set_title( "Re c_ff(" + str(int(imag2[-1].real)) + " + it), lambda=" + str(lambda_pert))
    ax[0].legend()

    ax[1].set_xlabel("t")
    ax[1].set_ylabel("Im c_ff(" + str(int(imag2[-1].real)) + " + it)")
    ax[1].set_title( "Im c_ff(" + str(int(imag2[-1].real)) + " + it), lambda=" + str(lambda_pert))
    ax[1].legend()
    plt.show()


#int0 = integrate.simps(cff2000_reduced)
#int1 = integrate.simps(cff1100_reduced)
#int2 = integrate.simps(cff1001_reduced)
#int3 = integrate.simps(cff1010_reduced)
#print(int1)
#print(int2)
#print(int0/int2)
#print(int0/int3)