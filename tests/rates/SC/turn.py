import numpy as np
from bbo.logger import logger
from bbo.rates import SemiClassicOneState, SemiClassicTwoState, print_crossover

from bbo.PES.tully import APES, APESBH, APES0, APES1, DPES, DPES0, DPES1
from bbo.dvr.tully import dvrBH, dvr0, dvr1, dvrE, Ddvr0, Ddvr1, gridl

print_crossover(APES0)
beta = 500
logger.info("beta: " + str(beta))

tau_test = .15


RateSC_single = SemiClassicOneState(APES0, beta)

logger.info("Testing consistency of turning points")
logger.info("SC One state")
A = RateSC_single.E_lower
B = RateSC_single.beta_from_E()
C = RateSC_single.E_from_beta(RateSC_single._half_traj, beta=B/2)

logger.info("Optimised E: " + str(A))
logger.info("Input beta: " + str(beta))
logger.info("Output beta: " + str(B))
logger.info("Output E: " + str(C))


RateSC = SemiClassicTwoState(APES0, APES1, APES, beta)
logger.info("SC Two state (lower state)")
A = RateSC.E_lower
B = RateSC.beta_from_E()
C = RateSC.E_from_beta(RateSC_single._half_traj, beta=B/2)

logger.info("Optimised E: " + str(A))
logger.info("Input beta: " + str(beta))
logger.info("Output beta: " + str(B))
logger.info("Output E: " + str(C))
