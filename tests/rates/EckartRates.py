import numpy as np
import matplotlib.pyplot as plt
from scipy import integrate
from bbo.logger import logger

from bbo.PES.tully import Eck
from bbo.dvr.tully import dvrE
from bbo.rates import print_crossover, BornOppenheimer
from bbo.rates import SemiClassicOneState, RPOneState


print_crossover(Eck, logger)

beta = 700
logger.info("Selected beta: " + str(beta))


RateBO = BornOppenheimer(PES=Eck, dvr=dvrE, beta=beta)

Zr =  np.sqrt(RateBO.mass/(2*np.pi*beta * RateBO.hbar**2) )


RateSC_single = SemiClassicOneState(Eck, beta)

#TODO: fix this later
#RateRP_single = RPOneState(Eck, beta, N_rec=128)

##WKB rate
#kWKB = (2*np.pi*RateSC.hbar)**(-1./2.) * (RateSC.d2WdE2_tot())**(-1./2.) * np.exp( - RateSC.S_tot()/RateSC.hbar) / Zr
"""cf.: Review paper: eq. 32 using dS/dtau = dS/dq * v becomes eq. 4,
then we use eq. 33 (we have no Q coordinates, because we are in 1D)"""
kWKB = RateSC_single.K_vV_full() * RateSC_single.v() / Zr
logger.info("v: " + str(RateSC_single.v()))
logger.info("Rate (WKB): " + str(kWKB))



def IntegrateAndPlotcff(length, cff, beta, Zr, title="", nquad=129, showplot=True):
	"""Plots the cff and computes the rate from it by integrating it.

	Args:
		length (int): time length over which to plot cff
		nquad (int): has to be 2^m + 1
		cff (function): function to compute the cff
		beta (float): inverse temperature of the calculation
		Zr (float): reactant partition function
		showplot (bool, optional): Show the plot? Defaults to True.

	Returns:
		k (float): the rate
		t (array of floats): the times at which we sampled the cff
		y (array of floats): sampled values of the cff
	"""
	t = np.arange(0,length,length/nquad)
	y = cff(t)
	#k = integrate.simps(y, t)
	#print("Rate: ", k)
	k = integrate.romb(y.real, dx=length/nquad)
	k /= Zr

	if showplot:
		fig, ax = plt.subplots()
		ax.plot(t, y.real)
		plt.xlabel("t")
		plt.ylabel("Re c_ff")
		plt.title(title)
		plt.show()
	return k, t, y

logger.info("Testing exact and leading-order rates")
showplots = False
cfftime = 1000
cfft0 = 0
cfft1 = cfft0 + cfftime
nquad_test = 128 + 1

logger.info("Calculating Born-Oppenheimer rate")
kBO, tBO, yBO = IntegrateAndPlotcff(cfftime, RateBO.cff, beta, Zr,
"Born-Oppenheimer", nquad=129, showplot=showplots)
logger.info("Rate (BO): " + str(kBO))
logger.info("BO/WKB: " + str(kBO/kWKB))



logger.info("Calculate the Eckart rate using an analytical formula")
"""Ref: Ring-polymer instanton theory sec 5.1"""

def W_Eck(E):
	"""Analytic formula for the Eckart barrier"""
	eta = E/Eck.B
	alpha = np.pi * np.sqrt(2*Eck.mass*Eck.a**2*Eck.B) / RateBO.hbar
	gamma = Eck.A/Eck.B
	res = RateBO.hbar * alpha * (2 - np.sqrt(eta) - np.sqrt(eta-gamma))
	return res

def integrand(E):
	return Eck.transmission(E) * np.exp(-beta*E)

def integrand2(E):
	W = RateSC_single.W_tot()
	return np.exp(-W(E)/RateBO.hbar) * np.exp(-beta*E)

Emax = Eck.B*3
E = np.linspace(0.0, Emax, 1024+1 )
kan = 1./(2*np.pi*Eck.UNITS.hbar) * integrate.simps(integrand(E), E)  /Zr
logger.info("Rate (analytic BO): " + str(kan))
#kan2 = 1./(2*np.pi*Eck.UNITS.hbar) * integrate.simps([integrand2(EE) for EE in E ] , E)  /Zr
#print("Rate (analytic approx): " + str(kan2))

if True: #plotting the integrand to check that the integral makes sense
	fig, ax = plt.subplots()
	ax.plot(E, integrand(E))
	plt.xlabel("E")
	plt.ylabel("P(E) * e^(-beta * E)")
	plt.title("Plot to check that the integration makes sense")
	plt.show()

alpha = np.pi * np.sqrt(2*Eck.mass*Eck.a**2*Eck.B) / RateBO.hbar

"""
Ref:
Microcanonical and thermal instanton rate theory
for chemical reactions at all temperatures.,
J. O. Richardson
https://doi.org/10.1039/c6fd00119j
"""
betac = alpha / Eck.B
S = RateBO.hbar * alpha * ( 2 - betac/beta  )
#print("Action: " + str(S))
#Eq. 18
ksc1 = np.sqrt(2*Eck.B/Eck.mass) * betac/beta *np.exp(-S/RateBO.hbar)
logger.info("Rate (inst analytic): " + str(ksc1))
logger.info("sc1/WKB (should be 1): " + str(ksc1/kWKB))
logger.info("analytic BO/WKB: " + str(kan/kWKB))

logger.info("B-O/analytic BO (should be 1): " + str(kBO/kan))
