import numpy as np
from scipy import integrate, linalg, optimize
from opt_einsum import contract
import matplotlib.pyplot as plt
from polylib.WKB import System as WKBSystem, Trajectory, account_for_bounces
from polylib import root, path_integral, trajectory as trajectory_RP
import pickle
import logging

import warnings

#TODO: only for testing, delete later
tau_test = .2

#TODO: ultimately get rid of the PES argument in TwoStateBase

#TODO check for convergence wrt all parameters
#Q1: is there any way to speed up the triple loops?


xdagger = 0. #dividing surface

################

class BornOppenheimer():
	r"""Tools for calculating rates in a 2-state 1D system using
	the Born-Oppenheimer approximation.
	That means, using only the lower PES."""
	def __init__(self, PES, dvr, beta, xdagger=xdagger) -> None:
		self.PES = PES
		self.dvr = dvr
		self.step = dvr.hh
		self.xdagger = xdagger
		self.beta = beta
		#jdagger consistent with dvr1d
		self.jdagger = np.argmin(  np.abs( self.dvr.grid - self.xdagger) )
		self.energies = self.dvr.evals.real
		self.evecs = self.dvr.evecs
		self.mass = PES.mass
		self.hbar = PES.UNITS.hbar

	def expterm(self, t):
		"""Calculation of the propagator term required for cff
		at the inverse temperature beta and for an array of times t.

		Args:
			t (array of floats): array of times for which the
			propagator is evaluated
			beta (float): inverse temperature
			energies (array of floats, optional): Eigenvalues of the Hamiltonian.
			Shape: (n)

		Returns:
			res: a float array of shape (len(t), n, n), where
			n is the number of energies.
		"""
		beta = self.beta
		energies = self.energies
		hbar = self.hbar

		z = (beta/2 + 1j*t)[:,None,None]
		zc = (beta/2 - 1j*t)[:,None,None]
		res = (np.exp(-energies[:,None]*z/hbar) *
			   np.exp(-energies[None,:]*zc/hbar))
		return res

	def Fmn_Step(self):
		"""Calculation of <m|F|n> where F is the Flux operator
		and m, n are energy eigenstates.
		Uses the step function.
		Implemenation taken from dvr1d.py, but vectorised.

		Args:
			evecs (array of eigenvectors, optional):
			Eigenvectors of the Hamiltonian with corresponding energies.
			Array of shape (n,d), where n is the number of eigenvectors
			and d is the dimension of the truncated Hilbert space.
			Defaults to evecs0: eigenvectors of the lower adiabat.

			dvr (object, optional): the dvr object from dvr1d
			Default is the dvr for the lower adiabat.

		Returns:
			array of floats: shape (n,n)
		"""
		evecs = self.evecs
		dvr = self.dvr
		xdagger = self.xdagger

		res = contract("mnj,jk,mnk->mn",evecs[:,None],dvr.flux(xdagger)[0],evecs[None,:])
		res *= self.step
		return res

	def Fmn_Velocity(self):
		"""Calculation of <m|F|n> where F is the Flux operator
		and m, n are energy eigenstates.
		Uses momentum.
		Implemenation taken from dvr1d.py, but vectorised.

		Args:
			evecs (array of eigenvectors, optional):
			Eigenvectors of the Hamiltonian with corresponding energies.
			Array of shape (n,d), where n is the number of eigenvectors
			and d is the dimension of the truncated Hilbert space.
			Defaults to evecs0: eigenvectors of the lower adiabat.

			dvr (object, optional): the dvr object from dvr1d
			Default is the dvr for the lower adiabat.

		Returns:
			array of floats: shape (n,n)
		"""
		evecs = self.evecs
		dvr = self.dvr
		xdagger = self.xdagger

		res = contract("mnj,jk,mnk->mn",evecs[:,None],dvr.flux(xdagger, method="direct")[0],evecs[None,:])
		res *= self.step
		return res

	def Fmn_fast(self):
		"""Calculation of <m|F|n> where F is the Flux operator
		and m, n are energy eigenstates.
		Uses a method equivalent to F_step.
		It was optimised to be faster by taking advantage of the
		particular shape of theta.

		Args:
			evecs (array of eigenvectors, optional):
			Eigenvectors of the Hamiltonian with corresponding energies.
			Array of shape (n,d), where n is the number of eigenvectors
			and d is the dimension of the truncated Hilbert space.
			Defaults to evecs0: eigenvectors of the lower adiabat.

			energies (array of floats, optional): Eigenvalues of the Hamiltonian.
			Shape: (n)
			Defaults to energies0: eigenvalues of the lower adiabat.

			dvr (object, optional): the dvr object from dvr1d
			Default is the dvr for the lower adiabat.

			jdagger (int, optional): index of xvec such that xvec[jdagger]
			corresponds to xdagger
			defaults to jdagger calculated above such that
			xvec[jdagger]=xdagger=0

		Returns:
			array of floats: shape (n,n)

		Notes:
			Optimisation: imagine we are trying to compute F_mn
			for a single index pair m, n. The F_Step implementation
			effectively contains this operation:
			res = np.conj(evecs[m]) @ theta @ evecs[n],
			where theta is the matrix representation of Theta(x-xdagger).
			This corresponds to a simple dot product which ignores elements
			lower than jdagger:
			res = np.matmul(np.conj(evecs[m, jdagger:]), evecs[n,jdagger:])
			Note: it is not exactly equal to F_Step, as the Theta function
			in there has a value .5 for Theta(0)=0 and here we use Theta(0)=1.
		"""
		# res = contract("mni,mni->mn",evecs[m_indices, jdagger:], np.conj(evecs[n_indices, jdagger:]) )
		evecs = self.evecs
		energies = self.energies
		jdagger = self.jdagger
		hbar = self.hbar

		res = np.sum(evecs[:,None,jdagger:] *
					np.conj(evecs)[None,:,jdagger:], axis=-1)
		res *= self.step
		res *= (energies[:,None] - energies[None,:])
		res = res / (hbar * 1j)
		return res

	def cff(self, t):
		"""Calculates the flux-flux autocorrelation function Cff
		at the inverse temperature beta and times t.

		Args:
			t (array of floats): array of times for which the
			propagator is evaluated
			beta (float): inverse temperature
			energies (array of floats, optional): Eigenvalues of the Hamiltonian.
			Shape: (n)
			Defaults to energies0: eigenvalues of the lower adiabat.

			Returns:
				res (array of floats): the value of cff at times t
		"""
		beta = self.beta
		Fmat = self.Fmn_Step()

		rr =  self.expterm(t) * Fmat * np.transpose(Fmat)
		res = np.sum(rr, axis=(-1,-2))
		return res


class TwoStateDVR():
	r"""Tools for calculating rates in a 2-state 1D system going beyond the Born-Oppenheimer approximation.
	That means, using mainly the lower PES, but including contributions
	from the upper PES."""

	def __init__(self, PES0, PES1, PES, dvr0, dvr1, beta, xdagger=xdagger) -> None:
		self.PES = PES
		self.PES1 = PES1
		self.PES0 = PES0
		self.dvr0 = dvr0 #TODO check that both dvrs use the same grid
		self.dvr1 = dvr1
		self.step = dvr0.hh
		self.xdagger = xdagger
		self.jdagger = np.argmin(  np.abs( self.dvr0.grid - self.xdagger) )
		self.beta = beta
		self.energies0 = self.dvr0.evals.real
		self.evecs0 = self.dvr0.evecs
		self.energies1 = self.dvr1.evals.real
		self.evecs1 = self.dvr1.evecs
		self.mass = PES0.mass
		self.hbar = PES0.UNITS.hbar
		self.dim = 2*len(self.dvr0.grid)
		self.gridl = (self.dvr0.ub - self.dvr0.lb)/2
		self.halfdim = len(self.dvr0.grid)
		self.energies_total = np.concatenate([self.energies0, self.energies1])
		def pad_evec(evec, before=False):
			padding = np.zeros_like(self.evecs0[0])
			if before:
				res = np.concatenate([padding, evec])
			else:
				res = np.concatenate([evec, padding])
			return res
		#evecs0[i] is the i-th eigenvector
		evecs0padded = [ pad_evec(self.evecs0[i], before=False) for i in range(self.halfdim) ]
		evecs1padded = [ pad_evec(self.evecs1[i], before=True) for i in range(self.halfdim) ]
		self.evecs_total = np.concatenate( [evecs0padded, evecs1padded] )
		#print(np.shape(self.energies0))
		#print(np.shape(self.energies_total))
		#print(np.shape(self.evecs0[0]))
		#print(np.shape(evecs1padded[0]))


	def integrate(self, integrand, *args, **kwargs):
		r"""Calculate a numerical integral in accordance with the
		underlying quadrature rule.
		Taken from George's dvr
		"""
		return np.sum(self.step * integrand, axis=kwargs.get('axis', -1))

	def diagonalise(self, Hamiltonian):
		r"""
		Diagonalise the Hamiltonian matrix
		Taken from George's dvr
		"""
		halfdim = self.halfdim
		energies, evecs = np.linalg.eigh(
			Hamiltonian.reshape(2*halfdim, 2*halfdim), UPLO='L')
		evecs = evecs.T

		# Normalisation factors:
		vectors = evecs*np.conj(evecs)
		N = self.integrate(vectors, axis=-1)

		evecs /= np.sqrt(N[:, None])

		evecs = evecs.reshape(2, halfdim, 2, halfdim)

		return energies, evecs

	def p2(self):
		"""Workaround"""
		dvr0 = self.dvr0
		KE = dvr0.KE

		fac = 2.*self.mass
		res = linalg.sqrtm(KE * fac)
		return res

	def p(self):
		"""Should work"""
		halfdim = self.halfdim
		indices = np.arange(halfdim)
		diff = indices[:, None] - indices[None, :]
		np.fill_diagonal(diff, 1.)
		sgn = 1 - 2*np.mod(diff,2)
		res = sgn /diff
		np.fill_diagonal(res, 0.)
		res = res / self.step
		res = res.astype(complex)
		res *= -1j * self.hbar
		return res

	def delta(self, x=None):
		"""
		Delta function in the x-representation.
		Shape: (halfdim, halfdim).
		if x is None, then x gets set to xdagger.
		"""
		if x is None:
			jdagger = self.jdagger
		else:
			jdagger = np.argmin(  np.abs( self.dvr0.grid - x) )
		#print("jdagger: " + str(jdagger))
		halfdim = self.halfdim
		res = np.zeros( (halfdim, halfdim) )
		res[jdagger, jdagger] = 1. / self.step
		return res

	def psquared(self):
		halfdim = self.halfdim
		indices = np.arange(halfdim)
		diff = indices[:,None] - indices[None,:]
		sgn = 1 - 2*np.mod(diff,2)
		np.fill_diagonal(diff, 1.) #avoid division by zero
		res = 2* sgn/diff**2
		np.fill_diagonal(res, np.pi**2 / 3)
		res *= self.hbar**2 / (self.step**2)
		return res

	def expterm(self, z, energies=None):
		"""Calculation of the propagator term required for cff
		at the inverse temperature beta and for an array of times t.

		Args:
			t (array of floats): array of times for which the
			propagator is evaluated
			beta (float): inverse temperature
			energies (array of floats, optional): Eigenvalues of the Hamiltonian.
			Shape: (n)

		Returns:
			res: a float array of shape (len(t), n, n), where
			n=2*halfdim is the number of energies.
		"""
		beta = self.beta
		if energies is None:
			energies = self.energies
		hbar = self.hbar

		z = z[:,None,None]
		zcompl = beta - z
		res = (np.exp(-energies[:,None]*z/hbar) *
			   np.exp(-energies[None,:]*zcompl/hbar))
		return res

	def F_numerical(self, func=None):
		if func is None:
			func = self.Hamiltonian
		halfdim = self.halfdim
		hbar = self.hbar
		jdagger = self.jdagger
		HH = func().reshape(2*halfdim, 2*halfdim)

		side = np.zeros( (halfdim, halfdim) )
		np.fill_diagonal(side[jdagger:, jdagger:], 1.)
		side[jdagger, jdagger] = .5
		bigside = np.zeros( (2, halfdim, 2, halfdim), dtype=complex )
		bigside[0,:,0] = side
		bigside[1,:,1] = side
		bigside = bigside.reshape(2*halfdim, 2*halfdim)

		commutator = bigside @ HH - HH @ bigside

		res = commutator.reshape(2, halfdim, 2, halfdim) / (1.j * hbar)
		return res



k = 0
class AdiabaticExact(TwoStateDVR):
	r"""Tools for the exact quantum calculation of rate in the adiabatic basis.
	We work with the dvr basis constructed from dvr0 and dvr1 - the dvrs of the
	lower and upper adiabat.
	The basis set is: {|i>⊗|x>} where i ∈ {0,1} and x is the dvr grid.
	"""
	def __init__(self, PES0, PES1, PES, dvr0, dvr1, beta, lambda_pert=1., xdagger=xdagger) -> None:
		super().__init__(PES0, PES1, PES, dvr0, dvr1, beta, xdagger=xdagger)
		self.lambda_pert = lambda_pert
		self.HH = self.Hamiltonian()
		self.energies, self.evecs = self.diagonalise(self.HH)
		# self.energiesl0 = self.energies_total
		# self.evecsl0 = self.evecs_total
		self.testl, self.testv = self.diagonalise(self.F1num())
		t1 = self.energies[k]
		t2 = self.testl[k]
		# print("Lowest adiabatic energy: " + str(t1))
		# print("Adiabatic F: " + str(t2))

	def d(self):
		r"""The derivative coupling vector (here scalar, because we are in 1D)
		Returns:
			array of shape (halfdim, 2, 2): d(x)_{i,j}, but the x is discretised
		"""
		grid = self.dvr0.grid
		halfdim = self.halfdim
		res = np.zeros( (halfdim, 2, 2) )
		for i in range(halfdim):
			res[i] = self.PES.first_order_coupling(grid[i])
		return res

	def Hl0(self):
		"""Order lambda^0"""
		halfdim = self.halfdim
		res = np.zeros( (2, halfdim, 2, halfdim), dtype=complex )
		res[0,:,0] = self.dvr0.HH
		res[1,:,1] = self.dvr1.HH
		return res

	def Lambdal1(self):
		r"""A part of the Hamiltonian containing derivative coupling.
		Order lambda^1.

		Returns:
			array of shape (2, halfdim, 2, halfdim): Lambda
		"""
		halfdim = self.halfdim
		hbar = self.hbar
		d = self.d()
		res = np.zeros( (2, halfdim, 2, halfdim), dtype=complex )

		p = self.p()

		expr = (p @ np.diag(d[:, 0, 1] ) + np.diag(d[:, 0, 1] ) @ p)
		res[0,:,1,:] =  expr
		res[1,:,0,:] = -expr
		res *= - 1j * hbar / (2.*self.PES0.mass)
		res *= self.lambda_pert
		return res

	def Lambdal2(self):
		r"""A part of the Hamiltonian containing derivative coupling.
		Order lambda^2.

		Returns:
			array of shape (2, halfdim, 2, halfdim): Lambda
		"""
		halfdim = self.halfdim
		hbar = self.hbar
		d = self.d()
		res = np.zeros( (2, halfdim, 2, halfdim), dtype=complex )

		res[0,:,0,:] = np.diag(d[:, 0, 1]) @ np.diag(d[:, 1, 0])
		res[1,:,1,:] = np.diag(d[:, 1, 0]) @ np.diag(d[:, 0, 1])
		res *= - (hbar**2) / (2*self.PES0.mass)
		res *= self.lambda_pert**2
		return res

	def Hamiltonian(self):
		res = self.Hl0()
		res += self.Lambdal1() + self.Lambdal2()
		return res

	def Fl0(self):
		"""Order lambda^0"""
		halfdim = self.halfdim

		res = np.zeros( (2, halfdim, 2, halfdim), dtype=complex )
		F0 = self.dvr0.flux(self.xdagger)[0]
		F1 = self.dvr1.flux(self.xdagger)[0]
		res[0,:,0,:] = F0
		res[1,:,1,:] = F1
		res *= -1. #TODO: idk why I have to do this
		return res

	def Fl1(self):
		"""Order lambda^1"""
		hbar = self.hbar
		halfdim = self.halfdim
		d = self.d()
		delta = self.delta()
		res = np.zeros( (2, halfdim, 2, halfdim), dtype=complex )

		commutator = (delta @ np.diag(d[:, 0, 1]))*2 # + np.diag(d[:, 0, 1]) @ delta) #They commute, because they are both diagonal in x-repre

		res[0, :, 1, :] =  commutator
		res[1, :, 0, :] = -commutator
		res *= - 1j * hbar / (2.*self.PES0.mass)
		res *= self.lambda_pert
		return res

	def F(self):
		r"""The flux operator

		Returns:
			array of floats (2, halfdim, 2, halfdim): F
		"""
		#res = self.Fl0() + self.Fl1()
		#res = self.F_numerical()
		res = self.Fl0() + self.F1num()
		return res

	def F0num(self):
		"""For testing"""
		res = self.F_numerical(func=self.Hl0)
		return res

	def F1num(self):
		"""For testing"""
		res = self.Fl1() / self.lambda_pert
		#res = self.F_numerical(func=self.Lambdal1)
		res *= self.lambda_pert
		return res

	def cff(self, t):
		"""Calculates the flux-flux autocorrelation function Cff
		at the inverse temperature beta and times t.

		Args:
			t (array of floats): array of times for which the
			propagator is evaluated
			beta (float): inverse temperature
			energies (array of floats, optional): Eigenvalues of the Hamiltonian.
			Shape: (n)
			Defaults to energies0: eigenvalues of the lower adiabat.

			Returns:
				res (array of floats): the value of cff at times t
		"""
		beta = self.beta
		halfdim = self.halfdim
		F = self.F().reshape(2*halfdim, 2*halfdim)
		evecs = self.evecs.reshape(2*halfdim, 2*halfdim)

		Fmn = contract("mnj,jk,mnk->mn",evecs[:,None],F,evecs[None,:])
		Fmn *= self.step

		rr =  self.expterm(t) * Fmn * np.transpose(Fmn)
		res = np.sum(rr, axis=(-1,-2))
		return res


class DiabaticExact(TwoStateDVR):
	r"""Tools for the exact quantum calculation of rate in the diabatic basis.
	We work with the dvr basis constructed from dvr0 and dvr1 - the dvrs of the
	two diabats.
	The basis set is: {|i>⊗|x>} where i ∈ {0,1} and x is the dvr grid.
	"""

	def __init__(self, PES0, PES1, PES, dvr0, dvr1, beta, xdagger=xdagger) -> None:
		super().__init__(PES0, PES1, PES, dvr0, dvr1, beta, xdagger=xdagger)
		self.HH = self.Hamiltonian()
		self.energies, self.evecs = self.diagonalise(self.HH)
		# self.energiesl0 = self.energies_total
		# self.evecsl0 = self.evecs_total
		self.testl, self.testv = self.diagonalise(self.F())
		# print("Lowest diabatic energy: " + str(self.energies[k]))
		# print("Diabatic F: " + str(self.testl[k]))

	def H0(self):
		halfdim = self.halfdim
		res = np.zeros( (2, halfdim, 2, halfdim))
		res[0,:,0] = self.dvr0.HH
		res[1,:,1] = self.dvr1.HH
		return res

	def H1(self):
		halfdim = self.halfdim
		PES = self.PES
		res = np.zeros( (2, halfdim, 2, halfdim) )
		V01 = np.asarray([PES.potential(x)[0,1] for x in self.dvr0.grid ])
		V10 = np.asarray([PES.potential(x)[1,0] for x in self.dvr0.grid ])
		res[0,:,1] = np.diag(V01)
		res[1,:,0] = np.diag(V10)
		return res

	def Hamiltonian_full(self):
		halfdim = self.halfdim
		PES = self.PES
		psq = self.psquared()
		res = np.zeros( (2, halfdim, 2, halfdim), dtype=complex)
		T = psq / (2*self.mass)
		V00 = np.asarray([PES.potential(x)[0,0] for x in self.dvr0.grid ])
		V11 = np.asarray([PES.potential(x)[1,1] for x in self.dvr0.grid ])
		V01 = np.asarray([PES.potential(x)[0,1] for x in self.dvr0.grid ])
		V10 = np.asarray([PES.potential(x)[1,0] for x in self.dvr0.grid ])

		res[0,:,0,:] = T + np.diag(V00)
		res[1,:,1,:] = T + np.diag(V11)
		res[0,:,1,:] = np.diag(V01)
		res[1,:,0,:] = np.diag(V10)
		return res

	def Hamiltonian(self):
		res = self.Hamiltonian_full()
		#res = self.H0() + self.H1()
		return res

	def F(self):
		res = self.F_numerical()
		#res = self.F_unused()
		return res

	def F_unused(self):
		halfdim = self.halfdim
		p = self.p()
		delta = self.delta()

		res = np.zeros( (2, halfdim, 2, halfdim), dtype=complex )
		res[0, :, 0, :] = delta @ p + p @ delta
		res[1, :, 1, :] = delta @ p + p @ delta
		res *= 1./(2*self.mass)
		return res

	def cff(self, t):
		beta = self.beta
		halfdim = self.halfdim
		F = self.F().reshape(2*halfdim, 2*halfdim)
		evecs = self.evecs.reshape(2*halfdim, 2*halfdim)

		Fmn = contract("mnj,jk,mnk->mn",evecs[:,None],F,evecs[None,:])
		Fmn *= self.step

		rr =  self.expterm(t) * Fmn * np.transpose(Fmn)
		res = np.sum(rr, axis=(-1,-2))
		return res

class Perturbative(AdiabaticExact):

	def __init__(self, PES0, PES1, PES, dvr0, dvr1, beta, lambda_pert=1., xdagger=xdagger) -> None:
		super().__init__(PES0, PES1, PES, dvr0, dvr1, beta, lambda_pert, xdagger)
		# self.energiesl0, self.evecsl0 = self.diagonalise(self.Hl0())
		self.energiesl0 = self.energies_total
		self.evecsl0 = self.evecs_total
		#Computing Fmn for re-use
		#We have Fmn = F0mn + F1mn
		self.Fl0mn = self.expectation(
			self.Fl0().reshape(2*self.halfdim, 2*self.halfdim)
		)
		self.Fl1mn = self.expectation(
			self.F1num().reshape(2*self.halfdim, 2*self.halfdim)
		)
		#We have Hmn = H0mn + H1mn + H2mn
		#pertlambdas are included in lambdal1 and lambdal2
		#Then we have to include a - sign with the Dyson series later.
		self.H1mn = self.expectation(
			self.Lambdal1().reshape(2*self.halfdim, 2*self.halfdim)
		)
		self.H2mn = self.expectation(
			self.Lambdal2().reshape(2*self.halfdim, 2*self.halfdim)
		)

		#doubleP = np.zeros((2, self.halfdim, 2, self.halfdim))
		#doubleP[0,:,0] = self.p()
		#doubleP[1,:,1] = self.p()
		#pexp = self.expectation( doubleP.reshape(2*self.halfdim, 2*self.halfdim))
		#exit()

	#Calculate the various functions cff(z), cff(z,z') and cff(z,z',z'')
	#of the order up to lambda^2

	def TimeOrderedDifference(self, z1, z2, reverse=False):
		"""For two time arrays z1 and z2, gives
		back an array z1 - z2 of shape (m,n).
		It is also time ordered -> when the times
		can be compared and z1-z2<0, it sets the value
		to None.
		Note: imaginary time is here a real number
		and real time an imaginary number.
		"""
		if reverse: z2 = -z2
		zz = z1[:, None] - z2[None, :]
		if (z1.imag == 0).all() and (z1.imag == 0).all():
			#cff(imag1, imag2)
			if reverse:
				zz[zz.real > 0] = None
			else:
				zz[zz.real < 0] = None
		#elif (z1.imag == 0).all() and (z2.imag != 0).any():
		#    #cff(imag1, real2)
		#    pass
		#elif (z2.imag == 0).all() and (z1.imag != 0).any():
		#    #cff(real1, imag2)
		#    pass
		elif (z1.imag != 0).any() and (z2.imag != 0).any():
			#cff(real1, real2)
			#we use the fact that both arguments have the same imaginary time component (beta/2)
			if reverse:
				zz[zz.imag > 0] = None
			else:
				zz[zz.imag < 0] = None
		return zz

	def Difference(self, z1, z2):
		return z1-z2

	def expterm(self, z, energies=None):
		beta = self.beta
		if energies is None:
			energies = self.energiesl0
		hbar = self.hbar

		z = z[:,None,None]
		zcompl = beta - z
		res = (np.exp(-energies[:,None]*z/hbar) *
			np.exp(-energies[None,:]*zcompl/hbar))
		return res

	def propagator(self, z, transpose=False, energies=None):
		if energies is None:
			energies = self.energiesl0
		hbar = self.hbar

		z = z[..., None]
		res = np.exp(-energies[:]*z/hbar)
		#Shape (...,n),
		#where ... is the shape of z and n is the
		#shape of energies.
		return res

	def expectation(self, A, evecs=None):
		"""Given an operator A, returns <m|A|n>,
		where m, n are energy eigenstates.
		"""
		halfdim = self.halfdim
		if evecs is None:
			evecs = self.evecsl0.reshape(2*halfdim, 2*halfdim)

		res = contract("mnj,jk,mnk->mn",evecs[:,None],A,evecs[None,:])
		res *= self.step
		return res

	# Just for testing
	def trRhoF0F0(self, z1_full, prop=1.):
		halfdim = self.halfdim
		Fl0mn = self.Fl0mn

		# Only the lower state
		F = self.select_dims(Fl0mn, 0, 0)
		rho = self.propagator(z1_full*prop)[0, :halfdim] * np.eye(halfdim)
		rho2 = self.propagator(z1_full*(1.-prop))[0, :halfdim] * np.eye(halfdim)

		res = contract("ij, jk, kl, li->", rho, F, rho2, F)
		#res = contract("ij, ji->", rho, F) # 0 from symmetry arguments
		return res

	def trRhodelta(self, z1):
		halfdim = self.halfdim
		rho = self.propagator(z1)[0, :halfdim] * np.eye(halfdim)

		delta_half = self.delta()
		delta = np.zeros((2, halfdim, 2, halfdim), dtype="complex")
		delta[0, :, 0, :] = delta_half
		#delta[1, :, 1, :] = delta_half
		delta = self.expectation(delta.reshape(2*self.halfdim, 2*self.halfdim))
		delta_e_half = self.select_dims(delta, 0, 0)

		res = contract("ij, ji->", rho, delta_e_half)
		return res

	def trRhoF1(self, z1):
		halfdim = self.halfdim
		rho = self.propagator(z1)[0, :halfdim] * np.eye(halfdim)
		rho_full = np.zeros((2, halfdim, 2, halfdim), dtype="complex")
		rho_full[0, :, 0, :] = rho
		rho_full = rho_full.reshape(2*halfdim, 2*halfdim)

		F1mn = self.Fl1mn
		down = np.zeros((2, halfdim, 2, halfdim), dtype="complex")
		down[1, :, 0, :] = np.eye(halfdim)
		down = down.reshape(2*self.halfdim, 2*self.halfdim)

		#up = np.zeros((2, halfdim, 2, halfdim), dtype="complex")
		#up[0, :, 1, :] = np.eye(halfdim)
		#up = up.reshape(2*self.halfdim, 2*self.halfdim)

		down = self.expectation(down)
		#up = self.expectation(up)

		down_r = self.select_dims(down, 1, 0)
		F1_01 = self.select_dims(F1mn, 0, 1)
		F1_10 = self.select_dims(F1mn, 1, 0)
		# print(F1_01 - np.conj(F1_10.T)) # -> this is 0

		d = self.d()[..., 0, 1]
		jd = self.jdagger
		d = d[jd]
		const = - 2*1j * self.hbar / (2.*self.mass) * d
		res = contract("ij, jk, ki->", rho, F1_01, down_r)
		#res = contract("ij, jk, ki->", rho_full, F1mn, down)

		#res /= const

		if 0:
			delta_half = self.delta()
			delta = np.zeros((2, halfdim, 2, halfdim), dtype="complex")
			delta[0, :, 0, :] = delta_half
			#delta[1, :, 1, :] = delta_half
			delta = self.expectation(delta.reshape(2*self.halfdim, 2*self.halfdim))
			delta_e_half = self.select_dims(delta, 0, 0)
			delta_alt = contract("ij, jk->ik", F1_01, down_r) / const
			print(delta_alt - delta_e_half)
			exit()
		return res


	def cff0000alt(self, z1):
		"""H0F0H0F0: Born-Oppenheimer approximation with both states, alternative"""
		beta = self.beta
		energiesl0 = self.energiesl0

		Fmn = self.Fl0mn
		rr =  self.expterm(z1, beta, energies=energiesl0) * Fmn * np.transpose(Fmn)
		res = np.sum(rr, axis=(-1,-2))
		return res

	def cff0000(self, z1):
		"""H0F0H0F0: Born-Oppenheimer approximation with both states"""
		beta = self.beta
		Fmnl = self.Fl0mn
		Fmnr = self.Fl0mn

		Ul = self.propagator(z1)
		Ur = self.propagator(beta - z1)

		res = np.zeros( len(z1), dtype=complex )
		for i in range(len(z1)):
			a = Ul[i, :, None] * Fmnl[:, :]
			b = Ur[i, :, None] * Fmnr[:, :]
			res[i] = contract("ij,ji->", a, b)
		return res

	def cff0101(self, z1):
		"""H0F1H0F1"""
		halfdim = self.halfdim
		beta = self.beta
		Fmnl = self.Fl1mn
		Fmnr = self.Fl1mn
		unwrapped = self.Fl1mn.reshape(2, halfdim, 2, halfdim)
		up = np.zeros_like(unwrapped)
		down = np.zeros_like(unwrapped)
		down[0, :, 1, :] = unwrapped[0, :, 1, :]
		up[1, :, 0, :]   = unwrapped[1, :, 0, :]
		up   = up.reshape(2*halfdim, 2*halfdim)
		down = down.reshape(2*halfdim, 2*halfdim)

		#print("Curr beta: " + str(z1))
		Ul = self.propagator(z1)
		Ur = self.propagator(beta - z1)

		res = np.zeros(len(z1), dtype=complex)
		for i in range(len(z1)):
			a = Ul[i, :, None] * up[:, :]
			b = Ur[i, :, None] * down[:, :]
			res[i] = contract("ij,ji->", a, b)
		res *= 2.
		return res

	def cff0101_alt(self, z1):
		"""H0F1H0F1"""
		beta = self.beta
		Fmnl = self.Fl1mn
		Fmnr = self.Fl1mn

		Ul = self.propagator(z1)
		Ur = self.propagator(beta - z1)

		res = np.zeros( len(z1), dtype=complex )
		for i in range(len(z1)):
			a = Ul[i, :, None] * Fmnl[:, :]
			b = Ur[i, :, None] * Fmnr[:, :]
			res[i] = contract("ij,ji->", a, b)
		return res

	def cff0101_g(self, z1):
		"""H0F1H0F1 projected to g"""
		beta = self.beta
		halfdim = self.halfdim
		Fmnl = self.Fl1mn
		Fmnr = self.Fl1mn

		Ul = self.propagator(z1)
		Ur = self.propagator(beta - z1)
		I = np.eye( halfdim )
		P_g = np.zeros_like(Fmnl).reshape(2, halfdim, 2, halfdim)
		P_g[0, :, 0] = I
		P_g = P_g.reshape(2*halfdim, 2*halfdim)

		res = np.zeros( len(z1), dtype=complex )
		for i in range(len(z1)):
			a = Ul[i, :, None] * Fmnl[:, :]
			b = Ur[i, :, None] * Fmnr[:, :]
			res[i] = contract("ij, jk, ki->", a, b, P_g)
		return res

	def cff0101_e(self, z1):
		"""H0F1H0F1 projected to e"""
		beta = self.beta
		halfdim = self.halfdim
		Fmnl = self.Fl1mn
		Fmnr = self.Fl1mn

		Ul = self.propagator(z1)
		Ur = self.propagator(beta - z1)
		I = np.eye( halfdim )
		P_e = np.zeros_like(Fmnl).reshape(2, halfdim, 2, halfdim)
		P_e[1, :, 1] = I
		P_e = P_e.reshape(2*halfdim, 2*halfdim)

		res = np.zeros( len(z1), dtype=complex )
		for i in range(len(z1)):
			a = Ul[i, :, None] * Fmnl[:, :]
			b = Ur[i, :, None] * Fmnr[:, :]
			res[i] = contract("ij, jk, ki->", P_e, a, b)
		return res

	def cff1001(self, z1, z2):
		"""H1F0H0F1"""
		#- for the Dyson series
		beta = self.beta
		H1mn = - self.H1mn / self.hbar
		Fmnl = self.Fl0mn
		Fmnr = self.Fl1mn

		zz = self.TimeOrderedDifference(z1, z2)
		Ul1 = self.propagator( zz )
		Ul2 = self.propagator(z2)
		Ur = self.propagator(beta - z1)

		res = np.zeros( (len(z1), len(z2)), dtype=complex )
		for i in range(len(z1)):
			for j in range( len(z2) ):
				a = Ul1[i, j, :, None] * H1mn[:, :]
				b = Ul2[j, :, None]    * Fmnl[:, :]
				c = Ur[i, :, None]     * Fmnr[:, :]
				res[i, j] = contract("ij, jk, ki->", a, b, c)
		return res

	def cff1100(self, z1, z2):
		"""H1F1H0F0"""
		beta = self.beta
		H1mn = - self.H1mn / self.hbar
		Fmnl = self.Fl1mn
		Fmnr = self.Fl0mn

		zz = self.TimeOrderedDifference(z1, z2)
		Ul1 = self.propagator(zz)
		Ul2 = self.propagator(z2)
		Ur = self.propagator(beta - z1)

		res = np.zeros( (len(z1), len(z2)), dtype=complex )
		for i in range(len(z1)):
			for j in range( len(z2) ):
				a = Ul1[i, j, :, None] * H1mn[:, :]
				b = Ul2[j, :, None]    * Fmnl[:, :]
				c = Ur[i, :, None]     * Fmnr[:, :]
				res[i, j] = contract("ij, jk, ki->", a, b, c)
		return res

	def select_dims(self, x, i, j):
		halfdim = self.halfdim
		x = x.reshape(2, halfdim, 2, halfdim)
		return x[i,:, j, :]

	def cff1100_part_testing(self, z1, z2):
		beta = self.beta
		H1mn = - self.H1mn / self.hbar
		H1mn /= self.lambda_pert
		#H1mn *= (2*self.mass ) #For consistency
		#The delta function operator -> restrict to the dividing surface
		halfdim = self.halfdim
		delta_half = self.delta()
		delta = np.zeros((2, halfdim, 2, halfdim), dtype="complex")
		delta[0, :, 0, :] = delta_half
		delta = self.expectation(delta.reshape(2*self.halfdim, 2*self.halfdim))
		delta_e_half = self.select_dims(delta, 0, 0)

		zz = self.TimeOrderedDifference(z1, z2)
		Ul1 = self.propagator(zz)
		Ul2 = self.propagator(z2)
		Ul2 = Ul2[0] * np.eye(2*halfdim)
		Ul2 = self.select_dims(Ul2, 1, 1)
		#TODO: just do L1_01

		for i in range(len(z1)):
			for j in range(len(z2)):
				a = Ul1[i, j, :halfdim] * np.eye(halfdim)

		L1_01 = self.select_dims(H1mn[:, :], 0, 1)
		if 0:
			print(np.shape(a))
			print(np.shape(Ul2))
			print(np.shape(delta_half))
			print(np.any(a))
		# print(a)
		#Is zero for some reason
		res = contract("ij, jk, kl, li->", a, L1_01, Ul2, delta_e_half) * self.step
		# res = contract("ij, ji->", delta_half, a)
		#res = contract("ij, ji->", a, Ul2)
		# print(res)
		# exit()
		# res = contract("ij, ji->", Ul2, delta)
		return res

	def cff1100_another_testing(self, z1, z2):
		beta = self.beta
		H1mn = - self.H1mn / self.hbar
		Fl0mn = self.Fl0mn
		#The delta function operator -> restrict to the dividing surface
		halfdim = self.halfdim
		delta_half = self.delta()
		delta = np.zeros((2, halfdim, 2, halfdim), dtype="complex")
		delta[0, :, 0, :] = delta_half
		#delta[1, :, 1, :] = delta_half
		delta = self.expectation(delta.reshape(2*self.halfdim, 2*self.halfdim))
		delta_e_half = self.select_dims(delta, 0, 0)

		zz = self.TimeOrderedDifference(z1, z2)
		Ul1 = self.propagator(zz)
		#Ul2 = self.propagator(z2)

		propagator = Ul1[0, 0] * np.eye(2*halfdim)
		sigma = np.zeros((2, halfdim, 2, halfdim), dtype="complex")
		sigma[0, :, 1, :] = np.eye(halfdim)
		sigma[1, :, 0, :] = np.eye(halfdim)
		sigma = sigma.reshape(2*halfdim, 2*halfdim)
		#x1_min = -0.075
		#print("Delta custom")
		#delta_custom = self.delta(x1_min)

		#res = contract("ij, ji", propagator, delta)
		L1_01 = self.select_dims(H1mn[:, :], 0, 1)
		#a = L1_01
		d = self.d()[:, 0, 1] * np.eye(halfdim)
		bigd = np.zeros((2, halfdim, 2, halfdim))
		bigd[0, :, 0, :] = d
		bigd = self.expectation(bigd.reshape(2*halfdim, 2*halfdim))
		bigd = self.select_dims(bigd, 0, 0)
		a = L1_01
		a = self.p()
		res = contract("ij, ji->", a, delta_e_half) * self.step


		#Just for testing
		if 1:
			# Tr[rho]
			rho = self.propagator(z1)[0, :halfdim] * np.eye(halfdim)
			trace = contract("jj->", rho)
			print("Tr[rho]: " + str(trace))

		if 1:
			F = Fl0mn
			#F = F.reshape(2, halfdim, 2, halfdim)
			#F[1, :, 1, :] = np.zeros((halfdim, halfdim))
			#F = F.reshape(2*halfdim, 2*halfdim)
			F = self.select_dims(Fl0mn, 0, 0)
			U = self.propagator(z1)[0, :halfdim] #* np.eye(2*halfdim)
			#U = self.select_dims(U, 0, 0)
			M = U[:, None] * F
			sigma = np.eye(2*halfdim)
			res = contract("ji, ij->", M, F)
			print("F: " + str(res))

		if 0:
			#The expectation value of d does not depend on the basis
			d_contr = contract("ij, ji->", d, delta_half)
			print("d contracted: " + str(d_contr))
			print(self.step)
			print("d(xdagger): " + str(d[self.jdagger, self.jdagger]))
			delta_contract = contract("ii->", delta_half)
			print("delta_con: " + str(delta_contract))
		return res
	#GO here

	def cff2000(self, z1, z2):
		"""H2F0H0F0"""
		beta = self.beta
		H2mn = - self.H2mn / self.hbar
		Fmn = self.Fl0mn

		zz = self.TimeOrderedDifference(z1, z2)
		Ul1 = self.propagator( zz )
		Ul2 = self.propagator(z2)
		Ur = self.propagator(beta - z1)

		res = np.zeros( (len(z1), len(z2)), dtype=complex )
		for i in range(len(z1)):
			for j in range( len(z2) ):
				a = Ul1[i, j, :, None] * H2mn[:, :]
				b = Ul2[j, :, None]    * Fmn[:, :]
				c = Ur[i, :, None]     * Fmn[:, :]
				res[i, j] = contract("ij, jk, ki->", a, b, c)
		#a = H2mn[:, :] * Ul1[..., :, None]
		#b = Fmn[:, :]  * Ul2[..., :, None]
		#c = Fmn[:, :]  * Ur[..., :, None]
		#res = contract("ijab, jbc, ica->ij", a, b, c)
		return res

	def cff0020(self, z1, z2):
		"""A redundant term (should be the same as H2F0H0F0)"""
		beta = self.beta
		H2mn = - self.H2mn / self.hbar
		Fmn = self.Fl0mn

		zz = self.TimeOrderedDifference(beta - z1, z2)
		Ul = self.propagator( z1 )
		Ur1 = self.propagator(zz)
		Ur2 = self.propagator(z2)

		res = np.zeros( (len(z1), len(z2)), dtype=complex )
		for i in range(len(z1)):
			for j in range( len(z2) ):
				a = Ul[i, :, None]     * Fmn[:, :]
				b = Ur1[i, j, :, None] * H2mn[:, :]
				c = Ur2[j, :, None]    * Fmn[:, :]
				res[i, j] = contract("ij, jk, ki->", a, b, c)
		return res

	def cff1000(self, z1, z2, z3):
		"""Second-order Dyson series in H1"""
		beta = self.beta
		H1mn = - self.H1mn / self.hbar
		#test = np.zeros_like(H1mn)
		#np.fill_diagonal(test, 1.0)
		Fmn = self.Fl0mn

		zz1 = self.TimeOrderedDifference(z1, z2)
		zz2 = self.TimeOrderedDifference(z2, z3)
		Ul1 = self.propagator(zz1)
		Ul2 = self.propagator(zz2)
		Ul3 = self.propagator(z3)
		Ur = self.propagator(beta - z1)

		res = np.zeros( (len(z1), len(z2), len(z3)), dtype=complex )
		for i in range(len(z1)):
			for j in range(len(z2)):
				for k in range(len(z3)):
					# a =  Ul1[i, j, :, None] * H1mn[:, :]
					# b =  Ul2[j, k, :, None] * H1mn[:, :]
					# c =  Ul3[k, :, None]    * Fmn[:, :]
					# d =  Ur[i, :, None]     * Fmn[:, :]
					# res[i, j, k] = contract("ij, jk, kl, li->", a, b, c, d)
					res[i, j, k] = contract("i, ij, j, jk, k, kl, l, li->", Ul1[i, j], H1mn, Ul2[j, k], H1mn, Ul3[k], Fmn, Ur[i], Fmn)

		return res

	def cff1010(self, z1, z2, z3):
		"""H1F0H1F0"""
		beta = self.beta
		H1mn = - self.H1mn / self.hbar
		Fmn = self.Fl0mn

		zz1 = self.TimeOrderedDifference(z1, z2)
		#We reverse when we integrate from 0 to negative t (due to beta - z)
		if (z1.imag != 0).any() and (z3.imag != 0).any():
			reverse = True
		else:
			reverse = False
		zz2 = self.TimeOrderedDifference(beta - z1, z3, reverse=reverse)
		Ul1 = self.propagator( zz1 )
		Ul2 = self.propagator(z2)
		Ur1 = self.propagator( zz2 )
		Ur2 = self.propagator( z3 )

		res = np.zeros( (len(z1), len(z2), len(z3)), dtype=complex )
		for i in range(len(z1)):
			for j in range(len(z2)):
				for k in range(len(z3)):
					# a = Ul1[i, j, :, None] * H1mn[:, :]
					# b = Ul2[j, :, None]    * Fmn[:, :]
					# c = Ur1[i, k, :, None] * H1mn[:, :]
					# d = Ur2[k, :, None]    * Fmn[:, :]
					# res[i, j , k] = contract("ij, jk, kl, li->", a, b, c, d)
					res[i, j , k] = contract("i, ij, j, jk, k, kl, l, li->", Ul1[i, j], H1mn, Ul2[j], Fmn, Ur1[i, k], H1mn, Ur2[k], Fmn)
		return res

	def cff1010_alt(self, z1, z2, z3):
		"""H1F0H1F0 alt"""
		beta = self.beta
		H1mn = - self.H1mn / self.hbar
		Fmn = self.Fl0mn

		def absolute(x):
			if (x.imag == 0).all():
				res = x.real
			elif (x.real == 0).all():
				res = x.imag
			res = np.abs(res)
			return res

		if (z1.imag != 0).any() and (z3.imag != 0).any():
			reverse = True
		else:
			reverse = False
		zz1 = self.TimeOrderedDifference(z1, z2)
		zz2 = self.TimeOrderedDifference(beta - z1, z3, reverse=reverse)
		Ul1 = self.propagator( zz1 )
		Ul2 = self.propagator(z2)
		Ur1 = self.propagator( zz2 )
		Ur2 = self.propagator( z3 )

		U1_int = np.zeros( np.shape(z1) + np.shape(H1mn), dtype=complex)
		U2_int = np.zeros( np.shape(z1) + np.shape(H1mn), dtype=complex)
		dx2 = absolute( (z2[-1] - z2[0])/len(z2) )
		dx3 = absolute( (z3[-1] - z3[0])/len(z3) )
		for i in range(len(z1)):
			#Building U1_int
			toBeIntegrated = np.zeros( (len(z2),) + np.shape(U1_int[0]), dtype=complex)
			for j in range(len(z2)):
				toBeIntegrated[j] +=  np.nan_to_num(
					# Ul1[i, j, :, None] * H1mn * Ul2[j, None, :]
					contract("m, mn, n->mn", Ul1[i, j], H1mn, Ul2[j])
				)
			U1_int[i] += integrate.simps(toBeIntegrated, axis=0, dx=dx2)

			#Building U2_int
			toBeIntegrated = np.zeros( (len(z3),) + np.shape(U2_int[0]), dtype=complex)
			for j in range(len(z3)):
				toBeIntegrated[j] += np.nan_to_num(
					# Ur1[i, j, :, None] * H1mn * Ur2[j, None, :]
					contract("m, mn, n->mn", Ur1[i, j], H1mn, Ur2[j])
				)
			U2_int[i] += integrate.simps(toBeIntegrated, axis=0, dx=dx3)

		res = np.zeros(len(z1), dtype=complex)
		for i in range(len(z1)):
			res[i] = contract("ij, jk, kl, li->", U1_int[i], Fmn, U2_int[i], Fmn)

		return res

	def cff1010_analytic(self, z1, z2, z3):
		"""H1F0H1F0 analytic for all imaginary"""
		beta = self.beta
		H1mn = - self.H1mn / self.hbar
		Fmn = self.Fl0mn
		energies = self.energiesl0

		def propmatrix(z):
			res = np.exp(-(energies[:,None] + energies[None, :]  )*z[..., None]/self.hbar)
			return res
		def integratedprop(zstart, zend):
			hbar = self.hbar
			energydiff = (energies[:,None] - energies[None, :])
			#Avoid division by zero
			np.fill_diagonal(energydiff, 1.)
			start = np.exp(-energydiff*zstart/hbar)
			end = np.exp(-energydiff*zend/hbar)
			res = (start - end) * hbar / energydiff
			np.fill_diagonal(res, (zend-zstart))
			return res

		constExp = propmatrix(z1)
		integral1 = integratedprop(z2[0], z2[-1])
		integral2 = integratedprop(z3[0], z3[-1])

		res = np.zeros(len(z1), dtype=complex)
		for i in range(len(z1)):
			res[i] = contract("mn, no, op, pm, mo, nm, po->", H1mn, Fmn, H1mn, Fmn, constExp, integral1, integral2)

		return res


class RPOneState():
	"""
	Ref.: Review paper = Ring-polymer instanton theory, J. O. Richardson
	https://doi.org/10.1080/0144235x.2018.1472353
	"""

	def __init__(self, PES0, beta, N_rec=512 ,xdagger=xdagger) -> None:
		self.beta = beta
		#self.gridl = gridl
		self.xdagger = xdagger
		self.PES0 = PES0
		self.hbar = PES0.UNITS.hbar
		self.mass = PES0.mass
		if N_rec % 2 == 0:
			self.N_rec = N_rec #The recommended number of RP beads (not including the fixed beads)
		else:
			print("N must be even")
			exit()

		#Optimisation
		xd = self.xdagger
		left_part = Trajectory(xd, xd, bounceno=1, direction="left")
		right_part = Trajectory(xd, xd, bounceno=1, direction="right")
		tau_half = self.beta*self.hbar / 2
		self.trajectory_left = self.optimise_traj(left_part, tau_half)
		self.trajectory_right = self.optimise_traj(right_part, tau_half)

	def optimise_traj(self, traj: Trajectory, tau, N=None, method="minimise") -> trajectory_RP:
		"""
		Stage 1: find the trajectory by optimisation
		Stage 2: generate the trajectory_RP object from trajectory.py
		"""
		if N is None:
			N = self.N_rec
		dt = tau / (self.N_rec)


		##Optimisation path
		fixed_path = path_integral.fixed(self.PES0, self.mass, traj.xi, traj.xf)

		#Constructing a guess trajectory
		#TODO: find a better way to guess the bouncepoint
		if traj.bounce:
			guess_bouncepoint = .1
			if traj.direction == "left": guess_bouncepoint *= -1
			guess_half1 = np.linspace(traj.xi, guess_bouncepoint, num = int(1+self.N_rec/2))
			gues_half2 = np.linspace(guess_bouncepoint, traj.xf, num = int(1+self.N_rec/2))
			guess = np.concatenate([guess_half1, gues_half2])[1:-1]
		else:
			guess = np.linspace(traj.xi, traj.xf, num = int(self.N_rec))

		def S(x):
			res = fixed_path.S(x, self.beta * self.hbar/2)
			return res

		def dSdx(x):
			res = fixed_path.dSdx(x, self.beta * self.hbar/2)
			return res

		if method == "minimise":
			traj = optimize.minimize(S, guess, jac=dSdx)
			res = traj.x
		elif method == "root":
			traj = optimize.root(dSdx, x0=guess)
			res = traj.x
		else:
			print("Invalid method selected")
			exit()

		optimised_x = fixed_path.addends(res)[:, None]

		#Step 2: Using trajectory.py
		optimised_V = np.array(
			[ self.PES0.potential(xx) for xx in optimised_x ]
			)
		optimised_dVdx = np.array(
			[ self.PES0.gradient(xx) for xx in optimised_x ]
			)
		optimised_d2Vdx2 = np.array(
			[ self.PES0.hessian(xx) for xx in optimised_x ]
			)
		if 0:
			print("Hi")
			print(np.shape(self.optimised_x))
			print(np.shape(self.optimised_V))
			print(np.shape(self.optimised_dVdx))
			print(np.shape(self.optimised_d2Vdx2))

		res = trajectory_RP.Trajectory(
			optimised_x,
			optimised_V,
			optimised_dVdx,
			optimised_d2Vdx2,
			dt = dt,
			m = self.mass,
			banded=False
		)
		return res

	def Flux(self, x=xdagger):
		"""The flux operator F0 (but without the delta function)"""
		p = self.trajectory_left.dWdq()[0]
		return p/self.mass

	def K_vV_half(self, part=0):
		if part == 0:
			traj = self.trajectory_left
		elif part == 1:
			traj = self.trajectory_right
		else:
			print("Part has to be either 0 or 1")
			exit()

		S = traj.S()
		d2Sdx2 = traj.d2Sdx2()

		C = - d2Sdx2[0,1]
		if 0:
			#print("C_RP: " + str(C))
			print("Half S_RP*2: " + str(2*S))
			d2Sdt2 = self.trajectory_left.d2Sdt2() * 2
			print("C_RP: " + str(C))
			print("d2Sdt2_RP: " + str(d2Sdt2))
			print("C_RP?: " + str(C**2 / d2Sdt2 *self.Flux()**2 ))
			#print("C_RP: " + str(C**2 * self.Flux() / d2Sdt2 ))
		res = np.sqrt(C/(2*np.pi*self.hbar)) * np.exp(-S/self.hbar)
		return res

	def K_vV(self, traj: trajectory_RP, tau):
		S = traj.S()
		d2Sdx2 = traj.d2Sdx2()
		C = - d2Sdx2[0,1]

		res = np.sqrt(C/(2*np.pi*self.hbar)) * np.exp(-S/self.hbar)
		return res

	def cff0000(self):
		""" cf.:
		Review paper: eq. 27 (but we have no Q coordinates)
		Review paper eq. 25 (for factors of 2)"""
		res = self.K_vV_half(part=0) * self.Flux(x=xdagger) * self.K_vV_half(part=1) * self.Flux(x=xdagger)

		if 0:
			S = self.trajectory_left.S()
			print("S_RP: " + str(S))
			E = self.trajectory_left.E()
			print("E_RP: " + str(E))
			p = self.trajectory_left.dWdq()[0]
			print("p_RP: " + str(p))
		return res

	def cff0000_integrated(self):
		"""Review paper: eq. 31 (but we have no Q coordinates)"""
		res = self.cff0000()
		d2Sdt2 = self.trajectory_left.d2Sdt2() *2
		res *= np.sqrt( (2*np.pi*self.hbar)/ -d2Sdt2 )
		return res

class RPTwoState(RPOneState):

	def __init__(self, PES0, PES1, PES, beta, N=512, xdagger=xdagger) -> None:
		super().__init__(PES0, beta, N, xdagger)
		self.PES1 = PES1
		self.PES = PES

	"""Note: a lot of functions are duplicates from SemiclassicTwoState"""
	def K_vV_0(self, traj, tau):
		res = self.K_vV(traj, tau)
		return res

	def d(self, x):
		"""x is a scalar"""
		res = self.PES.first_order_coupling(x)
		return res

	def p_1fp(self, xi, xf, tau):
		"""TODO: incomplete (missing the variation of the trajectory)?"""
		if tau == 0:
			return 0 #This is probably wrong
		res = - self.mass * (xf - xi) / tau
		return res

	def S_1_fp(self, xi, xf, tau):
		V = self.PES1.potential((xi+xf)/2)
		res = .5*self.mass* (xf-xi)**2/tau  +  V*tau
		return res

	def K_vV_1_fp(self, xi, xf, tau):
		"""
		van-Vleck propagator on the upper surface
		xi, xf are scalars
		"""
		if tau == 0:
			if xi == xf:
				return 1
			else:
				return 0

		hbar = self.hbar
		S = self.S_1_fp(xi, xf, tau)
		C = self.mass / tau #Maybe there should be a minus sign here?
		res = np.sqrt(C/(2*np.pi*hbar)) * np.exp(-S/hbar)
		return res

	def Fl1(self):
		"""F_1 (but without the delta function)"""
		hbar = self.hbar
		d = self.d(x=self.xdagger)

		res = np.zeros((2, 2), dtype=complex)
		commutator = 2*d[..., 0, 1]
		#commutator = (delta @ np.diag(d[:,0,1]) + np.diag(d[:,0,1]) @ delta)
		res[0, 1] =  commutator
		res[1, 0] = -commutator
		res *= - 1j * hbar / (2.*self.mass)
		return res

	def Lambdal2(self, x):
		"""TODO: maybe in the wrong place"""
		hbar = self.hbar
		A = self.d(x)[0, 1] * self.d(x)[1, 0]
		B = self.d(x)[1, 0] * self.d(x)[0, 1]
		res = np.zeros((2,2))
		res[0, 0] = A
		res[1, 1] = B
		res *= - hbar**2 / (2*self.mass)
		return res

	def Lambda_int(self):
		"""Integrates f along the given path"""

		#Quick, but not general fix (only for 1D)
		half_index = (self.N_rec + 2)//2
		path = self.trajectory_left.x[:half_index, 0]
		p_arr = np.sqrt(2*self.mass*(np.abs(
			self.trajectory_left.V[:half_index] - self.trajectory_left.E())
			))
		v_arr = -p_arr / self.mass
		#dt = self.dt
		y = np.array(
			[self.Lambdal2(xx)[0, 0] for xx in path]
		) / v_arr
		#print(y.shape)
		#print(path.shape)
		res = integrate.simps(y, x=path) * 2
		return res

	def cff2000(self):
		Lambda_int = self.Lambda_int()
		res = self.cff0000() * Lambda_int
		return res

	def cff1100(self):
		hbar = self.hbar
		beta = self.beta
		xd = self.xdagger
		halftime = beta * hbar/2

		def integrand_S0(tau, x1):
			left_part = Trajectory(xd, x1, bounceno=1, direction="left")
			left_traj = self.optimise_traj(left_part, halftime-tau)
			res = left_traj.S()
			return res

		def integrand_x1(tau, x1):
			left_part = Trajectory(xd, x1, bounceno=1, direction="left")
			left_traj = self.optimise_traj(left_part, halftime-tau)
			d = self.d(x1)
			p1 = self.p_1fp(xd, x1, tau)
			p0 = left_traj.dWdq()[1]
			op = 1j*hbar* d[..., 0, 1] * (p0 - p1)

			res = self.K_vV_0(left_traj, halftime-tau)
			res *= op
			res *= self.K_vV_1_fp(x1, xd, tau)
			return res

		def integrated_x1(tau):
			h = .0001
			x0 = 0

			a0 = 0
			b0 = (integrand_S0(tau, x0+h) - integrand_S0(tau, x0)) / h
			# print("b0: " + str(b0))
			a1 = .5*self.mass / tau
			b1 = 0
			a = a0 + a1
			b = b0 + b1
			#print("a: " + str(a))
			res = np.sqrt(np.pi/(a/hbar)) * np.exp((b**2/(4*a)) / hbar)
			return res

		if 1:
			a = .005*15. #TODO better plotting range?
			# a = gridl / 4
			x = np.linspace(-a, a, num=50, endpoint=False)
			y = np.array(
				[integrand_x1(tau_test, xx) for xx in x]
			)
			I1 = integrate.simps(y, x)
			I2 = integrated_x1(tau_test)
			print("Numerical integration: " + str(I1))
			print("Analytic integration: " + str(I2))

			fig, ax = plt.subplots()
			ax.plot(x, y.imag, label="Tau= " + str(tau_test))
			ax.set_ylabel("Integrand")
			ax.set_xlabel("x1")
			ax.legend()
			plt.show()
			exit()

		integrated_term, err = integrate.quad(integrated_x1, 0, halftime)
		#print(integrated_term)
		integrated_term *= 1./(2*self.mass)
		right_part = Trajectory(xd, xd, bounceno=1, direction="right")
		right_traj = self.optimise_traj(right_part, halftime)
		res = integrated_term * self.Fl1()[1, 0] * self.K_vV_0(right_traj, halftime) * self.Flux()
		return res

class SemiClassicOneState():

	def __init__(self, PES0, beta, xdagger=np.array([0., 0.]), gridl=15) -> None:
		"""
		gridl: limits for finding WKB turning points. Set it to something large.
		"""
		self.beta = beta
		self.gridl = gridl
		self.xdagger = xdagger
		self.PES0 = PES0
		self.hbar = PES0.UNITS.hbar
		self.mass = PES0.mass
		self.WKB_lower = WKBSystem(PES0.potential, PES0.mass)
		self.E_min = 0
		self.E_max = PES0.potential(0)
		self._half_traj = Trajectory(self.xdagger[0], self.xdagger[-1], bounceno=1, direction="right")
		# Most often, we will be using the half-trajectory, so it makes sense to precompute its corresponding energy.
		self.E_lower = self.E_from_beta(self._half_traj, beta = self.beta/2)
		#print(self.E_lower)
		self.turn_lower = np.array(
				self.WKB_lower.turning_points(self.E_lower,-self.gridl,self.gridl)
			)
		#Note: convergence parameterx
		h = .00001
		dturn = np.array(
				self.WKB_lower.turning_points(self.E_lower + h,-self.gridl,self.gridl)
			)
		dturn2 = np.array(
				self.WKB_lower.turning_points(self.E_lower - h,-self.gridl,self.gridl)
			)
		self.dturndE_lower = (dturn - self.turn_lower) / h
		self.d2turndE2_lower = (dturn - 2*self.turn_lower + dturn2) / h**2

	def E_from_beta(self, traj: Trajectory, beta=None, E_min=None, E_max=None, WKB: WKBSystem =None, mode="minimise"):
		"""Set E initally when given beta.
		traj is the trajectory, for which you set the energy.
		beta is the given beta

		The E is found by a minimisation algorithm.
		mode chooses a way to do it (minimise or brentq).
		E_max and E_min are the maximum and minimum allowed energies.

		WKB is the WKBSystem used for calculations.
		"""
		if beta is None:
			beta = self.beta
		if WKB is None:
			WKB = self.WKB_lower
		if E_min is None:
			E_min = self.E_min
		if E_max is None:
			E_max = self.E_max

		def Wder_from_E(E):
			turn = WKB.turning_points(E,-self.gridl,self.gridl, N=100)
			# print(turn)
			if len(turn) == 0:
				Wder = 0
			else:
				#Wder = WKB.dWdE(E, turn[0], turn[1])
				Wder = account_for_bounces(turn, traj,
						lambda xi, xf: WKB.dWdE(E, xi, xf)
					)
			return Wder

		Wder_goal = -self.hbar * beta

		if 0: #A graph
			E_arr = np.linspace(E_min, E_max, num=100)
			E_arr = E_arr[1:-1]
			Wder_arr = np.array(
				[ Wder_from_E(e) for e in E_arr ]
			)
			fig, ax = plt.subplots()
			ax.plot(E_arr, Wder_arr, label="Wder(E)")
			ax.set_xlabel("E")
			ax.set_ylabel("dW/dE")
			ax.axhline(y=Wder_goal,linestyle='--', label="goal")
			ax.legend()
			plt.show()
			exit()

		def should_be_zero(E):
			with warnings.catch_warnings():
				warnings.simplefilter("ignore")
				#The minimiser probably tries some not-convergent
				#values, so let us ignore the warnings
				res = (np.abs(Wder_from_E(E)) - np.abs(Wder_goal))
			return res

		if mode=="minimise":
			res = optimize.minimize_scalar(lambda x: np.abs(should_be_zero(x)), bounds=(E_min, E_max))
			res = res.x
		elif mode=="brentq":
			#Alternative: rewrite using brentq (root search)
			bracks = root.zbrak(should_be_zero, E_min, E_max, N=100)
			points = []
			for brack in bracks:
				point =  optimize.brentq(should_be_zero, brack[0], brack[1])
				points.append(point)

			res = points[-1]
		else:
			print("Invalid mode selected")
			# res = 0
			exit()

		rest = should_be_zero(res)
		if np.abs(rest) > 1.:
			print("Difference between betas: " + str(np.abs(rest)))
			print("Warning: optimisation failed. Make beta larger?")
		return res

	def beta_from_E(self):
		"""Just for completeness"""
		Wder = self.dWdE_tot()
		beta = -Wder/self.hbar
		return beta

	def W_tot(self):
		"""W(xi, xf, beta/hbar)"""
		#Hardcoded
		turn = self.turn_lower
		E = self.E_lower
		if len(turn) == 0:
			WW = 0.
		else:
			WW = 2*self.WKB_lower.W(E, turn[0], turn[1])
		return WW

	def dWdE_tot(self):
		#Hardcoded
		turn = self.turn_lower
		Wder = 2*self.WKB_lower.dWdE(self.E_lower, turn[0], turn[1])
		return Wder

	def d2WdE2_tot(self):
		#TODO check this analytically in the Eckart barrier
		#Hardcoded
		turn = self.turn_lower
		Wdder = 2*self.WKB_lower.d2WdE2(self.E_lower, turn[0], turn[1])
		return Wdder

	def S_tot(self):
		#Hardcoded
		W = self.W_tot()
		E = self.E_lower
		hbar = self.hbar
		beta = self.beta
		res = W + beta*hbar*E
		return res

	def K_vV_full(self):
		"""I compute the hessian using A7 (indirectly) in the review paper"""
		#Hardcoded
		S = self.S_tot()
		p1 = self.p(self.xdagger[0])
		p2 = self.p(self.xdagger[1])
		m = self.mass
		C = 1. / self.d2WdE2_tot() * (m**2 / p1 / p2)
		res = np.sqrt(C/(2*np.pi*self.hbar)) * np.exp(-S/self.hbar)
		if 0:
			print("S full: " + str(S))
			print("C full: " + str(C))
		return res

	def p(self, x, E=None):
		"""Semiclassical momentum"""
		if E is None:
			E = self.E_lower
		m = self.mass
		res =  np.sqrt(2*m*abs(self.PES0.potential(x)-E))
		return res

	def v(self, x):
		"""Semiclassical velocity"""
		return self.p(x) / self.mass

	def dpdx(self, x):
		"""Maybe not needed?"""
		E = self.E_lower
		m = self.mass
		res = np.sqrt(0.5*m/abs(self.PES0.potential(x)-E))
		res *= self.PES0.gradient(x)
		return res

	def dpdE(self, x):
		"""Note: this is just the partial derivative"""
		E = self.E_lower
		res = -np.sqrt(0.5*self.mass/abs(self.PES0.potential(x)-E))
		return res

	def Flux_tilde(self, x):
		"""The flux operator F0 (but without the delta function)"""
		res = 2* self.p(x=x)
		res = res / (2*self.mass)
		return res

	def C_half(self, part=0):
		#Hardcoded
		m = self.mass
		#Note: momentum should be zero at the turning point
		d2WdxdE_0 = -m / self.p()
		# d2WdxdE_0 += m / self.p(self.turn_lower[0])
		# d2WdxdE_0 += self.dturndE_lower[0] * self.p(self.turn_lower[0])
		d2WdxdE_1 = m / self.p() #+ m / self.p(self.turn_lower[1])
		#d2WdxdE_1 += self.dturndE_lower[1] * self.p(self.turn_lower[1])
		d2WdE2 = self.WKB_lower.d2WdE2(self.E_lower, self.xdagger[0], self.turn_lower[part])
		d2WdE2 = 2 * np.abs(d2WdE2)
		#This term diverges
		# d2WdE2 += np.abs(
		#       4*m/self.p(self.turxdan_lower[part]) * (self.dturndE_lower[part])
		#       )
		# d2WdE2 += np.abs(
		#     2*self.dpdx(self.turn_lower[part]) * (self.dturndE_lower[part])**2
		# )
		# d2WdE2 += np.abs(
		#     2*self.p(self.turn_lower[part]) * self.d2turndE2_lower[part]
		#     )
		#print(self.p(self.turn_lower[part]) * self.d2turndE2_lower[part])

		C = - d2WdxdE_1 * d2WdxdE_0 / d2WdE2
		C = np.abs(C)
		return C

	def K_vV_half(self, part=0):
		#Hardcoded
		time = self.beta * self.hbar / 2
		S  =  self.WKB_lower.W(self.E_lower, self.xdagger[0], self.turn_lower[part])
		print("W: " + str(S))
		#S += self.WKB_lower.W(self.E, self.turn[part], self.xdagger[0])
		S = 2*np.abs(S)
		S += time * self.E_lower

		C = self.C_half(part=part)

		if 0:
			print("Half S_SC*2: " + str(2*S))
			print("C_SC: " + str(C))
		res = np.sqrt(C/(2*np.pi*self.hbar)) * np.exp(-S/self.hbar)
		return res


	def S(self, traj: Trajectory, beta, E):
		"""
		The semiclassical action.
		If bounce is true, it goes to the turning point and bounces back.
		Direction: if bounceno=1, then it is not clear
		in which direction the trajectory should go. This is then
		decided by direction, which can be "left" or "right".
		"""
		#Calculate the energy
		W = account_for_bounces(self.turn_lower, traj,
				lambda xi, xf: self.WKB_lower.W(E, xi, xf)
				)
		#TODO: ask about the absolute value here in W

		tau = beta * self.hbar
		S = np.abs(W) + self.E_lower * tau
		return S

	def C(self, traj: Trajectory, beta, E):
		"""
		d2S/dxidxf
		Note: without the Leibniz rule terms (because they dont seem to work)
		"""
		#Calculate the energy

		m = self.mass

		#Note: momentum should be zero at the turning point
		d2WdxdE_i = -m / self.p(traj.xi, E)
		d2WdxdE_f =  m / self.p(traj.xf, E)
		#traj.print()
		#print(E)
		with warnings.catch_warnings():
			#Warnings about slow convergence get printed and I don't know why
			warnings.simplefilter("ignore")
			d2WdE2 = account_for_bounces(self.turn_lower, traj,
						lambda xi, xf: self.WKB_lower.d2WdE2(E, xi, xf)
					)
		# d2WdE2 = 1
		d2WdE2 = np.abs(d2WdE2)

		C = - d2WdxdE_i * d2WdxdE_f / d2WdE2
		C = np.abs(C)
		return C

	def K_vV(self, traj: Trajectory, beta, onlyS=False):
		"""The van-Vleck propagator for a given trajectory.
		"""
		hbar = self.hbar
		# given beta, we have to compute E
		E = self.E_from_beta(traj, beta, mode="minimise")
		#print("E: " + str(E))

		S = self.S(traj, beta, E)
		C = self.C(traj, beta, E)
		# C2 = self.C(traj, tau, E*1.00001)
		# print(C)
		# print(C2)

		res = np.exp(-S/hbar)
		#print("C: " + str(C))
		#print("S: " + str(S))
		#print("Sterm: " + str(res))
		if not onlyS:
			res *= np.sqrt(C/(2*np.pi*hbar))
		return res

	def cff0000(self, beta_in=None):
		"""cff0000(beta_in)"""
		xd = self.xdagger
		if beta_in is None:
			beta_in = self.beta/2
		left_part = Trajectory(xd[0], xd[1], bounceno=1, direction="left")
		right_part = Trajectory(xd[1], xd[0], bounceno=1, direction="right")

		res = self.K_vV(left_part, beta_in) * self.Flux_tilde(self.xdagger[0]) * self.K_vV(right_part, self.beta - beta_in) * self.Flux_tilde(self.xdagger[1])
		res *= 2 # LR + RL

		if 0: # printing logs
			time = self.beta * self.hbar / 2
			S  =  self.WKB_lower.W(self.E_lower, self.xdagger[0], self.turn_lower[1])
			#S += self.WKB_lower.W(self.E, self.turn[part], self.xdagger[1])
			S = 2*np.abs(S)
			S += time * self.E_lower
			print("S_SC: " + str(S))
			print("E_SC: " + str(self.E_lower))
			print("p_SC: " + str(self.p()))
		return res

	def cff0000_integrated(self):
		#TODO: modernise this
		"""Review paper: eq. 31 (but we have no Q coordinates)"""
		res = self.cff0000()
		xd = self.xdagger
		left_part = Trajectory(xd[0], xd[1], bounceno=1, direction="left")
		right_part = Trajectory(xd[1], xd[0], bounceno=1, direction="right")
		tau = self.beta* self.hbar /2
		E = self.E_lower
		d2Sdt2 = -self.Flux_tilde(self.xdagger[0])**2 * self.C(left_part, tau, E)
		d2Sdt2 += -self.Flux_tilde(self.xdagger[1])**2 * self.C(right_part, tau, E)
		res *= np.sqrt( (2*np.pi*self.hbar)/ -d2Sdt2 )
		return res


class SemiClassicTwoState(SemiClassicOneState):

	def __init__(self, PES0, PES1, PES, beta, xdagger=np.array([0., 0.]), gridl=15) -> None:
		super().__init__(PES0, beta, xdagger=xdagger, gridl=gridl)
		self.PES1 = PES1
		self.PES = PES
		self.WKB_upper = WKBSystem(PES1.potential, PES1.mass)
		#E_min = PES1.potential(0)
		#E_max = PES1.potential(gridl*.99)
		#self.E_upper = self.E_from_beta(E_min, E_max, WKB=self.WKB_upper)
		#self.turn_upper = np.array(
		#        self.WKB_upper.turning_points(self.E_upper,-self.gridl,self.gridl)
		#    )
		#Note: convergence parameter
		#h = .00001
		#dturn = np.array(
		#        self.WKB_upper.turning_points(self.E_upper + h,-self.gridl,self.gridl)
		#    )
		#self.dturndE_upper = (dturn - self.turn_upper) / h
		#print("Initialised SC with beta: " + str(beta))
		#print(self.v(self.turn_lower[1]))
		#print(self.v(0))
		#exit()

	def d(self, x=None):
		"""x is a scalar"""
		if x is None:
			x = self.xdagger[0]
		res = self.PES.first_order_coupling(x)
		return res

	def p_lower(self, x):
		"""Semiclassical momentum on the lower state"""
		return self.p(x)

	def p_upper(self, x):
		"""Semiclassical momentum on the upper state"""
		"""sus"""
		E = self.E_upper
		m = self.mass
		res =  np.sqrt(2*m*abs(self.PES1.potential(x)-E))
		return res

	def K_vV_0(self, traj, beta, onlyS=False):
		res = self.K_vV(traj, beta, onlyS=onlyS)
		return res

	def p_1fp(self, xi, xf, beta):
		"""TODO: incomplete (missing the variation of the trajectory)?"""
		if beta == 0:
			return 0 #This is probably wrong
		tau = beta*self.hbar
		res = - self.mass * (xf - xi) / tau
		return res


	def S_1_fp(self, xi, xf, beta):
		tau = beta*self.hbar
		V = -self.PES1.potential((xi+xf)/2)
		res = .5*self.mass/tau * (xf-xi)**2  +  V*tau
		return res

	def d2S_1_fp_dtau2(self, xi, xf, beta):
		tau = beta*self.hbar
		res = self.mass/tau**3 * (xf-xi)**2
		return res

	def C_1_fp(self, xi, xf, beta):
		tau = beta*self.hbar
		res = self.mass / tau
		return res

	def K_vV_1_fp(self, xi, xf, beta, onlyS=False):
		"""
		van-Vleck propagator on the upper surface
		xi, xf are scalars
		"""
		if beta == 0:
			if xi == xf:
				return 1
			else:
				return 0

		hbar = self.hbar

		S = self.S_1_fp(xi, xf, beta)
		C = self.mass / beta #Maybe there should be a minus sign here?
		res = np.exp(-S/hbar)
		if not onlyS:
			res *= np.sqrt(C/(2*np.pi*hbar))
		if 0:
			# print("V part: " + str(V*tau))
			print("T part: " + str(.5*self.mass* (xf-xi)**2/beta))
			print("T part 1: " + str((xf-xi)**2))
			print("T part 2: " + str(1./beta))
			print("T part 3: " + str(.5*self.mass))
			print(np.sqrt(C/(2*np.pi*hbar)))
			print("res: " + str(res))
		return res

	def Fl1_tilde(self, x=None):
		"""F_1 (but without the delta function)"""
		if x is None:
			x = self.xdagger[0]
		hbar = self.hbar
		d = self.d(x=x)

		res = np.zeros((2, 2), dtype=complex)
		commutator = 2*d[..., 0, 1]
		#commutator = (delta @ np.diag(d[:,0,1]) + np.diag(d[:,0,1]) @ delta)
		res[0, 1] =  commutator
		res[1, 0] = -commutator
		res *= - 1j * hbar / (2.*self.mass)
		return res

	def Lambdal1(self, x):
		hbar = self.hbar
		pl = self.p_lower()
		pu = self.p_upper()
		res[0,1] =  2 * pl * self.d()
		res[1,0] = -2 * pu * self.d()
		res *= - 1j * hbar / (2.*self.mass)
		return res

	def Lambdal2(self, x):
		hbar = self.hbar
		A = self.d(x)[0, 1] * self.d(x)[1, 0]
		B = self.d(x)[1, 0] * self.d(x)[0, 1]
		res = np.zeros((2,2))
		res[0, 0] = A
		res[1, 1] = B
		res *= - hbar**2 / (2*self.mass)
		return res

	def Lambda_int(self):
		def integrand(x):
			res = self.Lambdal2(x)[0, 0] /self.v(x)
			return res
		#def derivative(x, h = .001):
		#    res = ( integrand(x + h) - integrand(x) ) / h
		#    return res
		xstart = 0
		xend = self.turn_lower[0]
		plot = False
		if plot:
			nquad = 1280 + 1
			x = np.linspace(start=xstart, stop=xend, num=nquad)
			y = [ integrand(xx) for xx in x ]
			fig, ax = plt.subplots()
			ax.plot(x, y, label="Lambda_00")
			ax.set_xlabel("x")
			ax.set_ylabel("Lambda_00(x(tau))")
			plt.show()

		Lambda_int, err = integrate.quad(integrand, xstart, xend)
		# print("Lamba_int: " + str(Lambda_int) + " error: " + str(err))
		# print(integrand(xend))
		return Lambda_int*2

	# Just for testing:
	#TODO: re-interpret bounce as the bounces added to the minimum number of bounces
	def trRhoF0F0(self, beta_full, prop=1.):
		xd = self.xdagger
		trajplus = Trajectory(xd[0], xd[1], direction="right", bounceno=1)
		trajminus = Trajectory(xd[1], xd[0], direction="left", bounceno=1)
		#trajtest = Trajectory(xd, xd, direction="left", bounceno=0)
		#Ktest = self.K_vV(trajtest, beta_full*0)
		#print(Ktest)
		#exit()
		Kminus = self.K_vV(trajminus, beta_full*(1.-prop))
		Kplus = self.K_vV(trajplus, beta_full*prop)
		#print(Kminus)
		#print(Kplus)

		Fp = self.Flux_tilde(self.xdagger[0])
		Fm = self.Flux_tilde(self.xdagger[1])
		res = Kplus * Fp * Kminus * Fm
		res *= 2
		return res

	def trRhodelta(self, beta):
		xd = self.xdagger
		trajplus = Trajectory(xd[0], xd[1], direction="right", bounceno=1)
		trajminus = Trajectory(xd[1], xd[0], direction="left", bounceno=1)
		Kplus = self.K_vV(trajplus, beta)
		Kminus = self.K_vV(trajminus, beta)
		res = Kplus + Kminus
		return res

	def trRhoF1(self, beta):
		xd = self.xdagger
		trajplus = Trajectory(xd[0], xd[1], direction="right", bounceno=1)
		trajminus = Trajectory(xd[1], xd[0], direction="left", bounceno=1)
		Kplus = self.K_vV(trajplus, beta)
		Kminus = self.K_vV(trajminus, beta)
		Fl1 = self.Fl1_tilde()
		F1_01 = Fl1[0, 1]
		F1_10 = Fl1[1, 0]
		#res = (Kplus + Kminus) * F1_01 * (F1_10)
		d = self.d()[0, 1]
		const = - 2*1j * self.hbar / (2.*self.mass) * d
		res = (Kplus + Kminus) * F1_01
		#res /= const
		return res

	def cff2000(self):
		""" """
		Lambda_int = self.Lambda_int()
		res = Lambda_int * self.cff0000()
		return res

	def cff1100(self, showplot=False, method="SD1"):
		#TODO: change for 2 dividing surfaces
		hbar = self.hbar
		beta = self.beta
		xd = self.xdagger
		halfbeta = beta/2
		# Just for plotting
		a_short = .005*self.gridl

		def integrand_S1(beta, x1):
			res = self.S_1_fp(xd[0], x1, beta)
			return res

		def integrand_S0(beta, x1):
			traj = Trajectory(x1, xd[0], bounceno=1, direction="left")
			S = self.S(traj, beta, self.E_lower)
			return S

		def Pulse_full(beta, x1):
			"""
			Evaluate the "pulse" term
			I(tau, x1) = K0(x1,xd,beta hbar/2-tau) d(x1)(p0(x1) + p1(x1)) K1(xd, x1, tau) / (2M)
			fully
			Since the quadratic approximation is so accurate and can be
			integrated analytically, this is just for debugging.
			"""
			d = self.d(x1)
			p1 = self.p_1fp(xd[0], x1, beta)
			p0 = self.p_lower(x1)
			op = 1j*hbar * d[..., 0, 1] * (p0 + p1)
			# op = 1.

			left_part = Trajectory(x1, xd[0], bounceno=1, direction="left")
			res = self.K_vV_0(left_part, halfbeta-beta)
			# print("Propagator (SC): " + str(res))
			res *= op
			res *= self.K_vV_1_fp(x1, xd[0], beta)
			res *= 1./(2*self.mass)
			if 0:
				print(x1.shape)
				print(p1.shape)
				print(d.shape)
				print(res.shape)
				print(self.K_vV_1_fp(x1, xd, beta).shape)
				print(x1)
				print(res)
			return res

		def Pulse_quadratic(beta, x1=None, rettype=1):
			"""
			Evaluate the "pulse" term
			I(tau, x1) = K0(x1,xd,beta hbar/2-tau) d(x1)(p0(x1) + p1(x1)) K1(xd, x1, tau)
			with a quadratic approximation to the actions

			if x1 is None, evaluate instead the integral of I(tau, x1) over all x1

			Procedure:
			1) find x1_min
			2) calculate coefficients for quadratic approximations of S0 and S1
			3) either plot or integrate the function over x1 using the gaussian integral formula
			"""
			def S_sum(x):
				S0 = integrand_S0(halfbeta-beta, x)
				S1 = integrand_S1(beta, x)
				res = S0 + S1
				return res
			x1_min = optimize.minimize_scalar(S_sum, 0, bounds=(-self.gridl, self.gridl))
			x1_min = x1_min.x
			#x1_min = xd

			h = .00001
			# h = np.abs(x1_min) / 1000
			a0 = (integrand_S0(halfbeta-beta, x1_min + h) -2 *integrand_S0(halfbeta-beta, x1_min) + integrand_S0(halfbeta-beta, x1_min - h)) / h**2 /2
			b0 = (integrand_S0(halfbeta-beta, x1_min + h) - integrand_S0(halfbeta-beta, x1_min)) / h
			c0 = integrand_S0(halfbeta-beta, x1_min)

			C = self.PES1.potential( (xd[0]+x1_min)/2 ) * beta
			a1 = .5*self.mass / beta
			b1 = 2*(a1*(x1_min - xd[0]))
			c1 = (a1*(xd[0]-x1_min)**2+C)

			cp = c0 + c1
			ap = a0 + a1
			bp = b0 + b1
			am = a0 - a1
			bm = b0 - b1

			def S0(x):
				res = a0*(x-x1_min)**2 + b0*(x-x1_min) + c0
				return res

			def S(x):
				return ap*(x-x1_min)**2 + bp*(x-x1_min) + cp

			#C and d are slowly varying
			d = self.d(x1_min)
			op = 1j*hbar * d[..., 0, 1] #* (self.p_lower(x1_min) - p1)
			left_part = Trajectory(x1_min, xd[0], bounceno=1, direction="left")
			C0 = self.C_1_fp(xd[0], x1_min, beta)
			C1 = self.C(left_part, halfbeta-beta, self.E_lower)
			C_term = np.sqrt(C0*C1)/(2*np.pi)

			if x1 is not None:
				E = np.exp(-S(x1))
				res = (-2*am*(x1-x1_min) - bm) * E
				res *= 1./(2*self.mass)

				#TODO: why?
				res *= -1
				if rettype==1:
					res *=  C_term * op
				#For testing
				else:
					res *= op
				if rettype==2:
					return 1j*C_term
				if rettype==3:
					return op
				if rettype==4:
					return S(x1)
				return res
			else:
				first  = np.sqrt(complex(np.pi/(ap**3/hbar))) * np.exp((bp**2/(4*ap)) / hbar) * am *bp
				second = -np.sqrt(complex(np.pi/(ap/hbar))) * np.exp((bp**2/(4*ap)) / hbar) *bm
				S_part = (first + second)*np.exp(-cp/hbar)
				res = C_term * op * S_part
				res *= 1./(2*self.mass)

				#TODO: why?
				res *= -1
				if rettype==1:
					return res
				elif rettype==2:
					return res, x1_min
				else:
					print("Unsupported return type")
					exit()

		if 0: #Display the pulse term to verify that the quadratic approximation holds
			#TODO: do the QM version of this
			# a_short = gridl/4
			x = np.linspace(-a_short, a_short, num=100, endpoint=False)
			y = np.array(
				[Pulse_full(tau_test, xx) for xx in x]
			)
			y2 = np.array(
				[Pulse_quadratic(tau_test, xx, rettype=1) for xx in x]
			)
			#y3 = y2.imag / y.imag
			#print(y3)
			I1 = integrate.simps(y, x).imag
			I22 = integrate.simps(y2, x).imag
			I2, x1_min = Pulse_quadratic(tau_test, rettype=2)
			I2 = I2.imag
			print("Numerical integration (full): " + str(I1))
			print("Analytic integration  (steepest-descent): " + str(I2))
			print("Numerical integration (steepest-descent): " + str(I22))

			fig, ax = plt.subplots()
			ax.plot(x, y.imag, label="S full, tau= " + str(tau_test))
			ax.plot(x, y2.imag, label="S quadratic, tau= " + str(tau_test))
			plt.axvline(x1_min, color="black", label="x1_min")
			#ax.plot(x, y3, label="ratio" + str(tau_test))
			ax.set_ylabel("Integrand(x_1)")
			ax.set_xlabel("x_1")
			ax.set_title("I(tau, x_1) = K_1*d(p0-p1)*K_0")
			ax.legend()
			plt.show()
			exit()

		def integrand_tau(beta):
			"""Very slow, for the analytic integration"""
			res, err = integrate.quad(lambda x: Pulse_full(beta, x).imag, -self.gridl, self.gridl)
			res *= 1j
			return res

		small = 0.0001
		x = np.linspace(small, halfbeta, num=100)[:]
		if method == "SD1":
			func = Pulse_quadratic
		else:
			func = integrand_tau

		h = .001
		der = (func(small+h) - func(small)) / h
		A = func(small)
		B = -der / A
		def approx(x):
			res = A * np.exp(-B*x)
			return res

		y = np.array(
			[func(tt) for tt in x]
		)
		y_approx = np.array(
			[approx(tt) for tt in x]
		)
		integrated_term = 1j* integrate.simps(y.imag, x)
		# approx_term = 1j* integrate.simps(y_approx.imag, x)
		if showplot:
			fig, ax = plt.subplots()
			ax.plot(x, y.imag, label="Full")
			ax.plot(x, y_approx.imag, label="Approx")
			ax.set_ylabel("I(tau) integrated")
			ax.set_xlabel("tau")
			ax.legend()
			plt.show()
			#exit()

		right_part = Trajectory(xd[1], xd[0], bounceno=1, direction="right")
		other = self.Fl1_tilde()[1, 0] * self.K_vV_0(right_part, beta*hbar/2) * self.Flux_tilde()
		res = integrated_term * other
		res = res.real

		finalplot = True
		if finalplot:
			y *= other
			y_approx *= other
			if 1:
				data = {'y': y, 'y_approx': y_approx, 'x':x}
				name = './cffdata/test.pkl'
				outputfile = open(name,'wb')
				pickle.dump(data, outputfile)
				print("pickle file written")

			if showplot:
				fig, ax = plt.subplots()
				ax.plot(x, y, label="Full")
				ax.plot(x, y_approx, label="Approx")
				ax.set_ylabel("cff1100(beta, tau)")
				ax.set_xlabel("tau")
				if method == "SD1":
					word = "(I steepest-descent)"
				else:
					word = "(I numerical)"
				ax.set_title("Semiclassic " + word)
				ax.legend()
				plt.show()
		return res

	def cff1100_integrated(self):
		res = self.cff1100(showplot=False)
		xd = self.xdagger
		left_part = Trajectory(xd[0], xd[1], bounceno=1, direction="left")
		tau = self.beta* self.hbar /2
		E = self.E_lower
		d2Sdt2 = -self.Flux_tilde()**2 * self.C(left_part, tau, E)*2
		res *= np.sqrt( (2*np.pi*self.hbar)/ -d2Sdt2 )
		return res

	def cff0101(self):
		xd = self.xdagger
		hbar = self.hbar
		beta = self.beta
		right_part = Trajectory(xd[0], xd[1], bounceno=1, direction="right")
		left_part  = Trajectory(xd[1], xd[0], bounceno=1, direction="left")
		another = Trajectory(xd[0], xd[0], bounceno=2, direction="left")
		Fl1l = self.Fl1_tilde(xd[0])
		Fl1r = self.Fl1_tilde(xd[1])
		# Hypothesis for the deformed contour
		K_0_right = self.K_vV(right_part, beta)
		K_0_left  = self.K_vV(left_part,  beta)
		K_0_another  = self.K_vV(another,  beta)
		K_1 = 1.
		res = K_1 * Fl1l[0, 1] * (K_0_right + K_0_left) * Fl1r[1, 0]
		#res = K_1 * Fl1[0, 1] * (K_0_anotxdher) * Fl1[1, 0]
		res *= 2 # For K0 F K1 F
		res = res.real
		return res

	def cff1000(self, method="Full"):
		xd = self.xdagger
		hbar = self.hbar
		beta_half = self.beta * hbar /2
		simple_path = Trajectory(xd[0], xd[1], bounceno=1, direction="right")
		Fl1 = self.Fl1_tilde(self.xdagger[0])
		Fl0 = self.Flux_tilde(self.xdagger[0])
		K_0 = self.K_vV(simple_path, beta_half*hbar)
		res = Fl0 * K_0 * Fl0

		hard_part = 0
		if method == "Full":
			def double_int(beta1, beta2, x1, x2):
				#We assume that everything happens before the bounce
				assert np.abs(x1) < np.abs(x2)
				assert np.abs(x2) < np.abs(self.turn_lower[-1])
				first  = Trajectory(xd, x1, bounceno=0, direction="left")
				last   = Trajectory(x2, xd, bounceno=1, direction="left")
				K_first  = self.K_vV(first, beta2)
				K_middle = self.K_vV_1_fp(x1, x2, beta1-beta2)
				K_last   = self.K_vV(last, beta_half - beta1)

				# TODO: not sure about the signs of p1
				p0_first = self.p_lower(x1)
				d_first  = self.d(x1)[..., 0, 1]
				p1_first = self.p_1fp(x1)
				Lambda1_first = 1j * hbar * d_first * ( p0_first + p1_first ) /(2*self.mass)

				p0_last  = self.p_lower(x2)
				d_last = self.d(x2)[..., 0, 1]
				p1_last = self.p_1fp(x2)
				Lambda1_last = 1j * hbar * d_last * (p1_last + p0_last) /(2*self.mass)

				res = K_last * Lambda1_last * K_middle * Lambda1_first * K_first
				return res

			tau1 = tau_test
			tau2 = tau1 / 2
			x1 = -.1
			x2 = -.2

		res *= hard_part
		return res

def print_crossover(PES, logger=None):
	"""
	Crossover temperature: first normal mode freq of a free RP becomes smaller
	than the freq at the top of the barrier
	fRP = 2*np.pi/(beta*Rate.hbar)
	"""
	ftop = np.sqrt(-PES.hessian(0)[0, 0]/PES.mass)
	beta_crossover = 2*np.pi / (PES.UNITS.hbar * ftop)

	message = "beta_crossover: " + str(beta_crossover)
	if logger is None:
		print(message)
	else:
		logger.info(message)

if __name__ == '__main__':

	# Look into tests/rates/ to see how to use this file

	########
	#TODO: one remaining test has no place so far
	from exp_pot.PES.tully import APES, APESBH, APES0, APES1, DPES, DPES0, DPES1
	PES = APES0
	print_crossover(APES0)

	beta = 1000
	print("beta: " + str(beta))

	RateSC_single = SemiClassicOneState(APES0, beta)
	Zr =  np.sqrt(APES0.mass/(2*np.pi*beta * APES.UNITS.hbar**2) )
	##WKB rate
	#kWKB = (2*np.pi*RateSC.hbar)**(-1./2.) * (RateSC.d2WdE2_tot())**(-1./2.) * np.exp( - RateSC.S_tot()/RateSC.hbar) / Zr
	"""cf.: Review paper: eq. 32 using dS/dtau = dS/dq * v becomes eq. 4,
	then we use eq. 33 (we have no Q coordinates, because we are in 1D)"""
	kWKB = RateSC_single.K_vV_full() * RateSC_single.v() / Zr
	print("v: " + str(RateSC_single.v()))
	print("Rate (WKB): " + str(kWKB))
	# exit()

	#TODO: test the ring-polymer rates