import numpy as np

from bbo.dvr1d import OpenInterval
from bbo.PES.tully import APES, APESBH, APES0, APES1, Eck, DPES, DPES0, DPES1


gridl = 15
lb, ub = -gridl, gridl
npts = 200 + 1
# Pad the grid to 2**k + 1 (for consistency)
#npts = np.power(2, int(np.ceil(np.log2(npts))))+1
E_max0 = np.inf
E_max1 = np.inf
E_cutoff = None #TODO when this cutoff changes, the real gridl changes too
#todo bigger barrier height than delta

#How to choose the parameters?
#E_max:
#no of energy eigenvalues is significantly smaller than npts
#different for both PESs
#npts: sufficient density
#odd
#gridl: should go to infty
#Fix E_max, gridl:
#vary npts until convergence
#expand gridl while keeping density, see if it changes
#Converge wrt E_max
#E_max ~ 10 * 1/beta
#E_cutoff: (relevant for bound potentials)

dvrBH = OpenInterval(APESBH, lb=lb, ub=ub, npts=npts, E_max=E_max0, E_cut=E_cutoff)
dvr0 = OpenInterval(APES0, lb=lb, ub=ub, npts=npts, E_max=E_max0, E_cut=E_cutoff)
dvr1 = OpenInterval(APES1, lb=lb, ub=ub, npts=npts, E_max=E_max1, E_cut=E_cutoff)

dvrE = OpenInterval(Eck, lb=lb, ub=ub, npts=npts, E_max=E_max1, E_cut=E_cutoff)

Ddvr0 = OpenInterval(DPES0, lb=lb, ub=ub, npts=npts, E_max=E_max0, E_cut=E_cutoff)
Ddvr1 = OpenInterval(DPES1, lb=lb, ub=ub, npts=npts, E_max=E_max1, E_cut=E_cutoff)

plotEckart = False

#print(dvr1.evals)
#print(len(dvr1.evecs[1]))
#
#exit()

if __name__ == '__main__':
	import matplotlib.pyplot as plt

	print("npts: " + str(npts))
	fig, ax = plt.subplots(1,2,figsize=(12,5))
	Dfig, Dax = plt.subplots(1,2,figsize=(12,5))
	ax[1].set_title("Adiabatic 1")
	ax[0].set_title("Adiabatic 0")
	Dax[1].set_title("Diabatic 1")
	Dax[0].set_title("Diabatic 0")
	fig.suptitle("Adiabatic potentials and dvr solutions")
	Dfig.suptitle("Diabatic potentials and dvr solutions")
	y0 = np.zeros_like(dvr0.masked_grid)
	y1 = np.zeros_like(dvr1.masked_grid)
	Dy0 = np.zeros_like(Ddvr0.masked_grid)
	Dy1 = np.zeros_like(Ddvr1.masked_grid)
	if plotEckart:
		Efig, Eax = plt.subplots()
		Eax.set_title("Eckart")
		Efig.suptitle("Eckart potential and dvr solution")
		Ey = np.zeros_like(dvrE.masked_grid)
	for i in range(len(dvr0.masked_grid)):
		y0[i] = APES0.potential(dvr0.masked_grid[i])
		y1[i] = APES1.potential(dvr1.masked_grid[i])
		Dy0[i] = DPES0.potential(Ddvr0.masked_grid[i])
		Dy1[i] = DPES1.potential(Ddvr1.masked_grid[i])
		if plotEckart:
			Ey[i] = Eck.potential(dvrE.masked_grid[i])


	ax[0].plot(dvr0.masked_grid, y0, 'k-', label="V0")
	ax[1].plot(dvr1.masked_grid, y1, 'k-', label="V1")

	Dax[0].plot(dvr0.masked_grid, Dy0, 'k-', label="V0")
	Dax[1].plot(dvr1.masked_grid, Dy1, 'k-', label="V1")

	if plotEckart: Eax.plot(dvrE.masked_grid, Ey, 'k-', label="V")

	def plotEvecs(dvr, ax, imax=10, fac=0.003):
		i = 0
		for eval, evec in zip(dvr.evals, dvr.evecs):
			ax.plot(dvr0.masked_grid, (evec) *fac  + eval, label="psi"+str(i))
			i += 1
			if i == imax:
				break

	plotEvecs(dvr0, ax[0], imax=20)
	plotEvecs(dvr1, ax[1], imax=10)

	plotEvecs(Ddvr0, Dax[0], imax=-1)
	plotEvecs(Ddvr1, Dax[1], imax=50)

	if plotEckart: plotEvecs(dvrE, Eax, imax=50)

	ax[0].legend()
	ax[1].legend()

	#ax[1].set_xlim( [-4,4] )
	#ax[1].set_ylim( [0.04,.12] )

	#Dax[0].set_xlim( [-4,4] )
	#Dax[1].set_xlim( [-4,4] )
	#Dax[0].set_ylim( [0.00,.12] )
	#Dax[1].set_ylim( [0.00,.12] )
	Dax[0].legend()
	Dax[1].legend()

	plt.show()