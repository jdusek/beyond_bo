import numpy as np
from polylib.WKB import System as WKBSystem
import pickle

from bbo.rates import AdiabaticExact, DiabaticExact, BornOppenheimer, Perturbative
from bbo.PES.tully import APES, APESBH, APES0, APES1, DPES, DPES0, DPES1, Eck
from bbo.dvr.tully import dvrBH, dvr0, dvr1, dvrE, Ddvr0, Ddvr1, gridl, npts



rateEckart = False

dim = npts
print("Dimension: " + str(dim))

####
#Setup (beta etc.)
##Setting beta
ftop = np.sqrt(-APES0.hessian(0)[0, 0]/APES0.mass)
# print(ftop )
beta_crossover = 2*np.pi / (APES0.UNITS.hbar * ftop)
print("beta_crossover: " + str(beta_crossover))
#Crossover temperature: first normal mode freq of a free RP becomes smaller
#than the freq at the top of the barrier
#fRP = 2*np.pi/(beta*Rate.hbar)
beta = 1000
#beta = 10
# Eguess = E_from_beta(beta)
print("beta: " + str(beta))


####
#Born-Oppenheimer
if rateEckart:
    RateBO = BornOppenheimer(PES=Eck, dvr=dvrE, beta=beta)
    WKB = WKBSystem(Eck.potential, Eck.mass)
else:
    RateBO = BornOppenheimer(PES=APES0,dvr=dvr0, beta=beta)
    WKB = WKBSystem(APES0.potential, APES0.mass)
    RateBH = BornOppenheimer(PES=APESBH,dvr=dvrBH, beta=beta)

Zr =  np.sqrt(RateBO.mass/(2*np.pi*beta * RateBO.hbar**2) )

lambda_pert = 1.

####
#Exact
RateAExact = AdiabaticExact(APES0, APES1, APES, dvr0, dvr1, beta, lambda_pert=lambda_pert)
RateDExact = DiabaticExact(DPES0, DPES1, DPES, Ddvr0, Ddvr1, beta)

#####
#Perturbative
RatePert = Perturbative(APES0, APES1, APES, dvr0, dvr1, beta, lambda_pert=lambda_pert)



print("Calculating cffs")
result = {'beta': beta, 'Zr': Zr}
result.update({'lambda_pert': lambda_pert})
result.update({'dim': dim, 'gridl': gridl})


###Selecting calculation time ranges
cfftime = 1000
cfft0 = 0
cfft1 = cfft0 + cfftime
nquad1 = 64 + 1  #Used for imaginary time in cffZero
nquad2 = 128 + 1 #Used for a snippet of imaginary time in cffZero

result.update({'cfft0' : cfft0, 'cfftime': cfftime, 'nquad1': nquad1})

maxtau = beta/2
tau2 = np.linspace(0, maxtau, nquad1, dtype=complex)
tau3 = np.linspace(0, maxtau, nquad2, dtype=complex)
t1   = np.linspace(cfft0, cfft1, nquad1, dtype=complex)
t2   = np.linspace(cfft0, cfft1, nquad1, dtype=complex)
t3   = np.linspace(cfft0, cfft1, nquad1, dtype=complex)

mode = "cffZero"
#mode = "cffOne"
#mode = "cffMany"
# mode = "cffImagMin"

if mode == "cffZero":
    #Set of parameters for z1 = beta/2
    tarray = np.array([0])
    tau1 = np.array([maxtau])

    imag1 = tau1
    imag2 = tau2
    imag3 = tau2
    cutoff = nquad2 * 7 // 8
    imag2cutoff = tau3[cutoff:]
    imag3cutoff = tau3[cutoff:]

    result.update({'imag2': imag2})
    result.update({'imag2cutoff': imag2cutoff})
elif mode == "cffOne":
    #Set of parameters for z1 = beta/2 + i*realtime
    realtime = 0
    nquad3 = np.abs(int(realtime / 6)) + 1 #Used for realtime in cffOne
    t1array = np.array([realtime])
    t2array = np.linspace(0, realtime, num=nquad3)
    tau1 = np.array([maxtau])

    imag1 = tau1
    imag2 = tau2
    imag3 = tau2
    cutoff = nquad2 * 7 // 8
    imag2cutoff = tau3[cutoff:]
    imag3cutoff = tau3[cutoff:]

    real1 = maxtau + 1.j * t1array
    real2 = maxtau + 1.j * t2array
    real3 = maxtau + 1.j * t2array

    result.update({'imag2': imag2})
    result.update({'imag2cutoff': imag2cutoff})
    result.update({'real2': real2})
    result.update({'realtime': realtime})
    result.update({'nquad2': nquad2, 'nquad3': nquad3})
elif mode == 'cffMany':
    #Now z1 = beta/2 + i* (an array of times)
    maxtime = 400
    nquad3 = np.abs(int(maxtime / 6)) + 1 #Used for realtime in cffOne
    t1array = np.linspace(0, maxtime, num=nquad3)
    t2array = np.linspace(0, maxtime, num=nquad3)
    tau1 = np.array([maxtau])

    imag1 = tau1
    imag2 = tau2
    imag3 = tau2
    cutoff = nquad2 * 7 // 8
    imag2cutoff = tau3[cutoff:]
    imag3cutoff = tau3[cutoff:]

    real1 = maxtau + 1.j * t1array
    real2 = maxtau + 1.j * t1array
    real3 = maxtau + 1.j * t1array
    result.update({'imag2': imag2})
    result.update({'real2': real2})
    result.update({'maxtime': maxtime})
    result.update({'nquad2': nquad2, 'nquad3': nquad3})
elif mode == "cffImagMin":
    imag1 = tau2
    imag2 = tau2
    imag3 = tau2
    real1 = beta/2 + 1.j * t1
    real2 = beta/2 + 1.j * t2
    real3 = beta/2 + 1.j * t3
else:
    print("Please select a valid mode")
    exit()

calculate3d = True

##Testing 0101
#print("0101_e")
#res0101_e_i = RatePert.cff0101_e(imag1, beta)
#grad = np.gradient(res0101_e_i)
#min_index = np.argmin(np.abs(grad))
## print(res0101_e_i)
## print(grad)
#print(min_index)
#print(res0101_e_i[min_index])
#print("0101_g")
#res0101_g_i = RatePert.cff0101_g(imag1, beta)
#grad = np.gradient(res0101_g_i)
#min_index = np.argmin(np.abs(grad))
##print(res0101_g_i)
##print(grad)
#print(min_index)
#print(res0101_g_i[min_index])
#print("max: " + str(len(imag1)))
#exit()

##Testing 1010
#res1010_rii = RatePert.cff1010(real1, imag2, real2, beta)
#res1010_rii = RatePert.cff1010_alt(real1, imag2, imag3, beta)
#print(res1010_rii)
#res1010_an_rii = RatePert.cff1010_analytic(real1, imag2, imag3, beta)
#print(res1010_an_rii)
#exit()
print("Calculating cffDExact")
resDExact_i = RateDExact.cff(imag1)
if mode != "cffZero":
    resDExact_r = RateDExact.cff(real1)
print("Shape of cff: " + str(np.shape(resDExact_i)))
result.update({'cffDExact_i': resDExact_i})
if mode != "cffZero":
    result.update({'cffDExact_r': resDExact_r})

print("Calculating cffAExact")
resAExact_i = RateAExact.cff(imag1)
if mode != "cffZero":
    resAExact_r = RateAExact.cff(real1)
print("Shape of cff: " + str(np.shape(resAExact_i)))
result.update({'cffAExact_i': resAExact_i})
if mode != "cffZero":
    result.update({'cffAExact_r': resAExact_r})

print("Calculating cffBO")
resBO_i = RateBO.cff(imag1)
if mode != "cffZero":
    resBO_r = RateBO.cff(real1)
print("Shape of cff: " + str(np.shape(resBO_i)))
result.update({'cffBO_i': resBO_i})
if mode != "cffZero":
    result.update({'cffBO_r': resBO_r})

print("Calculating cffBH")
resBH_i = RateBH.cff(imag1)
if mode != "cffZero":
    resBH_r = RateBH.cff(real1)
print("Shape of cff: " + str(np.shape(resBH_i)))
result.update({'cffBH_i': resBH_i})
if mode != "cffZero":
    result.update({'cffBH_r': resBH_r})


print("Calculating cff0000")
res0000_i = RatePert.cff0000(imag1)
if mode != "cffZero":
    res0000_r = RatePert.cff0000(real1)
print("Shape of cff0000: " + str(np.shape(res0000_i)))
result.update({'cff0000_i': res0000_i })
if mode != "cffZero":
    result.update({'cff0000_r': res0000_r })

print("Calculating cff0101")
res0101_i = RatePert.cff0101(imag1)
if mode != "cffZero":
    res0101_r = RatePert.cff0101(real1)
print("Shape of cff0101: " + str(np.shape(res0101_i)))
result.update({'cff0101_i': res0101_i })
if mode != "cffZero":
    result.update({'cff0101_r': res0101_r })

print("Calculating 0101_g")
res0101_g_i = RatePert.cff0101_g(imag1)
if mode != "cffZero":
    res0101_g_r = RatePert.cff0101_g(real1)
print("Shape of cff0101_g: " + str(np.shape(res0101_g_i)))
result.update({'cff0101_g_i': res0101_g_i })
if mode != "cffZero":
    result.update({'cff0101_g_r': res0101_g_r })

print("Calculating 0101_e")
res0101_e_i = RatePert.cff0101_e(imag1)
if mode != "cffZero":
    res0101_e_r = RatePert.cff0101_e(real1)
print("Shape of cff0101_e: " + str(np.shape(res0101_e_i)))
result.update({'cff0101_e_i': res0101_e_i })
if mode != "cffZero":
    result.update({'cff0101_e_r': res0101_e_r })

print("Calculating cff1001")
res1001_ii =     RatePert.cff1001(imag1, imag2)
if mode != "cffZero":
    res1001_ri =     RatePert.cff1001(real1, imag2)
    res1001_rr = 1.j*RatePert.cff1001(real1, real2)
print("Shape of cff1001: " + str(np.shape(res1001_ii)))
result.update({'cff1001_ii': res1001_ii })
if mode != "cffZero":
    result.update({'cff1001_ri': res1001_ri })
    result.update({'cff1001_rr': res1001_rr })

print("Calculating cff1100")
res1100_ii =     RatePert.cff1100(imag1, imag2)
if mode != "cffZero":
    res1100_ri =     RatePert.cff1100(real1, imag2)
    res1100_rr = 1.j*RatePert.cff1100(real1, real2)
print("Shape of cff1100: " + str(np.shape(res1100_ii)))
result.update({'cff1100_ii': res1100_ii })
if mode != "cffZero":
    result.update({'cff1100_ri': res1100_ri })
    result.update({'cff1100_rr': res1100_rr })

print("Calculating cff2000")
res2000_ii =     RatePert.cff2000(imag1, imag2)
if mode != "cffZero":
    res2000_ri =     RatePert.cff2000(real1, imag2)
    res2000_rr = 1.j*RatePert.cff2000(real1, real2)
print("Shape of cff2000: " + str(np.shape(res2000_ii)))
result.update({'cff2000_ii': res2000_ii })
if mode != "cffZero":
    result.update({'cff2000_ri': res2000_ri })
    result.update({'cff2000_rr': res2000_rr })


# print("Calculating cff1010_alt")
# res1010_alt_iii = RatePert.cff1010_alt(imag1, imag2cutoff, imag3cutoff)
# if mode != "cffZero":
#     res1010_alt_rii =       RatePert.cff1010_alt(real1, imag2, imag3)
#     res1010_alt_rri = 1.j * RatePert.cff1010_alt(real1, real2, imag3)
#     res1010_alt_rir = 1.j * RatePert.cff1010_alt(real1, imag2, real3)
#     res1010_alt_rrr =     - RatePert.cff1010_alt(real1, real2, real3)
# print("Shape of cff1010: " + str(np.shape(res1010_alt_iii)))
# result.update({'cff1010_alt_iii': res1010_alt_iii })
# if mode != "cffZero":
#     result.update({'cff1010_alt_rii': res1010_alt_rii })
#     result.update({'cff1010_alt_rri': res1010_alt_rri })
#     result.update({'cff1010_alt_rrr': res1010_alt_rrr })
#     result.update({'cff1010_alt_rir': res1010_alt_rir })

if calculate3d:
    print("Calculating cff1000")
    res1000_iii =       RatePert.cff1000(imag1, imag2cutoff, imag3cutoff)
    if mode != "cffZero":
        res1000_rii =       RatePert.cff1000(real1, imag2, imag3)
        res1000_rri = 1.j * RatePert.cff1000(real1, real2, imag3)
        res1000_rrr =     - RatePert.cff1000(real1, real2, real3)
        #Forbidden because of time ordering
        #res1000_rir = 1.j * RatePert.cff1000(real1, imag2, real3)
    print("Shape of cff1000: " + str(np.shape(res1000_iii)))
    result.update({'cff1000_iii': res1000_iii })
    if mode != "cffZero":
        result.update({'cff1000_rii': res1000_rii })
        result.update({'cff1000_rri': res1000_rri })
        result.update({'cff1000_rrr': res1000_rrr })
        #result.update({'cff1000_rir': res1000_rir })

    print("Calculating cff1010")
    res1010_iii = RatePert.cff1010(imag1, imag2cutoff, imag3cutoff)
    if mode != "cffZero":
        res1010_rii =       RatePert.cff1010(real1, imag2, imag3)
        res1010_rri = 1.j * RatePert.cff1010(real1, real2, imag3)
        res1010_rir = 1.j * RatePert.cff1010(real1, imag2, real3)
        res1010_rrr =     - RatePert.cff1010(real1, real2, real3)
    print("Shape of cff1010: " + str(np.shape(res1010_iii)))
    result.update({'cff1010_iii': res1010_iii })
    if mode != "cffZero":
        result.update({'cff1010_rii': res1010_rii })
        result.update({'cff1010_rri': res1010_rri })
        result.update({'cff1010_rrr': res1010_rrr })
        result.update({'cff1010_rir': res1010_rir })

if mode == "cffZero":
    name = "./cffdata/cffsZero.pkl"
elif mode == "cffOne":
    name = "./cffdata/cffsOne.pkl"
elif mode == "cffMany":
    name = "./cffdata/cffsMany.pkl"
else:
    name = "./cffdata/cffs.pkl"
print("Writing into " + str(name))
outputfile = open(name,'wb')
pickle.dump(result,outputfile)
