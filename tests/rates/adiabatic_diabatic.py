import numpy as np
import matplotlib.pyplot as plt
from scipy import integrate
from bbo.logger import logger

from bbo.PES.tully import APES, APESBH, APES0, APES1, DPES, DPES0, DPES1
from bbo.dvr.tully import dvrBH, dvr0, dvr1, dvrE, Ddvr0, Ddvr1, gridl

from bbo.rates import print_crossover, BornOppenheimer, AdiabaticExact, DiabaticExact


"""
Compare quantum rates in the adiabatic and in the diabatic representation.
"""

print_crossover(APES0, logger)

beta = 1000
logger.info("Selected beta: " + str(beta))

####
#Born-Oppenheimer
RateBO = BornOppenheimer(PES=APES0, dvr=dvr0, beta=beta)
RateBH = BornOppenheimer(PES=APESBH, dvr=dvrBH, beta=beta)

####
#Exact
RateAExact = AdiabaticExact(APES0, APES1, APES, dvr0, dvr1, beta)
RateDExact = DiabaticExact(DPES0, DPES1, DPES, Ddvr0, Ddvr1, beta)

Zr =  np.sqrt(RateBO.mass/(2*np.pi*beta * RateBO.hbar**2) )

logger.info("Testing the exact rate calculation in the adiabatic and the diabatic representation")
showplots = False
cfftime = 1000
cfft0 = 0
cfft1 = cfft0 + cfftime
nquad_test = 128 + 1

#Testing the adiabatic and diabatic calculations
logger.info("Calculating rate in the adiabatic representation")
t_arr = np.arange(0,cfftime,cfftime/nquad_test)
z = beta/2 + 1.j * t_arr
y = RateAExact.cff(z)
#kAExact = integrate.simps(y, t)
kAExact = integrate.romb(y.real, dx=cfftime/nquad_test)
kAExact /= Zr
logger.info("Rate (A exact): " + str(kAExact))
logger.info("cff(0): " + str(RateAExact.cff( np.array([beta/2]))))

logger.info("Calculating rate in the diabatic representation")
t_arr = np.arange(0,cfftime,cfftime/nquad_test)
z = beta/2 + 1.j * t_arr
y = RateDExact.cff(z)
#kDExact = integrate.simps(y, t)
kDExact = integrate.romb(y.real, dx=cfftime/nquad_test)
kDExact /= Zr
logger.info("Rate (D exact): " + str(kDExact))
logger.info("cff(0): " + str(RateDExact.cff(np.array([beta/2]))))


ratioAD = kAExact / kDExact
logger.info("kA/kD (should be 1): " + str(ratioAD))

