import numpy as np
from polylib import morse
from polylib.PES import diabatic #Need latest dev-future
from polylib.PES import nonadiabatic #Need latest dev-future
from polylib.PES._base import BasePES
from polylib.PES.Eckart import Eckart
from polylib.units import Energy, atomic
from bbo.PES._utils import LowerPES, UpperPES

import matplotlib.pyplot as plt
import os.path


#My own implementation of a library class, not used
class D_exp(BasePES):
	def __init__(self, D=0.02, alpha0=+.5, alpha1=-.5, m=2000., Delta=.01):
		self.mass = m
		self.D = D
		self.Delta = Delta #Diabatic coupling
		self.alpha0 = alpha0
		self.alpha1 = alpha1
		self.UNITS = atomic()
		#Exponential potentials (polylib morse used for compatibility)
		#V(x)=D*exp(alpha*x)
		self.DPES0 = morse.Morse0(mass=self.mass, D=self.D, r0=0, alpha=self.alpha0)
		self.DPES1 = morse.Morse0(mass=self.mass, D=self.D, r0=0, alpha=self.alpha1)

	def potential(self, x):
		#TODO has to be only single instance (i. e., cannot pass vectors of positions)
		#TODO maybe update the documentation about this
		x = np.array(x)
		V0 = self.DPES0.potential(x)
		V1 = self.DPES1.potential(x)

		V = np.zeros((2,2))
		V[0,0] = V0
		V[1,1] = V1
		V[0,1] = self.Delta
		V[1,0] = self.Delta
		return V


#Using a library potential
DPES = diabatic.exp(A=.04, B=1., C=.01 )
##Changing B to 2 messes up the DVR
DPES.UNITS = atomic()

##Adiabatic representation
APES = nonadiabatic.TwoLevelAdiabaticRepresentation(DPES)
APES.UNITS = atomic()

#d = APES.first_order_coupling(1.2)[0,1]
#print(d)
#exit()

APES0 = LowerPES(APES, mode="adiabatic")
APES1 = UpperPES(APES, mode="adiabatic")
DPES0 = LowerPES(DPES, mode="diabatic")
DPES1 = UpperPES(DPES, mode="diabatic")

def d(x):
	r"""The derivative coupling vector (here scalar, because we are in 1D)
	"""
	res = APES.first_order_coupling(x)
	return res

def Lambda2(x):
	hbar = 1
	res = d(x)[0, 1] * d(x)[1, 0]
	res *= - hbar**2 / (2*APES.mass)
	return res

Eck = Eckart(m = APES0.mass, a = .5, B = .025)
Eck.UNITS = atomic()
plotEckart = True



if plotEckart:
	class custom_pes(BasePES):
		def __init__(self):
			self.UNITS = atomic()
			self.mass = Eck.mass

		def potential(self, x):
			return Eck.potential(x)
else:
	class custom_pes(BasePES):
		def __init__(self):
			self.UNITS = atomic()
			self.mass = APES0.mass

		def potential(self, x):
			return APES0.potential(x)


if __name__ == "__main__":
	#Energy units converter:
	#https://www.weizmann.ac.il/oc/martin/tools/hartree.html
	barrier_height = Energy(Eck.B, 'hartree' )
	print("Barrier Height/(kcal/mol): " + str(barrier_height.get('kcal/mol')))

	#Plotting
	l = 1.
	nplot = 1000
	x = np.linspace(-l,l,nplot)
	y0D = np.zeros_like(x)
	y1D = np.zeros_like(x)
	y0A = np.zeros_like(x)
	y1A = np.zeros_like(x)
	y0ALambda = np.zeros_like(x)
	yE = np.zeros_like(x)
	d_vec = np.zeros_like(x)
	for i in range(len(x)):
		y0D[i] = DPES0.potential(x[i])
		y1D[i] = DPES1.potential(x[i])
		y0A[i] = APES0.potential(x[i])
		y1A[i] = APES1.potential(x[i])
		y0ALambda[i] = APES0.potential(x[i]) + Lambda2(x[i])
		yE[i] = Eck.potential(x[i])
		d_vec[i] = APES.first_order_coupling(x[i])[0,1]

	d_vec = d_vec/d_vec[nplot//2] * 4*Eck.B

	fig, ax = plt.subplots()
	ax.plot(x, y0D, label="DPES0")
	ax.plot(x, y1D, label="DPES1")
	ax.plot(x, y0A, linewidth=2, label="APES0")
	ax.plot(x, y1A, linewidth=2, label="APES1")
	ax.plot(x, y0ALambda, linewidth=2, label="APES0 + Lambda2")
	if plotEckart:
		ax.plot(x, yE, linewidth=4, label="Eckart")
	ax.plot(x, d_vec, linestyle="dashed", linewidth=2, label="d_01 (rescaled)")

	instfile = "optimized_inst.pkl"
	if os.path.isfile(instfile) and 0:
		data = np.load(instfile, allow_pickle=True, encoding="latin1")
		instx = data['x']
		#print(np.shape(instx))
		insty = np.zeros_like(instx)
		for i in range(len(instx)):
			if plotEckart:
				insty[i] = Eck.potential(instx[i])
			else:
				insty[i] = APES0.potential(instx[i])
		ax.plot(instx, insty, 'o-', label="instanton")

	ax.set_ylabel("V/Ha")
	ax.set_xlabel("x/A")
	ax.legend()
	plt.show()