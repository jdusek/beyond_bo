import numpy as np
from bbo.logger import logger
from bbo.rates import SemiClassicOneState, SemiClassicTwoState, Perturbative, print_crossover

from bbo.PES.tully import APES, APESBH, APES0, APES1, DPES, DPES0, DPES1
from bbo.dvr.tully import dvrBH, dvr0, dvr1, dvrE, Ddvr0, Ddvr1, gridl


print_crossover(APES0)
beta = 500
beta = 1000
logger.info("beta: " + str(beta))

tau_test = .15

RatePert = Perturbative(APES0, APES1, APES, dvr0, dvr1, beta)

RateSC_single = SemiClassicOneState(APES0, beta)
RateSC = SemiClassicTwoState(APES0, APES1, APES, beta)

Zr =  np.sqrt(APES0.mass/(2*np.pi*beta * RateSC.hbar**2) )


#ratio1 = kAExact / kBO
#ratio2 = kAExact / kBH
#ratio3 = kAExact / kWKB
#print("kAexact/kBO: " + str(ratio1) )
#print("kAexact/kBH: " + str(ratio2) )
#print("kAexact/kWKB: " + str(ratio3) )

# Testing semiclassics
z1 = np.array([beta/2])
z2 = np.array([tau_test])

logger.info("#########")
logger.info("Testing SC and QM cffs:")
SC0000 = RateSC_single.cff0000()
logger.info("SC cff0000: " + str(SC0000))
QM0000 = RatePert.cff0000(z1)[0].real
logger.info("QM cff0000: " + str(QM0000))
delta0000 = (SC0000-QM0000) / QM0000
logger.info("error: " + "{:.2f}%".format(delta0000*100))

SC0101 = RateSC.cff0101()
logger.info("SC cff0101: " + str(SC0101))
val = np.array([beta/2.])
val = np.array([0.])
QM0101 = RatePert.cff0101(val)[0].real
logger.info("QM cff0101: " + str(QM0101))
delta0101 = (SC0101+-QM0101) / QM0101
logger.info("error: " + "{:.2f}%".format(delta0101*100))

SC2000 = RateSC.cff2000()
logger.info("SC cff2000: " + str(SC2000))
Lambda_int = RateSC.Lambda_int()
logger.info("SC Lambda_int: " + str(Lambda_int))
SCk0000 = RateSC.cff0000_integrated() / Zr
logger.info("SC k0000: " + str(SCk0000))
#delta2000 = (SC2000-QM2000) / QM2000
#logger.info("error: " + "{:.2f}%".format(delta2000*100))


exit()
logger.info("Rate (A exact): " + str(kAExact))
logger.info("Rate (WKB): " + str(kWKB))
ktest = kWKB * (1 +  2 * Lambda_int)
logger.info("Factor: " + str(1 +  2 * Lambda_int))
logger.info("ktest: " + str(ktest))
logger.info("ratio: " + str(ktest/kAExact))
exit()
logger.info("Turning point SC: " + str(RateSC_single.turn_lower[0]))
SC1100 = RateSC.cff1100(showplot=True)
logger.info("SC cff1100: " + str(SC1100))
SCk1100 = RateSC.cff1100_integrated()
logger.info("SC k1100: " + str(SCk1100))
logger.info("k1100/k0000: " + str((SCk1100/SCk0000).real))
