import numpy as np
from scipy import integrate
import matplotlib.pyplot as plt


name = "./cffdata/cffsZero.pkl"
print("Loading cff data from " + str(name))
cffdata = np.load(name, allow_pickle=True, encoding="latin1")
dim = cffdata['dim']

cfftime = cffdata['cfftime']
cfft0 = cffdata['cfft0']
cfft1 = cfft0 + cfftime
imag2 = cffdata['imag2']
imag2cutoff = cffdata['imag2cutoff']
nquad1 = cffdata['nquad1']
timeArray = np.arange(cfft0, cfft1, cfftime/nquad1)

cffBO = cffdata['cffBO']
cffBO_t = cffdata['cffBO_t']

cffAExact_i = cffdata['cffAExact_i']
#cffAExact_r = cffdata['cffAExact_r']

cffDExact_i = cffdata['cffDExact_i']
#cffDExact_r = cffdata['cffDExact_r']

cff0000_i = cffdata['cff0000_i']
#cff0000_r = cffdata['cff0000_r']

cff0101_i = cffdata['cff0101_i']
#cff0101_r = cffdata['cff0101_r']

cff1001_ii = cffdata['cff1001_ii']
#cff1001_ri = cffdata['cff1001_ri']
#cff1001_rr = cffdata['cff1001_rr']

cff1100_ii = cffdata['cff1100_ii']
#cff1100_ri = cffdata['cff1100_ri']
#cff1100_rr = cffdata['cff1100_rr']

cff2000_ii = cffdata['cff2000_ii']
#cff2000_ri = cffdata['cff2000_ri']
#cff2000_rr = cffdata['cff2000_rr']

cff1000_iii = cffdata['cff1000_iii']
#cff1000_rii = cffdata['cff1000_rii']
#cff1000_rir = cffdata['cff1000_rir']
#cff1000_rri = cffdata['cff1000_rri']
#cff1000_rrr = cffdata['cff1000_rrr']

cff1010_iii = cffdata['cff1010_iii']
#cff1010_rii = cffdata['cff1010_rii']
#cff1010_rir = cffdata['cff1010_rir']
#cff1010_rri = cffdata['cff1010_rri']
#cff1010_rrr = cffdata['cff1010_rrr']

Zr = cffdata['Zr']
beta = cffdata['beta']

print("beta: " + str(beta))
print("nquad: " + str(nquad1))

reduce2d = True
plot2d = False
plot3d = True
reduce3d = True
plotzero  = True

if plot2d:
    fig, ax = plt.subplots(1, 3, figsize=(16, 5))
    fig.suptitle('beta='+str(int(beta)))
    ax[0].plot(imag2, cff1001_ii[0], label="cff 1001")
    ax[1].plot(imag2, cff1100_ii[0], label="cff 1100")
    ax[2].plot(imag2, cff2000_ii[0], label="cff 2000")
    
    ax[0].set_xlabel("tau")
    ax[1].set_xlabel("tau")
    ax[2].set_xlabel("tau")
    ax[0].set_ylabel("cff(0, tau)")
    ax[1].set_ylabel("cff(0, tau)")
    ax[2].set_ylabel("cff(0, tau)")
    ax[0].legend()
    ax[1].legend()
    ax[2].legend()
    plt.show()

if plot3d:
    fig, ax = plt.subplots(1, 2, figsize=(12, 5))
    im0 = ax[0].imshow( cff1000_iii[0].real)
    #im1 = ax[1].imshow( cff1010_iii[0].real)
    
    ax[0].set_xlabel("tau2")
    ax[0].set_ylabel("tau1")
    ax[0].set_title("cff1000(0, tau1, tau2)")

    ax[1].set_xlabel("tau1")
    ax[1].set_ylabel("tau2")
    ax[1].set_title("cff1010(0, tau1, tau2)")
    
    plt.colorbar(im0, ax=ax[0])
    #plt.colorbar(im1, ax=ax[1])
    plt.show()

if reduce2d:
    cff1001_reduced = integrate.simps(cff1001_ii[0].real, imag2.real)
    cff1100_reduced = integrate.simps(cff1100_ii[0].real, imag2.real)
    cff2000_reduced = integrate.simps(cff2000_ii[0].real, imag2.real)
    print("2000: " + str(cff2000_reduced))
    print("1001: " + str(cff1001_reduced))

if reduce3d:
    #print(cff1000_iii[0,-1,-1])
    replaced = np.nan_to_num(cff1000_iii)
    #print(np.shape(replaced))
    tmp = integrate.simps(replaced[0], imag2cutoff.real)
    #print(np.shape(imag2cutoff))
    cff1000_reduced = integrate.simps(tmp.real, imag2cutoff.real)
    print("1000: " + str(cff1000_reduced))

    #replaced = np.nan_to_num(cff1010_iii) #No need for this: there is no time ordering here
    tmp = integrate.simps(cff1010_iii[0], imag2cutoff.real)
    cff1010_reduced = integrate.simps(tmp.real, imag2cutoff.real)


cff_fo = cff0000_i[0].real
cff_fo += cff0101_i[0].real
print("0101: " + str(cff0101_i[0].real))
if reduce2d:
    cff_fo += 2*(cff2000_reduced + cff1100_reduced + cff1001_reduced)

if reduce3d:
    cff_fo += 2*(cff1000_reduced) + cff1010_reduced


if plotzero:
    data_exact = \
    {
        'Adiabatic Exact' : cffAExact_i[-1].real ,
        'Diabatic Exact'  : cffDExact_i[-1].real ,
    }
    names_exact  = list(data_exact.keys())
    values_exact = list(data_exact.values())

    data_lo = \
    {
        'BO': cffBO[0].real,
        '0000' : cff0000_i[0].real,
    }
    names_lo  = list(data_lo.keys())
    values_lo = list(data_lo.values())

    data_fo = \
    {
        '0101' : cff0101_i[0].real,
    }
    if reduce2d:
        data_fo.update(
            {
                '1001' : cff1001_reduced,
                '1100' : cff1100_reduced,
                '2000' : cff2000_reduced,
            }
        )
    if reduce3d:
        data_fo.update(
            {
                '1000': cff1000_reduced,
                '1010': cff1010_reduced,
            }
        )
    names_fo  = list(data_fo.keys())
    values_fo = list(data_fo.values())

    data_fo_total = {
        '1st total': cff_fo
    }
    names_fo_total  = list(data_fo_total.keys())
    values_fo_total = list(data_fo_total.values())


    fig, ax = plt.subplots()
    #ax[0].set_xlabel("t")
    ax.set_ylabel("Re c_ff(beta/2) ")
    ax.set_title("Re cff(beta/2)")

    plt.bar(names_exact, values_exact, width = 0.4, label="exact")
    plt.bar(names_lo, values_lo, width = 0.4, label="Leading order")  
    plt.bar(names_fo_total, values_fo_total, width = 0.4, label="First order, total")  
    plt.bar(names_fo, values_fo, width = 0.4, label="First order, components")  
    ax.axhline(y=cffAExact_i[-1],linestyle='--')
    ax.legend()
    plt.show()

print("Exact:   " + str(cffAExact_i[-1].real))
#print("Exact:   " + str(cffDExact_i[-1].real))
print("Leading: " + str(cff0000_i[0].real))
print("First:   " + str(cff_fo))
a = cff_fo - cff0000_i[0].real
b = cffAExact_i[-1].real - cff0000_i[0].real
print("Ratio test: ")
print(a/b)