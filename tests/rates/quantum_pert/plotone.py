import numpy as np
from scipy import integrate
import matplotlib.pyplot as plt


name = "./cffdata/cffsOne.pkl"
generated3d = True
Newdata = True
print("Loading cff data from " + str(name))
cffdata = np.load(name, allow_pickle=True, encoding="latin1")
dim = cffdata['dim']
lambda_pert = cffdata['lambda_pert']

cfftime = cffdata['cfftime']
cfft0 = cffdata['cfft0']
cfft1 = cfft0 + cfftime
imag2 = cffdata['imag2']
imag2cutoff = cffdata['imag2cutoff']
real2 = cffdata['real2']
realtime = cffdata['realtime']
nquad1 = cffdata['nquad1']

cffBO = cffdata['cffBO']
cffBO_t = cffdata['cffBO_t']

cffAExact_i = cffdata['cffAExact_i']
cffAExact_r = cffdata['cffAExact_r']

cffDExact_i = cffdata['cffDExact_i']
cffDExact_r = cffdata['cffDExact_r']

cff0000_i = cffdata['cff0000_i']
cff0000_r = cffdata['cff0000_r']

cff0101_i = cffdata['cff0101_i']
cff0101_r = cffdata['cff0101_r']

cff1001_ii = cffdata['cff1001_ii']
cff1001_ri = cffdata['cff1001_ri']
cff1001_rr = cffdata['cff1001_rr']

cff1100_ii = cffdata['cff1100_ii']
cff1100_ri = cffdata['cff1100_ri']
cff1100_rr = cffdata['cff1100_rr']

cff2000_ii = cffdata['cff2000_ii']
cff2000_ri = cffdata['cff2000_ri']
cff2000_rr = cffdata['cff2000_rr']

if Newdata:
    cff1010_alt_iii = np.nan_to_num(cffdata['cff1010_alt_iii'])
    cff1010_alt_rii = np.nan_to_num(cffdata['cff1010_alt_rii'])
    cff1010_alt_rir = np.nan_to_num(cffdata['cff1010_alt_rir'])
    cff1010_alt_rri = np.nan_to_num(cffdata['cff1010_alt_rri'])
    cff1010_alt_rrr = np.nan_to_num(cffdata['cff1010_alt_rrr'])
    # print(cffdata['cff1010_alt_rri'])


if generated3d:
    cff1000_iii = cffdata['cff1000_iii']
    cff1000_rii = cffdata['cff1000_rii']
    #cff1000_rir = cffdata['cff1000_rir']
    cff1000_rri = cffdata['cff1000_rri']
    cff1000_rrr = cffdata['cff1000_rrr']

    cff1010_iii = np.nan_to_num(cffdata['cff1010_iii'])
    cff1010_rii = np.nan_to_num(cffdata['cff1010_rii'])
    cff1010_rir = np.nan_to_num(cffdata['cff1010_rir'])
    cff1010_rri = np.nan_to_num(cffdata['cff1010_rri'])
    cff1010_rrr = np.nan_to_num(cffdata['cff1010_rrr'])

Zr = cffdata['Zr']
beta = cffdata['beta']

print("beta: " + str(beta))
print("nquad1: " + str(nquad1))

reduce2d = True
plot2d = False
plot3d = False
reduce3d  = generated3d
plotzero  = True

if plot2d:
    fig1, ax1 = plt.subplots(1, 2, figsize=(12, 5))
    fig2, ax2 = plt.subplots(1, 2, figsize=(12, 5))
    fig3, ax3 = plt.subplots(1, 2, figsize=(12, 5))

    fig1.suptitle('beta='+str(int(beta)))
    fig2.suptitle('beta='+str(int(beta)))
    fig3.suptitle('beta='+str(int(beta)))
    ax1[0].plot(imag2, cff1001_ri[0], label="cff 1001")
    ax1[1].plot(real2.imag, cff1001_rr[0], label="cff 1001")

    ax2[0].plot(imag2, cff1100_ri[0], label="cff 1100")
    ax2[1].plot(real2.imag, cff1100_rr[0], label="cff 1100")

    ax3[0].plot(imag2, cff2000_ri[0], label="cff 2000")
    ax3[1].plot(real2.imag, cff2000_rr[0], label="cff 2000")
    
    ax1[0].set_xlabel("tau")
    ax1[1].set_xlabel("t")
    ax2[0].set_xlabel("tau")
    ax2[1].set_xlabel("t")
    ax3[0].set_xlabel("tau")
    ax3[1].set_xlabel("t")

    ax1[0].set_ylabel("cff(0, tau)")
    ax1[1].set_ylabel("cff(0, tau)")
    ax2[0].set_ylabel("cff(0, tau)")
    ax2[1].set_ylabel("cff(0, tau)")
    ax3[0].set_ylabel("cff(0, tau)")
    ax3[1].set_ylabel("cff(0, tau)")

    ax1[0].legend
    ax1[1].legend()
    ax2[0].legend()
    ax2[1].legend()
    ax3[0].legend()
    ax3[1].legend()
    plt.show()

if plot3d:
    fig, ax = plt.subplots(1, 2, figsize=(12, 5))
    im0 = ax[0].imshow( cff1000_rii[0].real, extent = (imag2cutoff.real[0], imag2cutoff.real[-1], imag2cutoff.real[-1], imag2cutoff.real[0]))
    im1 = ax[1].imshow( cff1010_rii[0].real, extent = (imag2cutoff.real[0], imag2cutoff.real[-1], imag2cutoff.real[-1], imag2cutoff.real[0]))
    
    ax[0].set_xlabel("tau2")
    ax[0].set_ylabel("tau1")
    ax[0].set_title("cff1000(" + str(int(realtime)) + ", tau1, tau2)")

    ax[1].set_xlabel("tau1")
    ax[1].set_ylabel("tau2")
    ax[1].set_title("cff1010(" + str(int(realtime)) + ", tau1, tau2)")
    
    plt.colorbar(im0, ax=ax[0])
    plt.colorbar(im1, ax=ax[1])
    plt.show()

if reduce2d:
    cff1001_reduced  = integrate.simps(cff1001_ri[0].real, imag2.real)
    cff1001_reduced += integrate.simps(cff1001_rr[0].real, real2.imag)
    #cff1001_reduced = 0

    cff1100_reduced  = integrate.simps(cff1100_ri[0].real, imag2.real)
    cff1100_reduced += integrate.simps(cff1100_rr[0].real, real2.imag)

    cff2000_reduced  = integrate.simps(cff2000_ri[0].real, imag2.
    real)
    cff2000_reduced += integrate.simps(cff2000_rr[0].real, real2.imag)
    # print("2000: " + str(cff2000_reduced))
    # print("1001: " + str(cff1001_reduced))
    # print("1100: " + str(cff1100_reduced))
    #print(cff1100_reduced / cff1001_reduced)

if reduce3d:
    ##1000
    #rii
    replaced_1000_rii = np.nan_to_num(cff1000_rii)
    tmp = integrate.simps(replaced_1000_rii[0], imag2.real)
    cff1000_reduced  = integrate.simps(tmp.real, imag2.real)
    #rri
    #First integrate along the imaginary time, then along real time
    replaced_1000_rri = np.nan_to_num(cff1000_rri)
    tmp = integrate.simps(replaced_1000_rri[0], imag2.real)
    cff1000_reduced += integrate.simps(tmp.real, real2.imag)
    #rrr
    replaced_1000_rrr = np.nan_to_num(cff1000_rrr)
    tmp = integrate.simps(replaced_1000_rrr[0], real2.imag)
    cff1000_reduced += integrate.simps(tmp.real, real2.imag)
    
    ##1010
    #rii
    tmp = integrate.simps(cff1010_rii[0], imag2.real)
    cff1010_reduced  = integrate.simps(tmp.real, imag2.real)
    print("1010: " + str(cff1010_reduced))
    #rri
    tmp = integrate.simps(cff1010_rri[0], imag2.real)
    cff1010_reduced += integrate.simps(tmp.real, real2.imag)
    print("1010: " + str(cff1010_reduced))
    #rir
    tmp = integrate.simps(cff1010_rir[0], real2.imag)
    cff1010_reduced += integrate.simps(tmp.real, imag2.real)
    #print(tmp[42].real)
    #print(integrate.simps(tmp.real, imag2.real))
    print("1010: " + str(cff1010_reduced))
    #rrr
    tmp = integrate.simps(cff1010_rrr[0], real2.imag)
    cff1010_reduced += integrate.simps(tmp.real, real2.imag)
    print(integrate.simps(tmp.real, real2.imag))
    
    #cff1010_reduced = 0
    # print("1000: " + str(cff1000_reduced))
    print("1010: " + str(cff1010_reduced))

if Newdata:
    cff1010_alt_reduced = cff1010_alt_rii + cff1010_alt_rri + cff1010_alt_rir + cff1010_alt_rrr
    print("1010 (alt): " + str(cff1010_alt_reduced[0].real))
    print(cff1010_alt_rii /  cff1010_reduced)
exit()

cff_fo = cff0000_r[0].real
cff_fo += cff0101_r[0].real
print("0101: " + str(cff0101_i[0].real))
if reduce2d:
    cff_fo += 2*(cff2000_reduced + cff1100_reduced + cff1001_reduced)

if reduce3d:
    cff_fo += 2*(cff1000_reduced) + cff1010_reduced


if plotzero:
    data_exact = \
    {
        'Adiabatic Exact' : cffAExact_r[0].real ,
        #'Diabatic Exact'  : cffDExact_r[0].real ,
    }
    names_exact  = list(data_exact.keys())
    values_exact = list(data_exact.values())

    data_lo = \
    {
        #'BO': cffBO[0].real,
        '0000' : cff0000_r[0].real,
    }
    names_lo  = list(data_lo.keys())
    values_lo = list(data_lo.values())

    data_fo = \
    {
        '0101' : cff0101_r[0].real,
    }
    if reduce2d:
        data_fo.update(
            {
                '1001' : cff1001_reduced,
                '1100' : cff1100_reduced,
                '2000' : cff2000_reduced,
            }
        )
    if reduce3d:
        data_fo.update(
            {
                '1000': cff1000_reduced,
                '1010': cff1010_reduced,
            }
        )
    names_fo  = list(data_fo.keys())
    values_fo = list(data_fo.values())

    data_fo_total = {
        '1st total': cff_fo
    }
    names_fo_total  = list(data_fo_total.keys())
    values_fo_total = list(data_fo_total.values())


    fig, ax = plt.subplots()
    #ax[0].set_xlabel("t")
    ax.set_ylabel("Re c_ff(beta/2 + " + str(realtime) + "), lambda=" + str(lambda_pert))
    ax.set_title("Re cff(beta/2 + " + str(realtime) + "), lambda=" + str(lambda_pert))

    plt.bar(names_exact, values_exact, width = 0.4, label="exact")
    plt.bar(names_lo, values_lo, width = 0.4, label="Leading order")  
    plt.bar(names_fo_total, values_fo_total, width = 0.4, label="First order, total")  
    plt.bar(names_fo, values_fo, width = 0.4, label="First order, components")  
    ax.axhline(y=cffAExact_r[0],linestyle='--')
    ax.legend()
    plt.show()

#print("Exact:   " + str(cffAExact_r[0].real))
#print("Leading: " + str(cff0000_i[0].real))
#print("First:   " + str(cff_fo))
#a = cff_fo - cff0000_i[0].real
#b = cffAExact_r[0].real - cff0000_i[0].real
#print("Ratio test: ")
#print(a/b)