import logging
import sys

"""
How to do logging in python? Basic tutorial.
https://docs.python.org/3/howto/logging.html
"""


logger = logging.getLogger(__name__)
logger.propagate = False

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler = logging.StreamHandler()
handler.setFormatter(formatter)

logger.addHandler(handler)

logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    logger.info("Hi, this is an info")
    logger.debug("Hi, this is some more granular info")