from bbo.logger import logger
from bbo.rates import BornOppenheimer, print_crossover

from bbo.PES.tully import APES, APESBH, APES0, APES1, DPES, DPES0, DPES1
from bbo.dvr.tully import dvrBH, dvr0, dvr1, dvrE, Ddvr0, Ddvr1, gridl

print_crossover(APES0)
beta = 500
logger.info("beta: " + str(beta))

RateBO = BornOppenheimer(PES=APES0, dvr=dvr0, beta=beta)


logger.info("Comparing different methods of computing F for the lower adiabat")
k = 9
l = 8
a =     RateBO.Fmn_fast()
b =     RateBO.Fmn_Step()
c = RateBO.Fmn_Velocity()
logger.info(a[k][l])
logger.info(b[k][l])
logger.info(c[k][l])