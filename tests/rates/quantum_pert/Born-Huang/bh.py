from scipy import integrate

#TODO fix this broken test



def IntegrateAndPlotcff(length, cff, beta, Zr, title="", nquad=129, showplot=True):
    """Plots the cff and computes the rate from it by integrating it.

    Args:
        length (int): time length over which to plot cff
        nquad (int): has to be 2^m + 1
        cff (function): function to compute the cff
        beta (float): inverse temperature of the calculation
        Zr (float): reactant partition function
        showplot (bool, optional): Show the plot? Defaults to True.

    Returns:
        k (float): the rate
        t (array of floats): the times at which we sampled the cff
        y (array of floats): sampled values of the cff
    """
    t = np.arange(0,length,length/nquad)
    y = cff(t)
    #k = integrate.simps(y, t)
    #print("Rate: ", k)
    k = integrate.romb(y.real, dx=length/nquad)
    k /= Zr

    if showplot:
        fig, ax = plt.subplots()
        ax.plot(t, y.real)
        plt.xlabel("t")
        plt.ylabel("Re c_ff")
        plt.title(title)
        plt.show()
    return k, t, y

print("Calculating Born-Oppenheimer rate")
kBO, tBO, yBO = IntegrateAndPlotcff(cfftime, RateBO.cff, beta, Zr,
"Born-Oppenheimer", nquad=129, showplot=showplots)
print("Rate (BO): ", kBO)
print("BO/WKB: " + str(kBO/kWKB))
#exit()

print("Calculating Born-Huang rate")
kBH, tBH, yBH = IntegrateAndPlotcff(cfftime, RateBH.cff, beta, Zr, "Born-Huang", nquad=129, showplot=showplots)
print("Rate (B-H): ", kBH)
print("BH/BO: " + str(kBH/kBO))