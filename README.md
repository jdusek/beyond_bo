# beyond_bo

Toy one-dimensional curve crossing model

Two models:
- with exponential potentials . For
results see
[Joe's paper](https://pubs.aip.org/jcp/article/151/11/114119/198864/On-the-calculation-of-quantum-mechanical-electron).
- The Tully model


## Code structure

- bbo: here is the library
- tests: here you can see how it works

Put this directory in your PYTHONPATH
